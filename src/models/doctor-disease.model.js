const mongoose = require('mongoose');

const { Schema } = mongoose;

const DoctorDiseaseSchema = new Schema(
  {
    disease: {
      type: Schema.Types.ObjectId,
      ref: 'Disease',
      required: true,
    },
    doctor: {
      type: Schema.Types.ObjectId,
      ref: 'Doctor',
      required: true,
    },
    status: {
      type: String,
      enum: ['waiting', 'accepted', 'denied', 'closed'],
      default: 'waiting',
      required: true,
    },
  },
  {
    timestamps: {
      createdAt: 'createdAt',
    },
  }
);
DoctorDiseaseSchema.index({ disease: 1, doctor: 1 }, { unique: true });

module.exports = mongoose.model('DoctorDisease', DoctorDiseaseSchema);
