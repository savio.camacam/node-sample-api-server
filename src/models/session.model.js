const mongoose = require('mongoose');
const versionHistory = require('mongoose-version-history');

const { Schema } = mongoose;

const SessionSchema = new Schema(
  {
    treatment: {
      type: Schema.Types.ObjectId,
      ref: 'Treatment',
      required: true,
    },
    sick: {
      type: Schema.Types.ObjectId,
      ref: 'Sick',
      required: true,
    },
    survey: {
      type: Schema.Types.ObjectId,
      ref: 'Survey',
    },
    doctor: {
      type: Schema.Types.ObjectId,
      ref: 'Doctor',
      required: true,
    },
    date: {
      type: Schema.Types.Date,
      required: true,
    },
    status: {
      type: String,
      enum: ['waiting', 'accepted', 'denied', 'closed', 'progress'],
      default: 'waiting',
      required: true,
    },
    frequency: { type: Schema.Types.Number },
    intensity: { type: Schema.Types.Number },
    seriesNumber: { type: Schema.Types.Number },
    intervalTime: { type: Schema.Types.Number },
    seriesTime: { type: Schema.Types.Number },
    pulsesNumber: { type: Schema.Types.Number },
    applicationLocal: { type: Schema.Types.Number },
  },
  {
    timestamps: {
      createdAt: 'createdAt',
    },
  }
);

SessionSchema.plugin(versionHistory, { trackDate: true });

module.exports = mongoose.model('Session', SessionSchema);
