const mongoose = require('mongoose');

const { Schema } = mongoose;

const SickTreatmentSchema = new Schema(
  {
    treatment: {
      type: Schema.Types.ObjectId,
      ref: 'Treatment',
      required: true,
    },
    sick: {
      type: Schema.Types.ObjectId,
      ref: 'Doctor',
      required: true,
    },
    status: {
      type: String,
      enum: ['waiting', 'accepted', 'denied', 'closed', 'progress'],
      default: 'waiting',
      required: true,
    },
  },
  {
    timestamps: {
      createdAt: 'createdAt',
    },
  }
);
SickTreatmentSchema.index({ treatment: 1, sick: 1 }, { unique: true });

module.exports = mongoose.model('SickTreatment', SickTreatmentSchema);
