const mongoose = require('mongoose');

const { Schema } = mongoose;

const SickQuestionSchema = new Schema(
  {
    question: {
      type: Schema.Types.ObjectId,
      ref: 'Question',
      required: true,
    },
    sick: {
      type: Schema.Types.ObjectId,
      ref: 'Sick',
      required: true,
    },
    survey: {
      type: Schema.Types.ObjectId,
      ref: 'Survey',
      required: true,
    },
    answer: {},
  },
  {
    timestamps: {
      createdAt: 'createdAt',
    },
  }
);

SickQuestionSchema.index({ question: 1, sick: 1, survey: 1 }, { unique: true });
module.exports = mongoose.model('SickQuestion', SickQuestionSchema);
