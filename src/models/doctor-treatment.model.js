const mongoose = require('mongoose');

const { Schema } = mongoose;

const DoctorTreatmentSchema = new Schema(
  {
    treatment: {
      type: Schema.Types.ObjectId,
      ref: 'Treatment',
      required: true,
    },
    doctor: {
      type: Schema.Types.ObjectId,
      ref: 'Doctor',
      required: true,
    },
    status: {
      type: String,
      enum: ['waiting', 'accepted', 'denied', 'closed', 'progress'],
      default: 'waiting',
      required: true,
    },
  },
  {
    timestamps: {
      createdAt: 'createdAt',
    },
  }
);

DoctorTreatmentSchema.index({ treatment: 1, doctor: 1 }, { unique: true });
module.exports = mongoose.model('DoctorTreatment', DoctorTreatmentSchema);
