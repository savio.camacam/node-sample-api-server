const mongoose = require('mongoose');

const { Schema } = mongoose;

const SickDiseaseSchema = new Schema(
  {
    disease: {
      type: Schema.Types.ObjectId,
      ref: 'Disease',
      required: true,
    },
    sick: {
      type: Schema.Types.ObjectId,
      ref: 'Doctor',
      required: true,
    },
    status: {
      type: String,
      enum: ['waiting', 'accepted', 'denied', 'closed'],
      default: 'waiting',
      required: true,
    },
  },
  {
    timestamps: {
      createdAt: 'createdAt',
    },
  }
);
SickDiseaseSchema.index({ disease: 1, sick: 1 }, { unique: true });

module.exports = mongoose.model('SickDisease', SickDiseaseSchema);
