const mongoose = require('mongoose');

const { Schema } = mongoose;

const ProfileSchema = new Schema(
  {
    user: {
      type: Schema.Types.ObjectId,
      ref: 'User',
    },
    avatar: {
      type: Schema.Types.String,
      default: 'assets/placeholder/profile.jpg',
    },
    active: {
      type: Boolean,
      default: true,
    },
  },
  {
    timestamps: {
      createdAt: 'createdAt',
    },
  }
);

module.exports = mongoose.model('Profile', ProfileSchema);
