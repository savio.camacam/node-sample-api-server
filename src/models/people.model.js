const mongoose = require('mongoose');

const { Schema } = mongoose;

const PeopleSchema = new Schema(
  {
    name: { type: Schema.Types.String, required: true },
    cpf: { type: Schema.Types.String },
    birth: { type: Schema.Types.String },
    gender: { type: Schema.Types.String, enum: ['m', 'f', 'nd'] },
    rg: { type: Schema.Types.String },
    schoolarDegree: { type: Schema.Types.String },
    rgUf: { type: Schema.Types.String },
    user: {
      type: Schema.Types.ObjectId,
      ref: 'User',
    },
  },
  {
    timestamps: {
      createdAt: 'createdAt',
    },
  }
);

module.exports = mongoose.model('People', PeopleSchema);
