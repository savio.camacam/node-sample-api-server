const mongoose = require('mongoose');

const { Schema } = mongoose;

const TreatmentDiseaseSchema = new Schema(
  {
    treatment: {
      type: Schema.Types.ObjectId,
      ref: 'Treatment',
      required: true,
    },
    disease: {
      type: Schema.Types.ObjectId,
      ref: 'Disease',
      required: true,
    },
    status: {
      type: String,
      enum: ['waiting', 'accepted', 'denied', 'closed'],
      default: 'waiting',
      required: true,
    },
  },
  {
    timestamps: {
      createdAt: 'createdAt',
    },
  }
);

TreatmentDiseaseSchema.index({ treatment: 1, disease: 1 }, { unique: true });
module.exports = mongoose.model('TreatmentDisease', TreatmentDiseaseSchema);
