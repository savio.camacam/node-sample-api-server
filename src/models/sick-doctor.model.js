const mongoose = require('mongoose');

const { Schema } = mongoose;

const SickDoctorSchema = new Schema(
  {
    sick: {
      type: Schema.Types.ObjectId,
      ref: 'Sick',
      required: true,
    },
    doctor: {
      type: Schema.Types.ObjectId,
      ref: 'Doctor',
      required: true,
    },
    status: {
      type: String,
      enum: ['waiting', 'accepted', 'denied', 'closed'],
      default: 'waiting',
      required: true,
    },
    symptoms: [],
    exams: [],
    medicines: [],
    treatments: [],
    diagnostics: {
      type: String,
    },
    motorThreshold: {
      type: Number,
    },
    intensity: {
      type: Number,
      min: 0,
      max: 100,
    },
  },
  {
    timestamps: {
      createdAt: 'createdAt',
    },
  }
);

SickDoctorSchema.index({ sick: 1, doctor: 1 }, { unique: true });
module.exports = mongoose.model('SickDoctor', SickDoctorSchema);
