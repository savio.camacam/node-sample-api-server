const mongoose = require('mongoose');

const { Schema } = mongoose;

const DoctorSurveySchema = new Schema(
  {
    survey: {
      type: Schema.Types.ObjectId,
      ref: 'Survey',
      required: true,
    },
    doctor: {
      type: Schema.Types.ObjectId,
      ref: 'Doctor',
      required: true,
    },
    date: {
      type: Schema.Types.Date,
    },
    status: {
      type: String,
      enum: ['waiting', 'accepted', 'denied', 'closed', 'progress'],
      default: 'waiting',
      required: true,
    },
  },
  {
    timestamps: {
      createdAt: 'createdAt',
    },
  }
);

DoctorSurveySchema.index({ survey: 1, doctor: 1 }, { unique: true });
module.exports = mongoose.model('DoctorSurvey', DoctorSurveySchema);
