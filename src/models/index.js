const Admin = require('../domains/Admin/admin.model');
const Contact = require('../domains/Contact/contact.model');
const Disease = require('../domains/Disease/disease.model');
const Device = require('../domains/Device/device.model');
const Doctor = require('../domains/Doctor/doctor.model');
const DoctorDevice = require('./doctor-device.model');
const DoctorDisease = require('./doctor-disease.model');
const DoctorSurvey = require('./doctor-survey.model');
const DoctorTreatment = require('./doctor-treatment.model');
const People = require('./people.model');
const Profile = require('./profile.model');
const Question = require('../domains/Question/question.model');
const Session = require('./session.model');
const Sick = require('../domains/Sick/sick.model');
const SickDoctor = require('./sick-doctor.model');
const SickQuestion = require('./sick-question.model');
const SickDisease = require('./sick-disease.model');
const SickSurvey = require('./sick-survey.model');
const SickTreatment = require('./sick-treatment.model');
const Survey = require('../domains/Survey/survey.model');
const SurveyQuestion = require('./survey-question.model');
const Treatment = require('../domains/Treatment/treatment.model');
const TreatmentDisease = require('./treatment-disease.model');
const TreatmentSurvey = require('./treatment-survey.model');
const User = require('../domains/User/user.model');

module.exports = {
  Admin,
  Contact,
  Device,
  Disease,
  Doctor,
  DoctorDevice,
  DoctorDisease,
  DoctorSurvey,
  DoctorTreatment,
  People,
  Profile,
  Question,
  Session,
  Sick,
  SickDoctor,
  SickDisease,
  SickQuestion,
  SickSurvey,
  SickTreatment,
  Survey,
  SurveyQuestion,
  Treatment,
  TreatmentDisease,
  TreatmentSurvey,
  User,
};
