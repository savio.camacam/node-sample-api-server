const mongoose = require('mongoose');

const { Schema } = mongoose;

const TreatmentSurveySchema = new Schema(
  {
    treatment: {
      type: Schema.Types.ObjectId,
      ref: 'Treatment',
      required: true,
    },
    survey: {
      type: Schema.Types.ObjectId,
      ref: 'Survey',
      required: true,
    },
    status: {
      type: String,
      enum: ['waiting', 'accepted', 'denied', 'closed'],
      default: 'waiting',
      required: true,
    },
  },
  {
    timestamps: {
      createdAt: 'createdAt',
    },
  }
);

TreatmentSurveySchema.index({ treatment: 1, survey: 1 }, { unique: true });
module.exports = mongoose.model('TreatmentSurvey', TreatmentSurveySchema);
