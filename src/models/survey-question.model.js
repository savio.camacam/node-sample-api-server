const mongoose = require('mongoose');

const { Schema } = mongoose;

const SurveyQuestionSchema = new Schema(
  {
    question: {
      type: Schema.Types.ObjectId,
      ref: 'Question',
      required: true,
    },
    survey: {
      type: Schema.Types.ObjectId,
      ref: 'Survey',
      required: true,
    },
    index: {
      type: Schema.Types.Number,
      required: true,
    },
  },
  {
    timestamps: {
      createdAt: 'createdAt',
    },
  }
);

SurveyQuestionSchema.index({ question: 1, survey: 1 }, { unique: true });
module.exports = mongoose.model('SurveyQuestion', SurveyQuestionSchema);
