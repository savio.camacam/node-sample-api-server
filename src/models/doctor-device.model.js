const mongoose = require('mongoose');

const { Schema } = mongoose;

const DoctorDeviceSchema = new Schema(
  {
    device: {
      type: Schema.Types.ObjectId,
      ref: 'Device',
      required: true,
    },
    doctor: {
      type: Schema.Types.ObjectId,
      ref: 'Doctor',
      required: true,
    },
    status: {
      type: String,
      enum: ['waiting', 'accepted', 'denied', 'closed'],
      default: 'waiting',
      required: true,
    },
  },
  {
    timestamps: {
      createdAt: 'createdAt',
    },
  }
);
DoctorDeviceSchema.index({ device: 1, doctor: 1 }, { unique: true });

module.exports = mongoose.model('DoctorDevice', DoctorDeviceSchema);
