const mongoose = require('mongoose');

const { Schema } = mongoose;

const SickSurveySchema = new Schema(
  {
    sick: {
      type: Schema.Types.ObjectId,
      ref: 'Sick',
      required: true,
    },
    survey: {
      type: Schema.Types.ObjectId,
      ref: 'Survey',
      required: true,
    },
    date: {
      type: Schema.Types.Date,
    },
    status: {
      type: String,
      enum: ['waiting', 'accepted', 'denied', 'closed', 'progress'],
      default: 'waiting',
      required: true,
    },
  },
  {
    timestamps: {
      createdAt: 'createdAt',
    },
  }
);

SickSurveySchema.index({ sick: 1, survey: 1 }, { unique: true });
module.exports = mongoose.model('SickSurvey', SickSurveySchema);
