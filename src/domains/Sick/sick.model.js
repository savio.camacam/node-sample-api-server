const mongoose = require('mongoose');
const Profile = require('../../models/profile.model');

const { Schema } = mongoose;

const SickSchema = new Schema(
  {
    att: {
      type: Schema.Types.ObjectId,
      ref: 'Model',
    },
  },
  {
    timestamps: {
      createdAt: 'createdAt',
    },
  }
);

SickSchema.index({ __t: 1, user: 1 }, { unique: true });

module.exports = Profile.discriminator('Sick', SickSchema);
