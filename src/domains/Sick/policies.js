/* eslint-disable no-unused-vars */
const policies = [];

/* ***************************************************************** */
/* ***************************************************************** */
/* ***************************************************************** */
/* ********************* Parent checkers ******************** */
/* ***************************************************************** */
/* ***************************************************************** */
/* ***************************************************************** */

policies.push({
  profileType: 'ProfileParent',
  resourceType: '',
  permissionName: '',
  action: 'read',
  checker: async (_profile, _payload) => true,
});

policies.push({
  profileType: 'ProfileParent',
  resourceType: '',
  permissionName: '',
  action: 'write',
  checker: async (_profile, _payload) => true,
});

policies.push({
  profileType: 'ProfileParent',
  resourceType: '',
  permissionName: '',
  action: 'delete',
  checker: async (_profile, _payload) => true,
});

/* ***************************************************************** */
/* ***************************************************************** */
/* ***************************************************************** */
/* ********************* Professor checkers ******************** */
/* ***************************************************************** */
/* ***************************************************************** */
/* ***************************************************************** */

policies.push({
  profileType: 'ProfileProfessor',
  resourceType: '',
  permissionName: '',
  action: 'read',
  checker: async (_profile, _payload) => true,
});

policies.push({
  profileType: 'ProfileProfessor',
  resourceType: '',
  permissionName: '',
  action: 'write',
  checker: async (_profile, _payload) => true,
});

policies.push({
  profileType: 'ProfileProfessor',
  resourceType: '',
  permissionName: '',
  action: 'delete',
  checker: async (_profile, _payload) => true,
});

/* ***************************************************************** */
/* ***************************************************************** */
/* ***************************************************************** */
/* ********************* School checkers ******************** */
/* ***************************************************************** */
/* ***************************************************************** */
/* ***************************************************************** */

policies.push({
  profileType: 'ProfileSchool',
  resourceType: '',
  permissionName: '',
  action: 'read',
  checker: async (_profile, _payload) => true,
});

policies.push({
  profileType: 'ProfileSchool',
  resourceType: '',
  permissionName: '',
  action: 'write',
  checker: async (_profile, _payload) => true,
});

policies.push({
  profileType: 'ProfileSchool',
  resourceType: '',
  permissionName: '',
  action: 'delete',
  checker: async (_profile, _payload) => true,
});

/* ***************************************************************** */
/* ***************************************************************** */
/* ***************************************************************** */
/* ********************* County checkers ******************** */
/* ***************************************************************** */
/* ***************************************************************** */
/* ***************************************************************** */

policies.push({
  profileType: 'ProfileCounty',
  resourceType: '',
  permissionName: '',
  action: 'read',
  checker: async (_profile, _payload) => true,
});

policies.push({
  profileType: 'ProfileCounty',
  resourceType: '',
  permissionName: '',
  action: 'write',
  checker: async (_profile, _payload) => true,
});

policies.push({
  profileType: 'ProfileCounty',
  resourceType: '',
  permissionName: '',
  action: 'delete',
  checker: async (_profile, _payload) => true,
});

/* ***************************************************************** */
/* ***************************************************************** */
/* ***************************************************************** */
/* ********************* Administrator checkers ******************** */
/* ***************************************************************** */
/* ***************************************************************** */
/* ***************************************************************** */

policies.push({
  profileType: 'ProfileAdmin',
  resourceType: '',
  permissionName: '',
  action: 'read',
  checker: async (_profile, _payload) => true,
});

policies.push({
  profileType: 'ProfileAdmin',
  resourceType: '',
  permissionName: '',
  action: 'write',
  checker: async (_profile, _payload) => true,
});

policies.push({
  profileType: 'ProfileAdmin',
  resourceType: '',
  permissionName: '',
  action: 'delete',
  checker: async (_profile, _payload) => true,
});
module.exports = policies;
