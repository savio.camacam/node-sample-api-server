const { Types } = require('mongoose');
const models = require('../../models');
const responses = require('../../utils/responses');
// const { permissionGuard } = require('../../utils/permissions');
const { paginate, addFields, makeLookup } = require('../../utils/aggregations');

module.exports = {
  /**
   * TODO Implement
   * TODO Complete docs
   * TODO Add permission
   */

  /**
   * @swagger
   *
   * /sick:
   *   post:
   *     description: Update push notification reference
   *     tags:
   *       - Sick
   *     security:
   *       - JWT: []
   *     produces:
   *       - application/json
   *     requestBody:
   *       required: true
   *       content:
   *         application/json:
   *           schema:
   *             type: object
   *             properties:
   *               att1:
   *                 type: string
   *                 example: old
   *               att2:
   *                 type: string
   *                 example: new
   *
   *     responses:
   *       200:
   *         description: Success
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/schemas/Response'
   *             example:
   *               message: 'SUCCESS'
   *               data: xxxxxxxxxxxxxxxxxxxxxxxx
   *
   *       400:
   *         $ref: '#/components/responses/BadRequest'
   *       401:
   *         $ref: '#/components/responses/Unauthorized'
   *       404:
   *         $ref: '#/components/responses/NotFound'
   *       Default:
   *         $ref: '#/components/responses/DefaultError'
   */
  async add(req, res, next) {
    try {
      const { session } = req;

      res.status(responses.codes.SUCCESS).json({
        message: responses.messages.SUCCESS,
        data: null,
      });
      next();
    } catch (error) {
      switch (error.message) {
        case 'SENTINEL_NOT_FOUND':
          res.status(responses.codes.BAD_REQUEST).send({ message: error.message });
          break;
        default:
      }
      next(error);
    }
  },

  /**
   * TODO Implement
   * TODO Complete docs
   * TODO Add permission
   */

  /**
   * @swagger
   *
   * /sick/{sickId}/doctor:
   *   post:
   *     description: Update push notification reference
   *     tags:
   *       - Sick
   *     security:
   *       - JWT: []
   *     produces:
   *       - application/json
   *     requestBody:
   *       required: true
   *       content:
   *         application/json:
   *           schema:
   *             type: object
   *             properties:
   *               att1:
   *                 type: string
   *                 example: old
   *               att2:
   *                 type: string
   *                 example: new
   *
   *     responses:
   *       200:
   *         description: Success
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/schemas/Response'
   *             example:
   *               message: 'SUCCESS'
   *               data: xxxxxxxxxxxxxxxxxxxxxxxx
   *
   *       400:
   *         $ref: '#/components/responses/BadRequest'
   *       401:
   *         $ref: '#/components/responses/Unauthorized'
   *       404:
   *         $ref: '#/components/responses/NotFound'
   *       Default:
   *         $ref: '#/components/responses/DefaultError'
   */
  async addDoctor(req, res, next) {
    try {
      const { session } = req;

      res.status(responses.codes.SUCCESS).json({
        message: responses.messages.SUCCESS,
        data: null,
      });
      next();
    } catch (error) {
      switch (error.message) {
        case 'SENTINEL_NOT_FOUND':
          res.status(responses.codes.BAD_REQUEST).send({ message: error.message });
          break;
        default:
      }
      next(error);
    }
  },

  /**
   * TODO Implement
   * TODO Complete docs
   * TODO Add permission
   */

  /**
   * @swagger
   *
   * /sick/{sickId}/treatment:
   *   post:
   *     description: Update push notification reference
   *     tags:
   *       - Sick
   *     security:
   *       - JWT: []
   *     produces:
   *       - application/json
   *     requestBody:
   *       required: true
   *       content:
   *         application/json:
   *           schema:
   *             type: object
   *             properties:
   *               att1:
   *                 type: string
   *                 example: old
   *               att2:
   *                 type: string
   *                 example: new
   *
   *     responses:
   *       200:
   *         description: Success
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/schemas/Response'
   *             example:
   *               message: 'SUCCESS'
   *               data: xxxxxxxxxxxxxxxxxxxxxxxx
   *
   *       400:
   *         $ref: '#/components/responses/BadRequest'
   *       401:
   *         $ref: '#/components/responses/Unauthorized'
   *       404:
   *         $ref: '#/components/responses/NotFound'
   *       Default:
   *         $ref: '#/components/responses/DefaultError'
   */
  async addTreatment(req, res, next) {
    try {
      const { session } = req;

      res.status(responses.codes.SUCCESS).json({
        message: responses.messages.SUCCESS,
        data: null,
      });
      next();
    } catch (error) {
      switch (error.message) {
        case 'SENTINEL_NOT_FOUND':
          res.status(responses.codes.BAD_REQUEST).send({ message: error.message });
          break;
        default:
      }
      next(error);
    }
  },

  /**
   * TODO Implement
   * TODO Complete docs
   * TODO Add permission
   */

  /**
   * @swagger
   *
   * /sick/{sickId}/survey:
   *   post:
   *     description: Update push notification reference
   *     tags:
   *       - Sick
   *     security:
   *       - JWT: []
   *     produces:
   *       - application/json
   *     requestBody:
   *       required: true
   *       content:
   *         application/json:
   *           schema:
   *             type: object
   *             properties:
   *               att1:
   *                 type: string
   *                 example: old
   *               att2:
   *                 type: string
   *                 example: new
   *
   *     responses:
   *       200:
   *         description: Success
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/schemas/Response'
   *             example:
   *               message: 'SUCCESS'
   *               data: xxxxxxxxxxxxxxxxxxxxxxxx
   *
   *       400:
   *         $ref: '#/components/responses/BadRequest'
   *       401:
   *         $ref: '#/components/responses/Unauthorized'
   *       404:
   *         $ref: '#/components/responses/NotFound'
   *       Default:
   *         $ref: '#/components/responses/DefaultError'
   */
  async addSurveyAnswer(req, res, next) {
    try {
      const { session } = req;
      const { sickId: sick, sickSurveyId } = req.params;

      const { answers } = req.body;

      const sickSurveyUpdated = await models.SickSurvey.findByIdAndUpdate(
        sickSurveyId,
        { $set: { status: 'progress' } },
        { session }
      );

      await Promise.all(
        answers.map(async ({ survey, question, answer }) => {
          return models.SickQuestion.updateOne(
            {
              survey: Types.ObjectId(survey),
              sick: Types.ObjectId(sick),
              question: Types.ObjectId(question),
            },
            { $set: { answer } },
            { session }
          );
        })
      );

      res.status(responses.codes.SUCCESS).json({
        message: responses.messages.SUCCESS,
        data: sickSurveyUpdated,
      });
      next();
    } catch (error) {
      switch (error.message) {
        case 'SENTINEL_NOT_FOUND':
          res.status(responses.codes.BAD_REQUEST).send({ message: error.message });
          break;
        default:
      }
      next(error);
    }
  },

  /**
   * TODO Implement
   * TODO Complete docs
   * TODO Add permission
   */

  /**
   * @swagger
   *
   * /sick/{sickId}/disease:
   *   post:
   *     description: Update push notification reference
   *     tags:
   *       - Sick
   *     security:
   *       - JWT: []
   *     produces:
   *       - application/json
   *     requestBody:
   *       required: true
   *       content:
   *         application/json:
   *           schema:
   *             type: object
   *             properties:
   *               att1:
   *                 type: string
   *                 example: old
   *               att2:
   *                 type: string
   *                 example: new
   *
   *     responses:
   *       200:
   *         description: Success
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/schemas/Response'
   *             example:
   *               message: 'SUCCESS'
   *               data: xxxxxxxxxxxxxxxxxxxxxxxx
   *
   *       400:
   *         $ref: '#/components/responses/BadRequest'
   *       401:
   *         $ref: '#/components/responses/Unauthorized'
   *       404:
   *         $ref: '#/components/responses/NotFound'
   *       Default:
   *         $ref: '#/components/responses/DefaultError'
   */
  async addDisease(req, res, next) {
    try {
      const { session } = req;

      res.status(responses.codes.SUCCESS).json({
        message: responses.messages.SUCCESS,
        data: null,
      });
      next();
    } catch (error) {
      switch (error.message) {
        case 'SENTINEL_NOT_FOUND':
          res.status(responses.codes.BAD_REQUEST).send({ message: error.message });
          break;
        default:
      }
      next(error);
    }
  },

  /**
   * TODO Implement
   * TODO Complete docs
   * TODO Add permission
   */

  /**
   * @swagger
   *
   * /sick:
   *   get:
   *     description: Get all Sicks
   *     tags:
   *       - Sick
   *     security:
   *       - JWT: []
   *       - Requesting: []
   *     produces:
   *       - application/json
   *     parameters:
   *       - $ref: '#/components/parameters/Pagination'
   *       - in: query
   *         name: checked
   *
   *         schema:
   *           type: string
   *       - in: query
   *         name: type
   *
   *         schema:
   *           type: string
   *       - in: query
   *         name: user
   *
   *         schema:
   *           type: string
   *       - in: query
   *         name: createdAt
   *
   *         schema:
   *           type: string
   *       - in: query
   *         name: updatedAt
   *
   *         schema:
   *           type: string
   *
   *     responses:
   *       200:
   *         description: Success
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/schemas/Response'
   *             example:
   *               message: SUCCESS
   *               metadata:
   *                 total: 1826
   *                 page: 1
   *                 limit: 5
   *               data:
   *                 - _id: xxxxxxxxxxxxxxxxxxxxxxxx
   *                   checked: true
   *                   address: 1234567890
   *                   createdAt: 2019-02-18T22:31:48.039Z
   *                   updatedAt: 2019-02-18T22:31:48.039Z
   *                   user: "@user"
   *       400:
   *         $ref: '#/components/responses/BadRequest'
   *       401:
   *         $ref: '#/components/responses/Unauthorized'
   *       404:
   *         $ref: '#/components/responses/NotFound'
   *       Default:
   *         $ref: '#/components/responses/DefaultError'
   */
  async get(req, res, next) {
    try {
      const { session } = req;
      const { pagination } = req.query;

      const result = await paginate(models.Sick, pagination, { session }, [
        ...makeLookup('users', 'user', '_id', 'userObj', true, true),
        ...makeLookup('peoples', 'userObj._id', 'user', 'peopleObj', true, true),
        { $unset: ['userObj.password'] },
      ]);
      res.status(responses.codes.SUCCESS).json({
        message: responses.messages.SUCCESS,
        ...result,
      });
      next();
    } catch (error) {
      switch (error.message) {
        case 'SENTINEL_NOT_FOUND':
          res.status(responses.codes.BAD_REQUEST).send({ message: error.message });
          break;
        default:
      }
      next(error);
    }
  },

  /**
   * TODO Implement
   * TODO Complete docs
   * TODO Add permission
   */

  /**
   * @swagger
   *
   * /sick/{sickId}/surveys:
   *   get:
   *     description: Get all Sicks
   *     tags:
   *       - Sick
   *     security:
   *       - JWT: []
   *       - Requesting: []
   *     produces:
   *       - application/json
   *     parameters:
   *       - $ref: '#/components/parameters/Pagination'
   *       - in: query
   *         name: checked
   *
   *         schema:
   *           type: string
   *       - in: query
   *         name: type
   *
   *         schema:
   *           type: string
   *       - in: query
   *         name: user
   *
   *         schema:
   *           type: string
   *       - in: query
   *         name: createdAt
   *
   *         schema:
   *           type: string
   *       - in: query
   *         name: updatedAt
   *
   *         schema:
   *           type: string
   *
   *     responses:
   *       200:
   *         description: Success
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/schemas/Response'
   *             example:
   *               message: SUCCESS
   *               metadata:
   *                 total: 1826
   *                 page: 1
   *                 limit: 5
   *               data:
   *                 - _id: xxxxxxxxxxxxxxxxxxxxxxxx
   *                   checked: true
   *                   address: 1234567890
   *                   createdAt: 2019-02-18T22:31:48.039Z
   *                   updatedAt: 2019-02-18T22:31:48.039Z
   *                   user: "@user"
   *       400:
   *         $ref: '#/components/responses/BadRequest'
   *       401:
   *         $ref: '#/components/responses/Unauthorized'
   *       404:
   *         $ref: '#/components/responses/NotFound'
   *       Default:
   *         $ref: '#/components/responses/DefaultError'
   */
  async getSurveys(req, res, next) {
    try {
      const { session } = req;
      const { sickId } = req.params;
      const { pagination, sickTreatment } = req.query;

      const sickTreatmentFound = await models.SickTreatment.findById(sickTreatment, null, {
        session,
      });
      const treatment = await models.Treatment.findById(sickTreatmentFound.treatment, null, {
        session,
      });

      const result = await paginate(models.SickSurvey, pagination, { session }, [
        ...makeLookup('treatmentsurveys', 'survey', 'survey', 'treatmentsurveyObj', true, true),
        addFields([{ prop: 'sick', type: 'String' }]),
        { $match: { sick: sickId, 'treatmentsurveyObj.treatment': treatment._id } },
        addFields([{ prop: 'sick', type: 'ObjectId' }]),
        ...makeLookup('sicksurveys', 'survey', 'survey', 'sicksurveyObj', true, true),
        ...makeLookup('profiles', 'sicksurveyObj.sick', '_id', 'sickObj', true, true),
        ...makeLookup('users', 'sickObj.user', '_id', 'sickObj.userObj', true),
        ...makeLookup('peoples', 'sickObj.userObj.people', '_id', 'sickObj.peopleObj', true),
        ...makeLookup('surveys', 'survey', '_id', 'surveyObj', true, true),
        ...makeLookup('profiles', 'surveyObj.owner', '_id', 'ownerObj', true),
        ...makeLookup('users', 'ownerObj.user', '_id', 'userObj', true),
        ...makeLookup('peoples', 'userObj.people', '_id', 'peopleObj', true),
        ...makeLookup('surveyquestions', 'survey', 'survey', 'surveyquestionsObj', false),
        {
          $addFields: {
            surveyQuestionSize: { $size: '$surveyquestionsObj' },
            surveyOwnerName: {
              $concat: ['$peopleObj.name', ' (', '$userObj.shortName', ') '],
            },
            sickName: {
              $concat: ['$sickObj.peopleObj.name', ' (', '$sickObj.userObj.shortName', ') '],
            },
          },
        },
        {
          $unset: ['sickObj', 'userObj', 'peopleObj', 'ownerObj', 'surveyquestionsObj'],
        },
        { $sort: { 'sicksurveyObj.date': -1, 'sicksurveyObj.updatedAt': -1 } },
      ]);
      res.status(responses.codes.SUCCESS).json({
        message: responses.messages.SUCCESS,
        ...result,
      });
      next();
    } catch (error) {
      switch (error.message) {
        case 'SENTINEL_NOT_FOUND':
          res.status(responses.codes.BAD_REQUEST).send({ message: error.message });
          break;
        default:
      }
      next(error);
    }
  },

  /**
   * @swagger
   *
   * /sick/{sickId}/survey/{sickSurveyId}:
   *   get:
   *     description: Get an Survey
   *     tags:
   *       - Survey
   *     security:
   *       - JWT: []
   *       - Requesting: []
   *     produces:
   *       - application/json
   *     parameters:
   *       - in: path
   *         name: sickSurveyId
   *         required: true
   *         schema:
   *           type: string
   *
   *     responses:
   *       200:
   *         description: Success
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/schemas/Response'
   *             example:
   *               message: SUCCESS
   *               data:
   *                 _id: xxxxxxxxxxxxxxxxxxxxxxxx
   *                 att1: true
   *                 att2: text
   *
   *       400:
   *         $ref: '#/components/responses/BadRequest'
   *       401:
   *         $ref: '#/components/responses/Unauthorized'
   *       404:
   *         $ref: '#/components/responses/NotFound'
   *       Default:
   *         $ref: '#/components/responses/DefaultError'
   */
  async getSurvey(req, res, next) {
    try {
      const { session } = req;
      const { sickSurveyId, sickId } = req.params;

      const [sickSurvey] = await models.SickSurvey.aggregate([
        { $match: { _id: Types.ObjectId(sickSurveyId), sick: Types.ObjectId(sickId) } },
        ...makeLookup('sicksurveys', 'survey', 'survey', 'sicksurveyObj', true, true),
        ...makeLookup('profiles', 'sicksurveyObj.sick', '_id', 'sickObj', true, true),
        ...makeLookup('users', 'sickObj.user', '_id', 'sickObj.userObj', true),
        ...makeLookup('peoples', 'sickObj.userObj.people', '_id', 'sickObj.peopleObj', true),
        ...makeLookup('surveys', 'survey', '_id', 'surveyObj', true, true),
        ...makeLookup('profiles', 'surveyObj.owner', '_id', 'ownerObj', true),
        ...makeLookup('users', 'ownerObj.user', '_id', 'userObj', true),
        ...makeLookup('peoples', 'userObj.people', '_id', 'peopleObj', true),
        ...makeLookup('surveyquestions', 'survey', 'survey', 'surveyquestionsObj', false),
        {
          $addFields: {
            surveyQuestionSize: { $size: '$surveyquestionsObj' },
            surveyOwnerName: {
              $concat: ['$peopleObj.name', ' (', '$userObj.shortName', ') '],
            },
            sickName: {
              $concat: ['$sickObj.peopleObj.name', ' (', '$sickObj.userObj.shortName', ') '],
            },
          },
        },
        {
          $unset: ['sickObj', 'userObj', 'peopleObj', 'ownerObj'],
        },
      ]).option({ session });

      if (!sickSurvey) throw new Error('SICK_SURVEY_NOT_FOUND');

      res.status(responses.codes.SUCCESS).json({
        message: responses.messages.SUCCESS,
        data: sickSurvey,
      });
      next();
    } catch (error) {
      switch (error.message) {
        case 'SICK_SURVEY_NOT_FOUND':
          res.status(responses.codes.BAD_REQUEST).send({ message: error.message });
          break;
        default:
      }
      next(error);
    }
  },

  /**
   * @swagger
   *
   * /sick/{sickId}/survey/{sickSurveyId}/questions:
   *   get:
   *     description: Get an Survey
   *     tags:
   *       - Survey
   *     security:
   *       - JWT: []
   *       - Requesting: []
   *     produces:
   *       - application/json
   *     parameters:
   *       - in: path
   *         name: sickSurveyId
   *         required: true
   *         schema:
   *           type: string
   *
   *     responses:
   *       200:
   *         description: Success
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/schemas/Response'
   *             example:
   *               message: SUCCESS
   *               data:
   *                 _id: xxxxxxxxxxxxxxxxxxxxxxxx
   *                 att1: true
   *                 att2: text
   *
   *       400:
   *         $ref: '#/components/responses/BadRequest'
   *       401:
   *         $ref: '#/components/responses/Unauthorized'
   *       404:
   *         $ref: '#/components/responses/NotFound'
   *       Default:
   *         $ref: '#/components/responses/DefaultError'
   */
  async getSickSurveyQuestions(req, res, next) {
    try {
      const { session } = req;
      const { sickSurveyId, sickId } = req.params;

      const sickSurveyFound = await models.SickSurvey.findById(sickSurveyId, null, {
        session,
      });

      const sickSurveyQuestions = await models.Question.aggregate([
        ...makeLookup('sickquestions', '_id', 'question', 'sickquestionsObj', true),
        ...makeLookup('surveyquestions', '_id', 'question', 'surveyquestionsObj', true),
        {
          $match: {
            'surveyquestionsObj.survey': sickSurveyFound.survey,
            'sickquestionsObj.sick': Types.ObjectId(sickId),
            'sickquestionsObj.survey': sickSurveyFound.survey,
          },
        },
      ]).option({ session });

      if (!sickSurveyQuestions) throw new Error('SICK_SURVEYQUESTIONS_NOT_FOUND');

      res.status(responses.codes.SUCCESS).json({
        message: responses.messages.SUCCESS,
        data: sickSurveyQuestions,
      });
      next();
    } catch (error) {
      console.log(error);
      switch (error.message) {
        case 'SICK_SURVEY_QUESTIONS_NOT_FOUND':
          res.status(responses.codes.BAD_REQUEST).send({ message: error.message });
          break;
        default:
      }
      next(error);
    }
  },

  /**
   * TODO Implement
   * TODO Complete docs
   * TODO Add permission
   */

  /**
   * @swagger
   *
   * /sick/{sickId}/treatments:
   *   get:
   *     description: Get all Sicks
   *     tags:
   *       - Sick
   *     security:
   *       - JWT: []
   *       - Requesting: []
   *     produces:
   *       - application/json
   *     parameters:
   *       - $ref: '#/components/parameters/Pagination'
   *       - in: query
   *         name: checked
   *
   *         schema:
   *           type: string
   *       - in: query
   *         name: type
   *
   *         schema:
   *           type: string
   *       - in: query
   *         name: user
   *
   *         schema:
   *           type: string
   *       - in: query
   *         name: createdAt
   *
   *         schema:
   *           type: string
   *       - in: query
   *         name: updatedAt
   *
   *         schema:
   *           type: string
   *
   *     responses:
   *       200:
   *         description: Success
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/schemas/Response'
   *             example:
   *               message: SUCCESS
   *               metadata:
   *                 total: 1826
   *                 page: 1
   *                 limit: 5
   *               data:
   *                 - _id: xxxxxxxxxxxxxxxxxxxxxxxx
   *                   checked: true
   *                   address: 1234567890
   *                   createdAt: 2019-02-18T22:31:48.039Z
   *                   updatedAt: 2019-02-18T22:31:48.039Z
   *                   user: "@user"
   *       400:
   *         $ref: '#/components/responses/BadRequest'
   *       401:
   *         $ref: '#/components/responses/Unauthorized'
   *       404:
   *         $ref: '#/components/responses/NotFound'
   *       Default:
   *         $ref: '#/components/responses/DefaultError'
   */
  async getTreatments(req, res, next) {
    try {
      const { session } = req;
      const { pagination } = req.query;
      const { sickId } = req.params;

      const result = await paginate(models.SickTreatment, pagination, { session }, [
        addFields([{ prop: 'sick', type: 'String' }]),
        { $match: { sick: sickId } },
        addFields([{ prop: 'sick', type: 'ObjectId' }]),
        ...makeLookup('treatments', 'treatment', '_id', 'treatmentObj', true, true),
        ...makeLookup('profiles', 'treatmentObj.owner', '_id', 'doctorObj', true, true),
        ...makeLookup('users', 'doctorObj.user', '_id', 'doctorObj.userObj', true),
        ...makeLookup('peoples', 'doctorObj.userObj.people', '_id', 'doctorObj.peopleObj', true),
        ...makeLookup('profiles', 'sick', '_id', 'sickObj', true),
        ...makeLookup('users', 'sickObj.user', '_id', 'userObj', true),
        ...makeLookup('peoples', 'userObj.people', '_id', 'peopleObj', true),
        {
          $addFields: {
            sickName: {
              $concat: ['$peopleObj.name', ' (', '$userObj.shortName', ') '],
            },
            doctorName: {
              $concat: ['$doctorObj.peopleObj.name', ' (', '$doctorObj.userObj.shortName', ') '],
            },
          },
        },
        { $unset: ['doctorObj', 'sickObj', 'userObj', 'peopleObj'] },
      ]);
      res.status(responses.codes.SUCCESS).json({
        message: responses.messages.SUCCESS,
        ...result,
      });
      next();
    } catch (error) {
      switch (error.message) {
        case 'SENTINEL_NOT_FOUND':
          res.status(responses.codes.BAD_REQUEST).send({ message: error.message });
          break;
        default:
      }
      next(error);
    }
  },

  /**
   * @swagger
   *
   * /sick/{sickId}/treatment/{sickTreatmentId}:
   *   get:
   *     description: Get an SickTreatment
   *     tags:
   *       - SickTreatment
   *     security:
   *       - JWT: []
   *       - Requesting: []
   *     produces:
   *       - application/json
   *     parameters:
   *       - in: path
   *         name: sickTreatmentId
   *         required: true
   *         schema:
   *           type: string
   *
   *     responses:
   *       200:
   *         description: Success
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/schemas/Response'
   *             example:
   *               message: SUCCESS
   *               data:
   *                 _id: xxxxxxxxxxxxxxxxxxxxxxxx
   *                 att1: true
   *                 att2: text
   *
   *       400:
   *         $ref: '#/components/responses/BadRequest'
   *       401:
   *         $ref: '#/components/responses/Unauthorized'
   *       404:
   *         $ref: '#/components/responses/NotFound'
   *       Default:
   *         $ref: '#/components/responses/DefaultError'
   */
  async getTreatment(req, res, next) {
    try {
      const { session } = req;
      const { sickTreatmentId, sickId } = req.params;

      const [sickTreatment] = await models.SickTreatment.aggregate([
        { $match: { _id: Types.ObjectId(sickTreatmentId), sick: Types.ObjectId(sickId) } },
        ...makeLookup('treatments', 'treatment', '_id', 'treatmentObj', true),
        ...makeLookup('profiles', 'treatmentObj.owner', '_id', 'doctorObj', true, true),
        ...makeLookup('users', 'doctorObj.user', '_id', 'doctorObj.userObj', true),
        ...makeLookup('peoples', 'doctorObj.userObj.people', '_id', 'doctorObj.peopleObj', true),
        ...makeLookup('profiles', 'sick', '_id', 'sickObj', true),
        ...makeLookup('users', 'sickObj.user', '_id', 'userObj', true),
        ...makeLookup('peoples', 'userObj.people', '_id', 'peopleObj', true),
        {
          $addFields: {
            sickName: {
              $concat: ['$peopleObj.name', ' (', '$userObj.shortName', ') '],
            },
            doctorName: {
              $concat: ['$doctorObj.peopleObj.name', ' (', '$doctorObj.userObj.shortName', ') '],
            },
          },
        },
        { $unset: ['doctorObj', 'sickObj', 'peopleObj', 'userObj'] },
      ]).option({ session });

      if (!sickTreatment) throw new Error('SICK_TREATMENT_NOT_FOUND');

      res.status(responses.codes.SUCCESS).json({
        message: responses.messages.SUCCESS,
        data: sickTreatment,
      });
      next();
    } catch (error) {
      switch (error.message) {
        case 'SICK_TREATMENT_NOT_FOUND':
          res.status(responses.codes.BAD_REQUEST).send({ message: error.message });
          break;
        default:
      }
      next(error);
    }
  },

  /**
   * TODO Implement
   * TODO Complete docs
   * TODO Add permission
   */

  /**
   * @swagger
   *
   * /sick/{sickId}/diseases:
   *   get:
   *     description: Get all Sicks
   *     tags:
   *       - Sick
   *     security:
   *       - JWT: []
   *       - Requesting: []
   *     produces:
   *       - application/json
   *     parameters:
   *       - $ref: '#/components/parameters/Pagination'
   *       - in: query
   *         name: checked
   *
   *         schema:
   *           type: string
   *       - in: query
   *         name: type
   *
   *         schema:
   *           type: string
   *       - in: query
   *         name: user
   *
   *         schema:
   *           type: string
   *       - in: query
   *         name: createdAt
   *
   *         schema:
   *           type: string
   *       - in: query
   *         name: updatedAt
   *
   *         schema:
   *           type: string
   *
   *     responses:
   *       200:
   *         description: Success
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/schemas/Response'
   *             example:
   *               message: SUCCESS
   *               metadata:
   *                 total: 1826
   *                 page: 1
   *                 limit: 5
   *               data:
   *                 - _id: xxxxxxxxxxxxxxxxxxxxxxxx
   *                   checked: true
   *                   address: 1234567890
   *                   createdAt: 2019-02-18T22:31:48.039Z
   *                   updatedAt: 2019-02-18T22:31:48.039Z
   *                   user: "@user"
   *       400:
   *         $ref: '#/components/responses/BadRequest'
   *       401:
   *         $ref: '#/components/responses/Unauthorized'
   *       404:
   *         $ref: '#/components/responses/NotFound'
   *       Default:
   *         $ref: '#/components/responses/DefaultError'
   */
  async getDiseases(req, res, next) {
    try {
      const { session } = req;

      res.status(responses.codes.SUCCESS).json({
        message: responses.messages.SUCCESS,
        data: null,
      });
      next();
    } catch (error) {
      switch (error.message) {
        case 'SENTINEL_NOT_FOUND':
          res.status(responses.codes.BAD_REQUEST).send({ message: error.message });
          break;
        default:
      }
      next(error);
    }
  },

  /**
   * TODO Implement
   * TODO Complete docs
   * TODO Add permission
   */

  /**
   * @swagger
   *
   * /sick/{sickId}/doctors:
   *   get:
   *     description: Get all Sicks
   *     tags:
   *       - Sick
   *     security:
   *       - JWT: []
   *       - Requesting: []
   *     produces:
   *       - application/json
   *     parameters:
   *       - $ref: '#/components/parameters/Pagination'
   *       - in: query
   *         name: checked
   *
   *         schema:
   *           type: string
   *       - in: query
   *         name: type
   *
   *         schema:
   *           type: string
   *       - in: query
   *         name: user
   *
   *         schema:
   *           type: string
   *       - in: query
   *         name: createdAt
   *
   *         schema:
   *           type: string
   *       - in: query
   *         name: updatedAt
   *
   *         schema:
   *           type: string
   *
   *     responses:
   *       200:
   *         description: Success
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/schemas/Response'
   *             example:
   *               message: SUCCESS
   *               metadata:
   *                 total: 1826
   *                 page: 1
   *                 limit: 5
   *               data:
   *                 - _id: xxxxxxxxxxxxxxxxxxxxxxxx
   *                   checked: true
   *                   address: 1234567890
   *                   createdAt: 2019-02-18T22:31:48.039Z
   *                   updatedAt: 2019-02-18T22:31:48.039Z
   *                   user: "@user"
   *       400:
   *         $ref: '#/components/responses/BadRequest'
   *       401:
   *         $ref: '#/components/responses/Unauthorized'
   *       404:
   *         $ref: '#/components/responses/NotFound'
   *       Default:
   *         $ref: '#/components/responses/DefaultError'
   */
  async getDoctors(req, res, next) {
    try {
      const { session } = req;
      const { pagination } = req.query;
      const { sickId } = req.params;

      const result = await paginate(models.SickDoctor, pagination, { session }, [
        addFields([{ prop: 'sick', type: 'String' }]),
        { $match: { sick: sickId } },
        addFields([{ prop: 'sick', type: 'ObjectId' }]),
        ...makeLookup('profiles', 'doctor', '_id', 'doctorObj', true, true),
        ...makeLookup('users', 'doctorObj.user', '_id', 'doctorObj.userObj', true),
        ...makeLookup('peoples', 'doctorObj.userObj.people', '_id', 'doctorObj.peopleObj', true),
        { $unset: ['doctorObj.userObj.password'] },
      ]);
      res.status(responses.codes.SUCCESS).json({
        message: responses.messages.SUCCESS,
        ...result,
      });
      next();
    } catch (error) {
      switch (error.message) {
        case 'SENTINEL_NOT_FOUND':
          res.status(responses.codes.BAD_REQUEST).send({ message: error.message });
          break;
        default:
      }
      next(error);
    }
  },

  /**
   * @swagger
   *
   * /sick/{sickId}/sick/{sickDoctorId}:
   *   get:
   *     description: Get an SickDoctor
   *     tags:
   *       - SickDoctor
   *     security:
   *       - JWT: []
   *       - Requesting: []
   *     produces:
   *       - application/json
   *     parameters:
   *       - in: path
   *         name: sickDoctorId
   *         required: true
   *         schema:
   *           type: string
   *
   *     responses:
   *       200:
   *         description: Success
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/schemas/Response'
   *             example:
   *               message: SUCCESS
   *               data:
   *                 _id: xxxxxxxxxxxxxxxxxxxxxxxx
   *                 att1: true
   *                 att2: text
   *
   *       400:
   *         $ref: '#/components/responses/BadRequest'
   *       401:
   *         $ref: '#/components/responses/Unauthorized'
   *       404:
   *         $ref: '#/components/responses/NotFound'
   *       Default:
   *         $ref: '#/components/responses/DefaultError'
   */
  async getDoctor(req, res, next) {
    try {
      const { session } = req;
      const { sickDoctorId, sickId } = req.params;

      const [sickDoctor] = await models.SickDoctor.aggregate([
        { $match: { _id: Types.ObjectId(sickDoctorId), sick: Types.ObjectId(sickId) } },
        ...makeLookup('profiles', 'doctor', '_id', 'doctorObj', true),
        ...makeLookup('users', 'doctorObj.user', '_id', 'userObj', true),
        ...makeLookup('peoples', 'userObj.people', '_id', 'peopleObj', true),
        {
          $addFields: {
            doctorName: {
              $concat: ['$peopleObj.name', ' (', '$userObj.shortName', ') '],
            },
          },
        },
        { $unset: ['userObj', 'peopleObj'] },
      ]).option({ session });

      if (!sickDoctor) throw new Error('SICK_DOCTOR_NOT_FOUND');

      res.status(responses.codes.SUCCESS).json({
        message: responses.messages.SUCCESS,
        data: sickDoctor,
      });
      next();
    } catch (error) {
      switch (error.message) {
        case 'SICK_DOCTOR_NOT_FOUND':
          res.status(responses.codes.BAD_REQUEST).send({ message: error.message });
          break;
        default:
      }
      next(error);
    }
  },

  /**
   * TODO Implement
   * TODO Complete docs
   * TODO Add permission
   */

  /**
   * @swagger
   *
   * /sick/{sentinelId}:
   *   get:
   *     description: Get an Sick
   *     tags:
   *       - Sick
   *     security:
   *       - JWT: []
   *       - Requesting: []
   *     produces:
   *       - application/json
   *     parameters:
   *       - in: path
   *         name: sentinelId
   *         required: true
   *         schema:
   *           type: string
   *
   *     responses:
   *       200:
   *         description: Success
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/schemas/Response'
   *             example:
   *               message: SUCCESS
   *               data:
   *                 _id: xxxxxxxxxxxxxxxxxxxxxxxx
   *                 att1: true
   *                 att2: text
   *
   *       400:
   *         $ref: '#/components/responses/BadRequest'
   *       401:
   *         $ref: '#/components/responses/Unauthorized'
   *       404:
   *         $ref: '#/components/responses/NotFound'
   *       Default:
   *         $ref: '#/components/responses/DefaultError'
   */
  async getById(req, res, next) {
    try {
      const { session } = req;
      const { sickId } = req.params;

      const [sick] = await models.Sick.aggregate([
        {
          $match: { _id: Types.ObjectId(sickId) },
        },
        ...makeLookup('users', 'user', '_id', 'userObj', true, true),
        ...makeLookup('peoples', 'userObj._id', 'user', 'peopleObj', true, true),
        { $unset: ['userObj.password'] },
      ]).option(session);
      res.status(responses.codes.SUCCESS).json({
        message: responses.messages.SUCCESS,
        data: sick,
      });
      next();
    } catch (error) {
      switch (error.message) {
        case 'SENTINEL_NOT_FOUND':
          res.status(responses.codes.BAD_REQUEST).send({ message: error.message });
          break;
        default:
      }
      next(error);
    }
  },
  /**
   * TODO Implement
   * TODO Complete docs
   * TODO Add permission
   */

  /**
   * @swagger
   *
   * /sick/{sentinelId}:
   *   delete:
   *     description: Delete a Sick
   *     tags:
   *       - Sick
   *     security:
   *       - JWT: []
   *       - Requesting: []
   *     produces:
   *       - application/json
   *     parameters:
   *       - in: path
   *         name: sentinelId
   *         required: true
   *         schema:
   *           type: string
   *
   *     responses:
   *       200:
   *         $ref: '#/components/responses/Success'
   *
   *       400:
   *         $ref: '#/components/responses/BadRequest'
   *       401:
   *         $ref: '#/components/responses/Unauthorized'
   *       404:
   *         $ref: '#/components/responses/NotFound'
   *       Default:
   *         $ref: '#/components/responses/DefaultError'
   */
  async delete(req, res, next) {
    try {
      const { session } = req;

      res.status(responses.codes.SUCCESS).json({
        message: responses.messages.SUCCESS,
        data: null,
      });
      next();
    } catch (error) {
      switch (error.message) {
        case 'SENTINEL_NOT_FOUND':
          res.status(responses.codes.BAD_REQUEST).send({ message: error.message });
          break;
        default:
      }
      next(error);
    }
  },

  /**
   * TODO Implement
   * TODO Complete docs
   * TODO Add permission
   */

  /**
   * @swagger
   *
   * /sick/{sentinelId}:
   *   put:
   *     description: Update a Sick
   *     tags:
   *       - Sick
   *     security:
   *       - JWT: []
   *       - Requesting: []
   *     produces:
   *       - application/json
   *     parameters:
   *       - in: path
   *         name: sentinelId
   *         required: true
   *         schema:
   *           type: string
   *     requestBody:
   *       required: true
   *       content:
   *         application/json:
   *           schema:
   *             type: object
   *             properties:
   *               email:
   *                 type: string
   *               phone:
   *                 type: string
   *           examples:
   *             Phone:
   *               value:
   *                 phone: "1234567890"
   *             Cellphone:
   *               value:
   *                 cellphone: "1234567890"
   *             Email:
   *               value:
   *                 email: email@domain.com
   *
   *     responses:
   *       200:
   *         $ref: '#/components/responses/Success'
   *
   *       400:
   *         $ref: '#/components/responses/BadRequest'
   *       401:
   *         $ref: '#/components/responses/Unauthorized'
   *       404:
   *         $ref: '#/components/responses/NotFound'
   *       Default:
   *         $ref: '#/components/responses/DefaultError'
   */
  async update(req, res, next) {
    try {
      const { session } = req;

      res.status(responses.codes.SUCCESS).json({
        message: responses.messages.SUCCESS,
        data: null,
      });
      next();
    } catch (error) {
      switch (error.message) {
        case 'SENTINEL_NOT_FOUND':
          res.status(responses.codes.BAD_REQUEST).send({ message: error.message });
          break;
        default:
      }
      next(error);
    }
  },

  /**
   * @swagger
   *
   * /sick/{sentinelId}:
   *   put:
   *     description: Update a Sick
   *     tags:
   *       - Sick
   *     security:
   *       - JWT: []
   *       - Requesting: []
   *     produces:
   *       - application/json
   *     parameters:
   *       - in: path
   *         name: sentinelId
   *         required: true
   *         schema:
   *           type: string
   *     requestBody:
   *       required: true
   *       content:
   *         application/json:
   *           schema:
   *             type: object
   *             properties:
   *               email:
   *                 type: string
   *               phone:
   *                 type: string
   *           examples:
   *             Phone:
   *               value:
   *                 phone: "1234567890"
   *             Cellphone:
   *               value:
   *                 cellphone: "1234567890"
   *             Email:
   *               value:
   *                 email: email@domain.com
   *
   *     responses:
   *       200:
   *         $ref: '#/components/responses/Success'
   *
   *       400:
   *         $ref: '#/components/responses/BadRequest'
   *       401:
   *         $ref: '#/components/responses/Unauthorized'
   *       404:
   *         $ref: '#/components/responses/NotFound'
   *       Default:
   *         $ref: '#/components/responses/DefaultError'
   */
  async updateDoctor(req, res, next) {
    try {
      const { session } = req;
      const { sickId, sickDoctorId } = req.params;
      const { status } = req.body;

      const rules = {
        accepted: ['closed', 'accepted'],
        waiting: ['accepted', 'denied', 'waiting'],
        denied: ['waiting', 'denied'],
        closed: ['accepted', 'closed'],
      };

      const sickDoctorUpdated = await models.SickDoctor.findOneAndUpdate(
        { sick: Types.ObjectId(sickId), _id: Types.ObjectId(sickDoctorId) },
        { $set: { status } },
        { session }
      );

      if (!sickDoctorUpdated) throw new Error('SICK_DOCTOR_NOT_FOUND');

      if (!rules[sickDoctorUpdated.status].includes(status)) {
        throw new Error('STATUS_NOT_ALLOWED');
      }

      res.status(responses.codes.SUCCESS).json({
        message: responses.messages.SUCCESS,
        data: sickDoctorUpdated,
      });
      next();
    } catch (error) {
      switch (error.message) {
        case 'STATUS_NOT_ALLOWED':
          res.status(responses.codes.BAD_REQUEST).send({ message: error.message });
          break;
        case 'SICK_DOCTOR_NOT_FOUND':
          res.status(responses.codes.BAD_REQUEST).send({ message: error.message });
          break;
        default:
      }
      next(error);
    }
  },
};
