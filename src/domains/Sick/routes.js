const router = require('express').Router();
const controller = require('./controller');
const validations = require('./validations');
const reportRoutes = require('./report/routes');
const auditRoutes = require('./audit/routes');
const auth = require('../../middlewares/authentication');

router.use('/audit', auditRoutes);
router.use('/report', reportRoutes);

router.post('/', validations.add(), auth.checkToken, controller.add);
router.post('/:sickId/doctor', validations.addDoctor(), auth.checkToken, controller.addDoctor);
router.post(
  '/:sickId/treatment',
  validations.addTreatment(),
  auth.checkToken,
  controller.addTreatment
);
router.post(
  '/:sickId/survey/:sickSurveyId/answer',
  validations.addSurveyAnswer(),
  auth.checkToken,
  controller.addSurveyAnswer
);
router.post('/:sickId/disease', validations.addDisease(), auth.checkToken, controller.addDisease);

router.get('/', validations.get(), auth.checkToken, controller.get);
router.get('/:sickId', auth.checkToken, controller.getById);
router.get('/:sickId/doctors', validations.getDoctors(), auth.checkToken, controller.getDoctors);
router.get('/:sickId/doctor/:sickDoctorId', auth.checkToken, controller.getDoctor);

router.get(
  '/:sickId/treatments',
  validations.getTreatments(),
  auth.checkToken,
  controller.getTreatments
);
router.get('/:sickId/treatment/:sickTreatmentId', auth.checkToken, controller.getTreatment);
router.get('/:sickId/surveys', validations.getSurveys(), auth.checkToken, controller.getSurveys);
router.get(
  '/:sickId/survey/:sickSurveyId/questions',
  auth.checkToken,
  controller.getSickSurveyQuestions
);
router.get('/:sickId/survey/:sickSurveyId', auth.checkToken, controller.getSurvey);
router.get('/:sickId/diseases', auth.checkToken, controller.getDiseases);

router.put(
  '/:sickId/doctor/:sickDoctorId',
  validations.updateDoctor(),
  auth.checkToken,
  controller.updateDoctor
);
router.put('/:sickId', validations.update(), auth.checkToken, controller.update);

module.exports = router;
