const { celebrate, Joi } = require('celebrate');
const { JoiPagination, JoiStatus } = require('../../utils/JoiModels');

module.exports = {
  add() {
    return celebrate({
      body: Joi.object()
        .keys({
          user: Joi.string().required(),
          diseases: Joi.array()
            .items(Joi.string())
            .required()
            .min(1),
        })
        .required(),
    });
  },
  addTreatment() {
    return celebrate({
      body: Joi.object()
        .keys({
          treatment: Joi.string().required(),
          disease: Joi.string().required(),
        })
        .required(),
    });
  },
  addSurveyAnswer() {
    return celebrate({
      body: Joi.object()
        .keys({
          answers: Joi.array().required(),
        })
        .required(),
    });
  },
  addDoctor() {
    return celebrate({
      body: Joi.object()
        .keys({
          doctor: Joi.string().required(),
        })
        .required(),
    });
  },
  addDisease() {
    return celebrate({
      body: Joi.object()
        .keys({
          disease: Joi.string().required(),
        })
        .required(),
    });
  },
  get() {
    return celebrate({
      query: Joi.object().keys({
        pagination: JoiPagination(1, 100),
      }),
    });
  },
  getTreatments() {
    return celebrate({
      query: Joi.object().keys({
        pagination: JoiPagination(1, 100),
      }),
    });
  },
  getDoctors() {
    return celebrate({
      query: Joi.object().keys({
        pagination: JoiPagination(1, 100),
      }),
    });
  },
  getSurveys() {
    return celebrate({
      query: Joi.object().keys({
        pagination: JoiPagination(1, 100),
        sickTreatment: Joi.string(),
      }),
    });
  },
  getDiseases() {
    return celebrate({
      query: Joi.object().keys({
        pagination: JoiPagination(1, 100),
      }),
    });
  },
  update() {
    return celebrate({
      body: Joi.object()
        .keys({})
        .required(),
    });
  },
  updateDoctor() {
    return celebrate({
      body: Joi.object()
        .keys({
          status: Joi.string().regex(JoiStatus),
        })
        .required(),
    });
  },
};
