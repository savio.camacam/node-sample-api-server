const { Types } = require('mongoose');
const models = require('../../models');
const responses = require('../../utils/responses');
const { permissionGuard } = require('../../utils/permissions');
const { paginate, makeLookup, addFields } = require('../../utils/aggregations');

module.exports = {
  /**
   * TODO Implement
   * TODO Complete docs
   * TODO Add permission
   */

  /**
   * @swagger
   *
   * /device:
   *   post:
   *     description: Update push notification reference
   *     tags:
   *       - Device
   *     security:
   *       - JWT: []
   *     produces:
   *       - application/json
   *     requestBody:
   *       required: true
   *       content:
   *         application/json:
   *           schema:
   *             type: object
   *             properties:
   *               att1:
   *                 type: string
   *                 example: old
   *               att2:
   *                 type: string
   *                 example: new
   *
   *     responses:
   *       200:
   *         description: Success
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/schemas/Response'
   *             example:
   *               message: 'SUCCESS'
   *               data: xxxxxxxxxxxxxxxxxxxxxxxx
   *
   *       400:
   *         $ref: '#/components/responses/BadRequest'
   *       401:
   *         $ref: '#/components/responses/Unauthorized'
   *       404:
   *         $ref: '#/components/responses/NotFound'
   *       Default:
   *         $ref: '#/components/responses/DefaultError'
   */
  async add(req, res, next) {
    try {
      const { session } = req;

      const device = await models.Device.create([{ ...req.body }], { session });

      res.status(responses.codes.SUCCESS).json({
        message: responses.messages.SUCCESS,
        data: device,
      });
      next();
    } catch (error) {
      switch (error.message) {
        case 'SENTINEL_NOT_FOUND':
          res.status(responses.codes.BAD_REQUEST).send({ message: error.message });
          break;
        default:
      }
      next(error);
    }
  },

  /**
   * TODO Implement
   * TODO Complete docs
   * TODO Add permission
   */

  /**
   * @swagger
   *
   * /device:
   *   get:
   *     description: Get all Devices
   *     tags:
   *       - Device
   *     security:
   *       - JWT: []
   *       - Requesting: []
   *     produces:
   *       - application/json
   *     parameters:
   *       - $ref: '#/components/parameters/Pagination'
   *       - in: query
   *         name: checked
   *
   *         schema:
   *           type: string
   *       - in: query
   *         name: type
   *
   *         schema:
   *           type: string
   *       - in: query
   *         name: user
   *
   *         schema:
   *           type: string
   *       - in: query
   *         name: createdAt
   *
   *         schema:
   *           type: string
   *       - in: query
   *         name: updatedAt
   *
   *         schema:
   *           type: string
   *
   *     responses:
   *       200:
   *         description: Success
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/schemas/Response'
   *             example:
   *               message: SUCCESS
   *               metadata:
   *                 total: 1826
   *                 page: 1
   *                 limit: 5
   *               data:
   *                 - _id: xxxxxxxxxxxxxxxxxxxxxxxx
   *                   checked: true
   *                   address: 1234567890
   *                   createdAt: 2019-02-18T22:31:48.039Z
   *                   updatedAt: 2019-02-18T22:31:48.039Z
   *                   user: "@user"
   *       400:
   *         $ref: '#/components/responses/BadRequest'
   *       401:
   *         $ref: '#/components/responses/Unauthorized'
   *       404:
   *         $ref: '#/components/responses/NotFound'
   *       Default:
   *         $ref: '#/components/responses/DefaultError'
   */
  async get(req, res, next) {
    try {
      const { session } = req;
      const { pagination } = req.query;

      const result = await paginate(models.Device, pagination, { session }, [
        { $match: {} },
        ...makeLookup('profiles', 'owner', '_id', 'ownerObj', true),
        ...makeLookup('users', 'ownerObj.user', '_id', 'userObj', true),
        ...makeLookup('peoples', 'userObj.people', '_id', 'peopleObj', true),
        {
          $addFields: {
            ownerName: {
              $concat: ['$peopleObj.name', ' (', '$userObj.shortName', ') '],
            },
          },
        },
        { $unset: ['userObj', 'peopleObj', 'ownerObj'] },
      ]);

      res.status(responses.codes.SUCCESS).json({
        message: responses.messages.SUCCESS,
        ...result,
      });
      next();
    } catch (error) {
      switch (error.message) {
        case 'SENTINEL_NOT_FOUND':
          res.status(responses.codes.BAD_REQUEST).send({ message: error.message });
          break;
        default:
      }
      next(error);
    }
  },

  /**
   * TODO Implement
   * TODO Complete docs
   * TODO Add permission
   */

  /**
   * @swagger
   *
   * /device/{deviceId}:
   *   get:
   *     description: Get an Device
   *     tags:
   *       - Device
   *     security:
   *       - JWT: []
   *       - Requesting: []
   *     produces:
   *       - application/json
   *     parameters:
   *       - in: path
   *         name: deviceId
   *         required: true
   *         schema:
   *           type: string
   *
   *     responses:
   *       200:
   *         description: Success
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/schemas/Response'
   *             example:
   *               message: SUCCESS
   *               data:
   *                 _id: xxxxxxxxxxxxxxxxxxxxxxxx
   *                 att1: true
   *                 att2: text
   *
   *       400:
   *         $ref: '#/components/responses/BadRequest'
   *       401:
   *         $ref: '#/components/responses/Unauthorized'
   *       404:
   *         $ref: '#/components/responses/NotFound'
   *       Default:
   *         $ref: '#/components/responses/DefaultError'
   */
  async getById(req, res, next) {
    try {
      const { session } = req;
      const { deviceId } = req.params;

      const [device] = await models.Device.aggregate([
        { $match: { _id: Types.ObjectId(deviceId) } },
        ...makeLookup('profiles', 'owner', '_id', 'ownerObj', true),
        ...makeLookup('users', 'ownerObj.user', '_id', 'userObj', true),
        ...makeLookup('peoples', 'userObj.people', '_id', 'peopleObj', true),
        {
          $addFields: {
            ownerName: {
              $concat: ['$peopleObj.name', ' (', '$userObj.shortName', ') '],
            },
          },
        },
        { $unset: ['userObj', 'peopleObj', 'ownerObj'] },
      ]).option({ session });

      if (!device) throw new Error('DEVICE_NOT_FOUND');

      let deviceOwner = {};
      if (device.owner) {
        deviceOwner = await models.DoctorDevice.findOne(
          { doctor: device.owner, device: device._id },
          null,
          {
            session,
          }
        );
        if (deviceOwner) {
          device.ownerStatus = deviceOwner.status;
        }
      }

      res.status(responses.codes.SUCCESS).json({
        message: responses.messages.SUCCESS,
        data: device,
      });
      next();
    } catch (error) {
      switch (error.message) {
        case 'DEVICE_NOT_FOUND':
          res.status(responses.codes.BAD_REQUEST).send({ message: error.message });
          break;
        default:
      }
      next(error);
    }
  },

  /**
   * TODO Implement
   * TODO Complete docs
   * TODO Add permission
   */

  /**
   * @swagger
   *
   * /device/{deviceId}:
   *   delete:
   *     description: Delete a Device
   *     tags:
   *       - Device
   *     security:
   *       - JWT: []
   *       - Requesting: []
   *     produces:
   *       - application/json
   *     parameters:
   *       - in: path
   *         name: deviceId
   *         required: true
   *         schema:
   *           type: string
   *
   *     responses:
   *       200:
   *         $ref: '#/components/responses/Success'
   *
   *       400:
   *         $ref: '#/components/responses/BadRequest'
   *       401:
   *         $ref: '#/components/responses/Unauthorized'
   *       404:
   *         $ref: '#/components/responses/NotFound'
   *       Default:
   *         $ref: '#/components/responses/DefaultError'
   */
  async delete(req, res, next) {
    try {
      const { session } = req;
      const { deviceId } = req.params;
      const device = await models.Device.findByIdAndDelete(deviceId, { session });
      if (!device) throw new Error('DEVICE_NOT_FOUND');

      await models.DoctorDevice.deleteMany({ device: deviceId }).session(session);

      res.status(responses.codes.SUCCESS).json({
        message: responses.messages.SUCCESS,
        data: device,
      });
      next();
    } catch (error) {
      switch (error.message) {
        case 'DEVICE_NOT_FOUND':
          res.status(responses.codes.BAD_REQUEST).send({ message: error.message });
          break;
        default:
      }
      next(error);
    }
  },

  /**
   * TODO Implement
   * TODO Complete docs
   * TODO Add permission
   */

  /**
   * @swagger
   *
   * /device/{deviceId}:
   *   put:
   *     description: Update a Device
   *     tags:
   *       - Device
   *     security:
   *       - JWT: []
   *       - Requesting: []
   *     produces:
   *       - application/json
   *     parameters:
   *       - in: path
   *         name: deviceId
   *         required: true
   *         schema:
   *           type: string
   *     requestBody:
   *       required: true
   *       content:
   *         application/json:
   *           schema:
   *             type: object
   *             properties:
   *               email:
   *                 type: string
   *               phone:
   *                 type: string
   *           examples:
   *             Phone:
   *               value:
   *                 phone: "1234567890"
   *             Cellphone:
   *               value:
   *                 cellphone: "1234567890"
   *             Email:
   *               value:
   *                 email: email@domain.com
   *
   *     responses:
   *       200:
   *         $ref: '#/components/responses/Success'
   *
   *       400:
   *         $ref: '#/components/responses/BadRequest'
   *       401:
   *         $ref: '#/components/responses/Unauthorized'
   *       404:
   *         $ref: '#/components/responses/NotFound'
   *       Default:
   *         $ref: '#/components/responses/DefaultError'
   */
  async update(req, res, next) {
    try {
      const { session } = req;
      const { deviceId } = req.params;
      const { ownerStatus: status, active, ...body } = req.body;

      const device = await models.Device.findByIdAndUpdate(
        deviceId,
        { active, ...body },
        { session }
      );

      if (!device) throw new Error('DEVICE_NOT_FOUND');

      if (!active) {
        await models.Device.findByIdAndUpdate(deviceId, { $unset: { owner: 1 } }, { session });
        await models.DoctorDevice.deleteMany({ device }).session(session);
      } else {
        await models.DoctorDevice.updateMany({ device }, { $set: { status } }, { session });
      }

      res.status(responses.codes.SUCCESS).json({
        message: responses.messages.SUCCESS,
        data: device,
      });
      next();
    } catch (error) {
      switch (error.message) {
        case 'DEVICE_NOT_FOUND':
          res.status(responses.codes.BAD_REQUEST).send({ message: error.message });
          break;
        default:
      }
      next(error);
    }
  },
};
