const { celebrate, Joi } = require('celebrate');
const { JoiPagination, JoiStatus } = require('../../utils/JoiModels');

module.exports = {
  add() {
    return celebrate({
      body: Joi.object()
        .keys({
          serialNumber: Joi.string().required(),
          mac: Joi.string().required(),
        })
        .required(),
    });
  },
  get() {
    return celebrate({
      query: Joi.object().keys({
        pagination: JoiPagination(1, 100),
      }),
    });
  },
  update() {
    return celebrate({
      body: Joi.object()
        .keys({
          serialNumber: Joi.string().required(),
          mac: Joi.string().required(),
          active: Joi.boolean(),
          ownerStatus: Joi.string().regex(JoiStatus),
        })
        .required(),
    });
  },
};
