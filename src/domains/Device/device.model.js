const mongoose = require('mongoose');

const { Schema } = mongoose;

const DeviceSchema = new Schema(
  {
    owner: {
      type: Schema.Types.ObjectId,
      ref: 'Doctor',
    },
    serialNumber: {
      type: Schema.Types.String,
      required: true,
      unique: true,
    },
    mac: { type: Schema.Types.String, required: true },
    active: {
      type: Schema.Types.Boolean,
      required: true,
      default: false,
    },
    shared: {
      type: Schema.Types.Boolean,
      required: true,
      default: false,
    },
  },
  {
    timestamps: {
      createdAt: 'createdAt',
    },
  }
);

module.exports = mongoose.model('Device', DeviceSchema);
