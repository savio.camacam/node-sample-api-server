const mongoose = require('mongoose');

const { Schema } = mongoose;

const EmailSchema = new Schema(
  {
    att: {
      type: Schema.Types.ObjectId,
      ref: 'Email',
    },
  },
  {
    timestamps: {
      createdAt: 'createdAt',
    },
  }
);

module.exports = mongoose.model('Email', EmailSchema);
