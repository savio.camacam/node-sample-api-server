const mongoose = require('mongoose');
const Profile = require('../../models/profile.model');

const { Schema } = mongoose;

const DoctorSchema = new Schema(
  {
    crm: {
      type: Schema.Types.String,
      required: true,
    },
  },
  {
    timestamps: {
      createdAt: 'createdAt',
    },
  }
);

DoctorSchema.index({ __t: 1, user: 1 }, { unique: true });

module.exports = Profile.discriminator('Doctor', DoctorSchema);
