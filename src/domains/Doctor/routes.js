const router = require('express').Router();
const controller = require('./controller');
const validations = require('./validations');
const reportRoutes = require('./report/routes');
const auditRoutes = require('./audit/routes');
const auth = require('../../middlewares/authentication');

router.use('/audit', auditRoutes);
router.use('/report', reportRoutes);

router.post('/', validations.add(), auth.checkToken, controller.add);
router.post('/:doctorId/sick', validations.addSick(), auth.checkToken, controller.addSick);
router.post(
  '/:doctorId/treatment',
  validations.addTreatment(),
  auth.checkToken,
  controller.addTreatment
);
router.post(
  '/:doctorId/treatment/:doctorTreatmentId/session',
  validations.addSessionToTreatment(),
  auth.checkToken,
  controller.addSessionToTreatment
);
router.post('/:doctorId/survey', validations.addSurvey(), auth.checkToken, controller.addSurvey);
router.post('/:doctorId/device', validations.addDevice(), auth.checkToken, controller.addDevice);
router.post('/:doctorId/disease', validations.addDisease(), auth.checkToken, controller.addDisease);

router.get('/', validations.get(), auth.checkToken, controller.get);
router.get('/:doctorId', auth.checkToken, controller.getById);
router.get('/:doctorId/sicks', validations.getSicks(), auth.checkToken, controller.getSicks);
router.get('/:doctorId/sick/:sickDoctorId', auth.checkToken, controller.getSick);

router.get(
  '/:doctorId/treatments',
  validations.getTreatments(),
  auth.checkToken,
  controller.getTreatments
);
router.get('/:doctorId/treatment/:doctorTreatmentId', auth.checkToken, controller.getTreatment);
router.get(
  '/:doctorId/treatment/:doctorTreatmentId/sessions',
  validations.getTreatmentSessions(),
  auth.checkToken,
  controller.getSessions
);
router.get(
  '/:doctorId/treatment/:doctorTreatmentId/session/:sessionId',
  auth.checkToken,
  controller.getSession
);

router.get('/:doctorId/surveys', validations.getSurveys(), auth.checkToken, controller.getSurveys);
router.get('/:doctorId/survey/:doctorSurveyId', auth.checkToken, controller.getSurvey);
router.get(
  '/:doctorId/survey/:doctorSurveyId/questions',
  auth.checkToken,
  controller.getDoctorSurveyQuestions
);

router.get('/:doctorId/devices', validations.getDevices(), auth.checkToken, controller.getDevices);
router.get('/:doctorId/device/:doctorDeviceId', auth.checkToken, controller.getDevice);

router.delete(
  '/:doctorId/device/:doctorDeviceId',
  auth.checkToken,
  controller.deleteDeviceFromDoctor
);

router.delete('/:doctorId/sick/:doctorSickId', auth.checkToken, controller.deleteSickFromDoctor);
router.delete(
  '/:doctorId/treatment/:doctorTreatmentId',
  auth.checkToken,
  controller.deleteTreatmentFromDoctor
);
router.delete(
  '/:doctorId/survey/:doctorSurveyId',
  auth.checkToken,
  controller.deleteSurveyFromDoctor
);

router.delete(
  '/:doctorId/treatment/:doctorTreatmentId/session/:sessionId',
  auth.checkToken,
  controller.deleteSessionFromTreatment
);

router.get(
  '/:doctorId/diseases',
  validations.getDiseases(),
  auth.checkToken,
  controller.getDiseases
);

router.put(
  '/:doctorId/survey/:doctorSurveyId',
  validations.updateDoctorSurvey(),
  auth.checkToken,
  controller.updateDoctorSurvey
);
router.put(
  '/:doctorId/treatment/:doctorTreatmentId',
  validations.updateDoctorTreatment(),
  auth.checkToken,
  controller.updateDoctorTreatment
);

router.put(
  '/:doctorId/treatment/:doctorTreatmentId/session/:sessionId',
  validations.updateSessionFromTreatment(),
  auth.checkToken,
  controller.updateSessionFromTreatment
);

router.put(
  '/:doctorId/sick/:doctorSickId',
  validations.updateSick(),
  auth.checkToken,
  controller.updateSickFromDoctor
);

router.put('/:doctorId', validations.update(), auth.checkToken, controller.update);

module.exports = router;
