const { Types } = require('mongoose');

const models = require('../../models');
const responses = require('../../utils/responses');
// const { permissionGuard } = require('../../utils/permissions');
const { paginate, addFields, makeLookup } = require('../../utils/aggregations');

module.exports = {
  /**
   * TODO Implement
   * TODO Complete docs
   * TODO Add permission
   */

  /**
   * @swagger
   *
   * /doctor:
   *   post:
   *     description: Update push notification reference
   *     tags:
   *       - Doctor
   *     security:
   *       - JWT: []
   *     produces:
   *       - application/json
   *     requestBody:
   *       required: true
   *       content:
   *         application/json:
   *           schema:
   *             type: object
   *             properties:
   *               att1:
   *                 type: string
   *                 example: old
   *               att2:
   *                 type: string
   *                 example: new
   *
   *     responses:
   *       200:
   *         description: Success
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/schemas/Response'
   *             example:
   *               message: 'SUCCESS'
   *               data: xxxxxxxxxxxxxxxxxxxxxxxx
   *
   *       400:
   *         $ref: '#/components/responses/BadRequest'
   *       401:
   *         $ref: '#/components/responses/Unauthorized'
   *       404:
   *         $ref: '#/components/responses/NotFound'
   *       Default:
   *         $ref: '#/components/responses/DefaultError'
   */
  async add(req, res, next) {
    try {
      const { session } = req;

      res.status(responses.codes.SUCCESS).json({
        message: responses.messages.SUCCESS,
        data: null,
      });
      next();
    } catch (error) {
      switch (error.message) {
        case 'SENTINEL_NOT_FOUND':
          res.status(responses.codes.BAD_REQUEST).send({ message: error.message });
          break;
        default:
      }
      next(error);
    }
  },

  /**
   * @swagger
   *
   * /doctor:
   *   post:
   *     description: Update push notification reference
   *     tags:
   *       - Doctor
   *     security:
   *       - JWT: []
   *     produces:
   *       - application/json
   *     requestBody:
   *       required: true
   *       content:
   *         application/json:
   *           schema:
   *             type: object
   *             properties:
   *               att1:
   *                 type: string
   *                 example: old
   *               att2:
   *                 type: string
   *                 example: new
   *
   *     responses:
   *       200:
   *         description: Success
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/schemas/Response'
   *             example:
   *               message: 'SUCCESS'
   *               data: xxxxxxxxxxxxxxxxxxxxxxxx
   *
   *       400:
   *         $ref: '#/components/responses/BadRequest'
   *       401:
   *         $ref: '#/components/responses/Unauthorized'
   *       404:
   *         $ref: '#/components/responses/NotFound'
   *       Default:
   *         $ref: '#/components/responses/DefaultError'
   */
  async addSick(req, res, next) {
    try {
      const { session } = req;
      const { doctorId: doctor } = req.params;
      const { shortName, email } = req.body;

      const contact = await models.Contact.findOne({ address: email }, null, { session });
      const user = await models.User.findOne({ mainEmail: contact, shortName }, null, { session });
      const sick = await models.Sick.findOne({ user }, null, { session });

      const sickDoctor = await models.SickDoctor.create([{ doctor, sick }], { session });

      res.status(responses.codes.SUCCESS).json({
        message: responses.messages.SUCCESS,
        data: sickDoctor,
      });
      next();
    } catch (error) {
      switch (error.message) {
        case 'SENTINEL_NOT_FOUND':
          res.status(responses.codes.BAD_REQUEST).send({ message: error.message });
          break;
        default:
      }
      next(error);
    }
  },

  /**
   * @swagger
   *
   * /doctor:
   *   post:
   *     description: Update push notification reference
   *     tags:
   *       - Doctor
   *     security:
   *       - JWT: []
   *     produces:
   *       - application/json
   *     requestBody:
   *       required: true
   *       content:
   *         application/json:
   *           schema:
   *             type: object
   *             properties:
   *               att1:
   *                 type: string
   *                 example: old
   *               att2:
   *                 type: string
   *                 example: new
   *
   *     responses:
   *       200:
   *         description: Success
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/schemas/Response'
   *             example:
   *               message: 'SUCCESS'
   *               data: xxxxxxxxxxxxxxxxxxxxxxxx
   *
   *       400:
   *         $ref: '#/components/responses/BadRequest'
   *       401:
   *         $ref: '#/components/responses/Unauthorized'
   *       404:
   *         $ref: '#/components/responses/NotFound'
   *       Default:
   *         $ref: '#/components/responses/DefaultError'
   */
  async addTreatment(req, res, next) {
    try {
      const { session } = req;
      const { doctorId: owner } = req.params;
      const { requesting: doctor } = req.headers;
      const { body } = req;

      const [treatment] = await models.Treatment.create([{ owner, ...body }], { session });
      const doctorTreatment = await models.DoctorTreatment.create([{ treatment, doctor }], {
        session,
      });
      const sickTreatment = await models.SickTreatment.create([{ treatment, sick: body.sick }], {
        session,
      });

      res.status(responses.codes.SUCCESS).json({
        message: responses.messages.SUCCESS,
        data: { doctorTreatment, sickTreatment },
      });
      next();
    } catch (error) {
      switch (error.message) {
        case 'SENTINEL_NOT_FOUND':
          res.status(responses.codes.BAD_REQUEST).send({ message: error.message });
          break;
        default:
      }
      next(error);
    }
  },

  /**
   * @swagger
   *
   * /doctor:
   *   post:
   *     description: Update push notification reference
   *     tags:
   *       - Doctor
   *     security:
   *       - JWT: []
   *     produces:
   *       - application/json
   *     requestBody:
   *       required: true
   *       content:
   *         application/json:
   *           schema:
   *             type: object
   *             properties:
   *               att1:
   *                 type: string
   *                 example: old
   *               att2:
   *                 type: string
   *                 example: new
   *
   *     responses:
   *       200:
   *         description: Success
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/schemas/Response'
   *             example:
   *               message: 'SUCCESS'
   *               data: xxxxxxxxxxxxxxxxxxxxxxxx
   *
   *       400:
   *         $ref: '#/components/responses/BadRequest'
   *       401:
   *         $ref: '#/components/responses/Unauthorized'
   *       404:
   *         $ref: '#/components/responses/NotFound'
   *       Default:
   *         $ref: '#/components/responses/DefaultError'
   */
  async addSessionToTreatment(req, res, next) {
    try {
      const { session } = req;
      const { doctorTreatmentId } = req.params;
      const { requesting: doctor } = req.headers;
      const { date, ...body } = req.body;

      const dateObj = new Date(date);
      const newDate = new Date(
        dateObj.getFullYear(),
        dateObj.getMonth(),
        dateObj.getDate(),
        0,
        0,
        0
      );
      const doctorTreatment = await models.DoctorTreatment.findById(doctorTreatmentId, null, {
        session,
      });

      if (!doctorTreatment) throw new Error('DOCTOR_TREATMENT_NOT_FOUND');

      const treatment = await models.Treatment.findById(doctorTreatment.treatment, null, {
        session,
      });

      let newSession = await models.Session.findOne(
        {
          doctor,
          sick: treatment.sick,
          treatment: doctorTreatment.treatment,
          date: newDate,
        },
        null,
        { session }
      );

      if (!newSession) {
        newSession = new models.Session({
          doctor,
          sick: treatment.sick,
          treatment: doctorTreatment.treatment,
          date: newDate,
          ...body,
        });
        await newSession.save({ session });
      } else {
        newSession.doctor = doctor;
        newSession.sick = treatment.sick;
        newSession.treatment = doctorTreatment.treatment;
        newSession.date = newDate;

        newSession.status = body.status;
        newSession.applicationLocal = body.applicationLocal;
        newSession.pulsesNumber = body.pulsesNumber;
        newSession.seriesTime = body.seriesTime;
        newSession.intervalTime = body.intervalTime;
        newSession.seriesNumber = body.seriesNumber;
        newSession.intensity = body.intensity;
        newSession.frequency = body.frequency;
        await newSession.save();
      }

      res.status(responses.codes.SUCCESS).json({
        message: responses.messages.SUCCESS,
        data: { newSession },
      });
      next();
    } catch (error) {
      console.log(error);
      switch (error.message) {
        case 'DOCTOR_TREATMENT_NOT_FOUND':
          res.status(responses.codes.BAD_REQUEST).send({ message: error.message });
          break;
        default:
      }
      next(error);
    }
  },

  /**
   * @swagger
   *
   * /doctor:
   *   post:
   *     description: Update push notification reference
   *     tags:
   *       - Doctor
   *     security:
   *       - JWT: []
   *     produces:
   *       - application/json
   *     requestBody:
   *       required: true
   *       content:
   *         application/json:
   *           schema:
   *             type: object
   *             properties:
   *               att1:
   *                 type: string
   *                 example: old
   *               att2:
   *                 type: string
   *                 example: new
   *
   *     responses:
   *       200:
   *         description: Success
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/schemas/Response'
   *             example:
   *               message: 'SUCCESS'
   *               data: xxxxxxxxxxxxxxxxxxxxxxxx
   *
   *       400:
   *         $ref: '#/components/responses/BadRequest'
   *       401:
   *         $ref: '#/components/responses/Unauthorized'
   *       404:
   *         $ref: '#/components/responses/NotFound'
   *       Default:
   *         $ref: '#/components/responses/DefaultError'
   */
  async addSurvey(req, res, next) {
    try {
      const { session } = req;
      const { doctorId } = req.params;
      const { questions, doctorTreatment, sickDate, doctorDate } = req.body;
      const { requesting } = req.headers;

      const doctorTreatmentFound = await models.DoctorTreatment.findById(doctorTreatment, null, {
        session,
      });

      if (!doctorTreatmentFound) throw new Error('DOCTOR_TREATMENT_NOT_FOUND');
      const treatment = await models.Treatment.findById(doctorTreatmentFound.treatment, null, {
        session,
      });
      if (!treatment) throw new Error('TREATMENT_NOT_FOUND');
      const sick = await models.Sick.findById(treatment.sick, null, { session });
      if (!sick) throw new Error('SICK_NOT_FOUND');
      const survey = await new models.Survey({ owner: requesting }).save({ session });
      const treatmentSurvey = await models.TreatmentSurvey.create(
        [{ treatment: treatment._id, survey: survey._id }],
        { session }
      );
      const doctorSurvey = await models.DoctorSurvey.create(
        [{ doctor: doctorId, survey: survey._id, date: doctorDate }],
        { session }
      );
      const sickSurvey = await models.SickSurvey.create(
        [{ sick: sick._id, survey: survey._id, date: sickDate }],
        {
          session,
        }
      );
      const surveyQuestions = await Promise.all(
        questions.map(async ({ item, index }) => {
          return models.SurveyQuestion.create([{ survey: survey._id, question: item, index }], {
            session,
          });
        })
      );
      const sickQuestions = await Promise.all(
        questions.map(async ({ item }) => {
          return models.SickQuestion.create(
            [{ sick: sick._id, survey: survey._id, question: item }],
            {
              session,
            }
          );
        })
      );

      res.status(responses.codes.SUCCESS).json({
        message: responses.messages.SUCCESS,
        data: { surveyQuestions, sickSurvey, doctorSurvey, treatmentSurvey, sickQuestions },
      });
      next();
    } catch (error) {
      switch (error.message) {
        case 'DOCTOR_TREATMENT_NOT_FOUND':
          res.status(responses.codes.BAD_REQUEST).send({ message: error.message });
          break;
        case 'TREATMENT_NOT_FOUND':
          res.status(responses.codes.BAD_REQUEST).send({ message: error.message });
          break;
        case 'SICK_NOT_FOUND':
          res.status(responses.codes.BAD_REQUEST).send({ message: error.message });
          break;
        default:
      }
      next(error);
    }
  },

  /**
   * @swagger
   *
   * /doctor:
   *   post:
   *     description: Update push notification reference
   *     tags:
   *       - Doctor
   *     security:
   *       - JWT: []
   *     produces:
   *       - application/json
   *     requestBody:
   *       required: true
   *       content:
   *         application/json:
   *           schema:
   *             type: object
   *             properties:
   *               att1:
   *                 type: string
   *                 example: old
   *               att2:
   *                 type: string
   *                 example: new
   *
   *     responses:
   *       200:
   *         description: Success
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/schemas/Response'
   *             example:
   *               message: 'SUCCESS'
   *               data: xxxxxxxxxxxxxxxxxxxxxxxx
   *
   *       400:
   *         $ref: '#/components/responses/BadRequest'
   *       401:
   *         $ref: '#/components/responses/Unauthorized'
   *       404:
   *         $ref: '#/components/responses/NotFound'
   *       Default:
   *         $ref: '#/components/responses/DefaultError'
   */
  async addDevice(req, res, next) {
    try {
      const { session } = req;
      const { doctorId: doctor } = req.params;
      const { serialNumber } = req.body;

      const deviceFound = await models.Device.findOne({ serialNumber }, null, {
        session,
      });
      const doctorFound = await models.Doctor.findById(doctor, null, { session });

      if (!deviceFound) throw new Error('DEVICE_NOT_FOUND');
      if (!doctorFound) throw new Error('DOCTOR_NOT_FOUND');

      const doctorDevice = await models.DoctorDevice.create([{ doctor, device: deviceFound._id }], {
        session,
      });

      if (!deviceFound.active && !deviceFound.owner) {
        await models.Device.findByIdAndUpdate(
          deviceFound._id,
          { $set: { owner: doctor } },
          { session }
        );
      }

      res.status(responses.codes.SUCCESS).json({
        message: responses.messages.SUCCESS,
        data: doctorDevice,
      });
      next();
    } catch (error) {
      switch (error.message) {
        case 'DEVICE_NOT_FOUND':
          res.status(responses.codes.BAD_REQUEST).send({ message: error.message });
          break;
        case 'DOCTOR_NOT_FOUND':
          res.status(responses.codes.BAD_REQUEST).send({ message: error.message });
          break;
        default:
      }
      next(error);
    }
  },

  /**
   * @swagger
   *
   * /doctor:
   *   post:
   *     description: Update push notification reference
   *     tags:
   *       - Doctor
   *     security:
   *       - JWT: []
   *     produces:
   *       - application/json
   *     requestBody:
   *       required: true
   *       content:
   *         application/json:
   *           schema:
   *             type: object
   *             properties:
   *               att1:
   *                 type: string
   *                 example: old
   *               att2:
   *                 type: string
   *                 example: new
   *
   *     responses:
   *       200:
   *         description: Success
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/schemas/Response'
   *             example:
   *               message: 'SUCCESS'
   *               data: xxxxxxxxxxxxxxxxxxxxxxxx
   *
   *       400:
   *         $ref: '#/components/responses/BadRequest'
   *       401:
   *         $ref: '#/components/responses/Unauthorized'
   *       404:
   *         $ref: '#/components/responses/NotFound'
   *       Default:
   *         $ref: '#/components/responses/DefaultError'
   */
  async addDisease(req, res, next) {
    try {
      const { session } = req;

      res.status(responses.codes.SUCCESS).json({
        message: responses.messages.SUCCESS,
        data: null,
      });
      next();
    } catch (error) {
      switch (error.message) {
        case 'SENTINEL_NOT_FOUND':
          res.status(responses.codes.BAD_REQUEST).send({ message: error.message });
          break;
        default:
      }
      next(error);
    }
  },

  /**
   * TODO Implement
   * TODO Complete docs
   * TODO Add permission
   */

  /**
   * @swagger
   *
   * /doctor:
   *   get:
   *     description: Get all Doctors
   *     tags:
   *       - Doctor
   *     security:
   *       - JWT: []
   *       - Requesting: []
   *     produces:
   *       - application/json
   *     parameters:
   *       - $ref: '#/components/parameters/Pagination'
   *       - in: query
   *         name: checked
   *
   *         schema:
   *           type: string
   *       - in: query
   *         name: type
   *
   *         schema:
   *           type: string
   *       - in: query
   *         name: user
   *
   *         schema:
   *           type: string
   *       - in: query
   *         name: createdAt
   *
   *         schema:
   *           type: string
   *       - in: query
   *         name: updatedAt
   *
   *         schema:
   *           type: string
   *
   *     responses:
   *       200:
   *         description: Success
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/schemas/Response'
   *             example:
   *               message: SUCCESS
   *               metadata:
   *                 total: 1826
   *                 page: 1
   *                 limit: 5
   *               data:
   *                 - _id: xxxxxxxxxxxxxxxxxxxxxxxx
   *                   checked: true
   *                   address: 1234567890
   *                   createdAt: 2019-02-18T22:31:48.039Z
   *                   updatedAt: 2019-02-18T22:31:48.039Z
   *                   user: "@user"
   *       400:
   *         $ref: '#/components/responses/BadRequest'
   *       401:
   *         $ref: '#/components/responses/Unauthorized'
   *       404:
   *         $ref: '#/components/responses/NotFound'
   *       Default:
   *         $ref: '#/components/responses/DefaultError'
   */
  async get(req, res, next) {
    try {
      const { session } = req;
      const { pagination } = req.query;

      const result = await paginate(models.Doctor, pagination, { session }, [
        ...makeLookup('users', 'user', '_id', 'userObj', true, true),
        ...makeLookup('peoples', 'userObj._id', 'user', 'peopleObj', true, true),
        { $unset: ['userObj.password'] },
      ]);
      res.status(responses.codes.SUCCESS).json({
        message: responses.messages.SUCCESS,
        ...result,
      });
      next();
    } catch (error) {
      switch (error.message) {
        case 'SENTINEL_NOT_FOUND':
          res.status(responses.codes.BAD_REQUEST).send({ message: error.message });
          break;
        default:
      }
      next(error);
    }
  },

  /**
   * TODO Implement
   * TODO Complete docs
   * TODO Add permission
   */

  /**
   * @swagger
   *
   * /doctor:
   *   get:
   *     description: Get all Doctors
   *     tags:
   *       - Doctor
   *     security:
   *       - JWT: []
   *       - Requesting: []
   *     produces:
   *       - application/json
   *     parameters:
   *       - $ref: '#/components/parameters/Pagination'
   *       - in: query
   *         name: checked
   *
   *         schema:
   *           type: string
   *       - in: query
   *         name: type
   *
   *         schema:
   *           type: string
   *       - in: query
   *         name: user
   *
   *         schema:
   *           type: string
   *       - in: query
   *         name: createdAt
   *
   *         schema:
   *           type: string
   *       - in: query
   *         name: updatedAt
   *
   *         schema:
   *           type: string
   *
   *     responses:
   *       200:
   *         description: Success
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/schemas/Response'
   *             example:
   *               message: SUCCESS
   *               metadata:
   *                 total: 1826
   *                 page: 1
   *                 limit: 5
   *               data:
   *                 - _id: xxxxxxxxxxxxxxxxxxxxxxxx
   *                   checked: true
   *                   address: 1234567890
   *                   createdAt: 2019-02-18T22:31:48.039Z
   *                   updatedAt: 2019-02-18T22:31:48.039Z
   *                   user: "@user"
   *       400:
   *         $ref: '#/components/responses/BadRequest'
   *       401:
   *         $ref: '#/components/responses/Unauthorized'
   *       404:
   *         $ref: '#/components/responses/NotFound'
   *       Default:
   *         $ref: '#/components/responses/DefaultError'
   */
  async getSicks(req, res, next) {
    try {
      const { session } = req;
      const { pagination } = req.query;
      const { doctorId } = req.params;

      const result = await paginate(models.SickDoctor, pagination, { session }, [
        addFields([{ prop: 'doctor', type: 'String' }]),
        { $match: { doctor: doctorId } },
        addFields([{ prop: 'doctor', type: 'ObjectId' }]),
        ...makeLookup('profiles', 'sick', '_id', 'sickObj', true, true),
        ...makeLookup('sickdiseases', 'sick', 'sick', 'sickdiseasesObj', false),
        ...makeLookup('users', 'sickObj.user', '_id', 'sickObj.userObj', true),
        ...makeLookup('peoples', 'sickObj.userObj.people', '_id', 'sickObj.peopleObj', true),
        { $unset: ['sickObj.userObj.password'] },
      ]);
      res.status(responses.codes.SUCCESS).json({
        message: responses.messages.SUCCESS,
        ...result,
      });
      next();
    } catch (error) {
      switch (error.message) {
        case 'SENTINEL_NOT_FOUND':
          res.status(responses.codes.BAD_REQUEST).send({ message: error.message });
          break;
        default:
      }
      next(error);
    }
  },

  /**
   * @swagger
   *
   * /doctor/{doctorId}/sick/{sickDoctorId}:
   *   get:
   *     description: Get an Device
   *     tags:
   *       - Device
   *     security:
   *       - JWT: []
   *       - Requesting: []
   *     produces:
   *       - application/json
   *     parameters:
   *       - in: path
   *         name: sickDoctorId
   *         required: true
   *         schema:
   *           type: string
   *
   *     responses:
   *       200:
   *         description: Success
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/schemas/Response'
   *             example:
   *               message: SUCCESS
   *               data:
   *                 _id: xxxxxxxxxxxxxxxxxxxxxxxx
   *                 att1: true
   *                 att2: text
   *
   *       400:
   *         $ref: '#/components/responses/BadRequest'
   *       401:
   *         $ref: '#/components/responses/Unauthorized'
   *       404:
   *         $ref: '#/components/responses/NotFound'
   *       Default:
   *         $ref: '#/components/responses/DefaultError'
   */
  async getSick(req, res, next) {
    try {
      const { session } = req;
      const { sickDoctorId, doctorId } = req.params;

      const [sickDoctor] = await models.SickDoctor.aggregate([
        { $match: { _id: Types.ObjectId(sickDoctorId), doctor: Types.ObjectId(doctorId) } },
        ...makeLookup('profiles', 'sick', '_id', 'sickObject', true),
        ...makeLookup('users', 'sickObject.user', '_id', 'userObj', true),
        ...makeLookup('peoples', 'userObj.people', '_id', 'peopleObj', true),
        {
          $addFields: {
            sickName: {
              $concat: ['$peopleObj.name', ' (', '$userObj.shortName', ') '],
            },
          },
        },
        { $unset: ['userObj', 'peopleObj'] },
      ]).option({ session });

      if (!sickDoctor) throw new Error('SICK_DOCTOR_NOT_FOUND');

      res.status(responses.codes.SUCCESS).json({
        message: responses.messages.SUCCESS,
        data: sickDoctor,
      });
      next();
    } catch (error) {
      switch (error.message) {
        case 'SICK_DOCTOR_NOT_FOUND':
          res.status(responses.codes.BAD_REQUEST).send({ message: error.message });
          break;
        default:
      }
      next(error);
    }
  },

  /**
   * @swagger
   *
   * /doctor/{doctorId}/treatment/{doctorTreatmentId}:
   *   get:
   *     description: Get an DoctorTreatment
   *     tags:
   *       - DoctorTreatment
   *     security:
   *       - JWT: []
   *       - Requesting: []
   *     produces:
   *       - application/json
   *     parameters:
   *       - in: path
   *         name: doctorTreatmentId
   *         required: true
   *         schema:
   *           type: string
   *
   *     responses:
   *       200:
   *         description: Success
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/schemas/Response'
   *             example:
   *               message: SUCCESS
   *               data:
   *                 _id: xxxxxxxxxxxxxxxxxxxxxxxx
   *                 att1: true
   *                 att2: text
   *
   *       400:
   *         $ref: '#/components/responses/BadRequest'
   *       401:
   *         $ref: '#/components/responses/Unauthorized'
   *       404:
   *         $ref: '#/components/responses/NotFound'
   *       Default:
   *         $ref: '#/components/responses/DefaultError'
   */
  async getTreatment(req, res, next) {
    try {
      const { session } = req;
      const { doctorTreatmentId, doctorId } = req.params;

      const [doctorTreatment] = await models.DoctorTreatment.aggregate([
        { $match: { _id: Types.ObjectId(doctorTreatmentId), doctor: Types.ObjectId(doctorId) } },
        ...makeLookup('treatments', 'treatment', '_id', 'treatmentObj', true),
        ...makeLookup('sicktreatments', 'treatment', 'treatment', 'sicktreatmentsObj', true),
        ...makeLookup('profiles', 'treatmentObj.sick', '_id', 'sickObj', true, true),
        ...makeLookup('sickdoctors', 'treatmentObj.sick', 'sick', 'doctorSickObj', true, true),
        { $match: { 'doctorSickObj.doctor': Types.ObjectId(doctorId) } },
        ...makeLookup(
          'diseases',
          'treatmentObj.disease',
          '_id',
          'treatmentObj.diseaseObj',
          true,
          true
        ),
        ...makeLookup('users', 'sickObj.user', '_id', 'sickObj.userObj', true),
        ...makeLookup('peoples', 'sickObj.userObj.people', '_id', 'sickObj.peopleObj', true),
        ...makeLookup('profiles', 'doctor', '_id', 'doctorObj', true),
        ...makeLookup('users', 'doctorObj.user', '_id', 'userObj', true),
        ...makeLookup('peoples', 'userObj.people', '_id', 'peopleObj', true),
        {
          $addFields: {
            doctorSick: '$doctorSickObj._id',
            treatmentDays: '$treatmentObj.treatmentDays',
            doctorName: {
              $concat: ['$peopleObj.name', ' (', '$userObj.shortName', ') '],
            },
            sickName: {
              $concat: ['$sickObj.peopleObj.name', ' (', '$sickObj.userObj.shortName', ') '],
            },
          },
        },
        { $unset: ['sickObj', 'doctorObj', 'peopleObj', 'userObj'] },
      ]).option({ session });

      if (!doctorTreatment) throw new Error('DOCTOR_TREATMENT_NOT_FOUND');

      res.status(responses.codes.SUCCESS).json({
        message: responses.messages.SUCCESS,
        data: doctorTreatment,
      });
      next();
    } catch (error) {
      switch (error.message) {
        case 'DOCTOR_TREATMENT_NOT_FOUND':
          res.status(responses.codes.BAD_REQUEST).send({ message: error.message });
          break;
        default:
      }
      next(error);
    }
  },

  /**
   * @swagger
   *
   * /sick/{sickId}/diseases:
   *   get:
   *     description: Get all Sicks
   *     tags:
   *       - Sick
   *     security:
   *       - JWT: []
   *       - Requesting: []
   *     produces:
   *       - application/json
   *     parameters:
   *       - $ref: '#/components/parameters/Pagination'
   *       - in: query
   *         name: checked
   *
   *         schema:
   *           type: string
   *       - in: query
   *         name: type
   *
   *         schema:
   *           type: string
   *       - in: query
   *         name: user
   *
   *         schema:
   *           type: string
   *       - in: query
   *         name: createdAt
   *
   *         schema:
   *           type: string
   *       - in: query
   *         name: updatedAt
   *
   *         schema:
   *           type: string
   *
   *     responses:
   *       200:
   *         description: Success
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/schemas/Response'
   *             example:
   *               message: SUCCESS
   *               metadata:
   *                 total: 1826
   *                 page: 1
   *                 limit: 5
   *               data:
   *                 - _id: xxxxxxxxxxxxxxxxxxxxxxxx
   *                   checked: true
   *                   address: 1234567890
   *                   createdAt: 2019-02-18T22:31:48.039Z
   *                   updatedAt: 2019-02-18T22:31:48.039Z
   *                   user: "@user"
   *       400:
   *         $ref: '#/components/responses/BadRequest'
   *       401:
   *         $ref: '#/components/responses/Unauthorized'
   *       404:
   *         $ref: '#/components/responses/NotFound'
   *       Default:
   *         $ref: '#/components/responses/DefaultError'
   */
  async getSessions(req, res, next) {
    try {
      const { session } = req;
      const { pagination } = req.query;
      const { doctorId, doctorTreatmentId } = req.params;

      const doctorTreatment = await models.DoctorTreatment.findById(doctorTreatmentId, null, {
        session,
      });

      if (!doctorTreatment) throw new Error('DOCTOR_TREATMENT_NOT_FOUND');

      const result = await paginate(models.Session, pagination, { session }, [
        { $match: { doctor: Types.ObjectId(doctorId), treatment: doctorTreatment.treatment } },
        ...makeLookup('profiles', 'doctor', '_id', 'doctorObj', true, true),
        ...makeLookup('users', 'doctorObj.user', '_id', 'doctorObj.userObj', true),
        ...makeLookup('peoples', 'doctorObj.userObj.people', '_id', 'doctorObj.peopleObj', true),
        {
          $addFields: {
            doctorName: {
              $concat: ['$doctorObj.peopleObj.name', ' (', '$doctorObj.userObj.shortName', ') '],
            },
          },
        },
        { $unset: ['doctorObj', 'peopleObj', 'userObj'] },
      ]);
      res.status(responses.codes.SUCCESS).json({
        message: responses.messages.SUCCESS,
        ...result,
      });
      next();
    } catch (error) {
      console.log(error);
      switch (error.message) {
        case 'SENTINEL_NOT_FOUND':
          res.status(responses.codes.BAD_REQUEST).send({ message: error.message });
          break;
        default:
      }
      next(error);
    }
  },

  /**
   * @swagger
   *
   * /sick/{sickId}/diseases:
   *   get:
   *     description: Get all Sicks
   *     tags:
   *       - Sick
   *     security:
   *       - JWT: []
   *       - Requesting: []
   *     produces:
   *       - application/json
   *     parameters:
   *       - $ref: '#/components/parameters/Pagination'
   *       - in: query
   *         name: checked
   *
   *         schema:
   *           type: string
   *       - in: query
   *         name: type
   *
   *         schema:
   *           type: string
   *       - in: query
   *         name: user
   *
   *         schema:
   *           type: string
   *       - in: query
   *         name: createdAt
   *
   *         schema:
   *           type: string
   *       - in: query
   *         name: updatedAt
   *
   *         schema:
   *           type: string
   *
   *     responses:
   *       200:
   *         description: Success
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/schemas/Response'
   *             example:
   *               message: SUCCESS
   *               metadata:
   *                 total: 1826
   *                 page: 1
   *                 limit: 5
   *               data:
   *                 - _id: xxxxxxxxxxxxxxxxxxxxxxxx
   *                   checked: true
   *                   address: 1234567890
   *                   createdAt: 2019-02-18T22:31:48.039Z
   *                   updatedAt: 2019-02-18T22:31:48.039Z
   *                   user: "@user"
   *       400:
   *         $ref: '#/components/responses/BadRequest'
   *       401:
   *         $ref: '#/components/responses/Unauthorized'
   *       404:
   *         $ref: '#/components/responses/NotFound'
   *       Default:
   *         $ref: '#/components/responses/DefaultError'
   */
  async getSession(req, res, next) {
    try {
      const { session } = req;
      const { doctorId, doctorTreatmentId, sessionId } = req.params;

      const doctorTreatment = await models.DoctorTreatment.findById(doctorTreatmentId, null, {
        session,
      });
      const [sessionFound] = await models.Session.aggregate([
        {
          $match: {
            _id: Types.ObjectId(sessionId),
            doctor: Types.ObjectId(doctorId),
            treatment: doctorTreatment.treatment,
          },
        },
        ...makeLookup('profiles', 'doctor', '_id', 'doctorObj', true, true),
        ...makeLookup('users', 'doctorObj.user', '_id', 'doctorObj.userObj', true),
        ...makeLookup('peoples', 'doctorObj.userObj.people', '_id', 'doctorObj.peopleObj', true),
        {
          $addFields: {
            doctorName: {
              $concat: ['$doctorObj.peopleObj.name', ' (', '$doctorObj.userObj.shortName', ') '],
            },
          },
        },
        { $unset: ['doctorObj', 'peopleObj', 'userObj'] },
      ]).option({ session });

      if (!sessionFound) throw new Error('SESSION_NOT_FOUND');

      res.status(responses.codes.SUCCESS).json({
        message: responses.messages.SUCCESS,
        data: sessionFound,
      });
      next();
    } catch (error) {
      switch (error.message) {
        case 'SESSION_NOT_FOUND':
          res.status(responses.codes.BAD_REQUEST).send({ message: error.message });
          break;
        default:
      }
      next(error);
    }
  },

  /**
   * @swagger
   *
   * /doctor/{doctorId}/survey/{doctorSurveyId}:
   *   get:
   *     description: Get an Survey
   *     tags:
   *       - Survey
   *     security:
   *       - JWT: []
   *       - Requesting: []
   *     produces:
   *       - application/json
   *     parameters:
   *       - in: path
   *         name: doctorSurveyId
   *         required: true
   *         schema:
   *           type: string
   *
   *     responses:
   *       200:
   *         description: Success
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/schemas/Response'
   *             example:
   *               message: SUCCESS
   *               data:
   *                 _id: xxxxxxxxxxxxxxxxxxxxxxxx
   *                 att1: true
   *                 att2: text
   *
   *       400:
   *         $ref: '#/components/responses/BadRequest'
   *       401:
   *         $ref: '#/components/responses/Unauthorized'
   *       404:
   *         $ref: '#/components/responses/NotFound'
   *       Default:
   *         $ref: '#/components/responses/DefaultError'
   */
  async getSurvey(req, res, next) {
    try {
      const { session } = req;
      const { doctorSurveyId, doctorId } = req.params;

      const [doctorSurvey] = await models.DoctorSurvey.aggregate([
        { $match: { _id: Types.ObjectId(doctorSurveyId), doctor: Types.ObjectId(doctorId) } },
        ...makeLookup('sicksurveys', 'survey', 'survey', 'sicksurveyObj', true, true),
        ...makeLookup('profiles', 'sicksurveyObj.sick', '_id', 'sickObj', true, true),
        ...makeLookup('users', 'sickObj.user', '_id', 'sickObj.userObj', true),
        ...makeLookup('peoples', 'sickObj.userObj.people', '_id', 'sickObj.peopleObj', true),
        ...makeLookup('profiles', 'doctor', '_id', 'doctorObj', true),
        ...makeLookup('users', 'doctorObj.user', '_id', 'userObj', true),
        ...makeLookup('peoples', 'userObj.people', '_id', 'peopleObj', true),
        ...makeLookup('surveyquestions', 'survey', 'survey', 'surveyquestionsObj', false),
        {
          $addFields: {
            surveyQuestionSize: { $size: '$surveyquestionsObj' },
            surveyOwnerName: {
              $concat: ['$peopleObj.name', ' (', '$userObj.shortName', ') '],
            },
            sickName: {
              $concat: ['$sickObj.peopleObj.name', ' (', '$sickObj.userObj.shortName', ') '],
            },
          },
        },
        { $unset: ['sickObj', 'doctorObj', 'peopleObj', 'userObj', 'surveyquestionsObj'] },
      ]).option({ session });

      if (!doctorSurvey) throw new Error('DOCTOR_SURVEY_NOT_FOUND');

      res.status(responses.codes.SUCCESS).json({
        message: responses.messages.SUCCESS,
        data: doctorSurvey,
      });
      next();
    } catch (error) {
      switch (error.message) {
        case 'DOCTOR_SURVEY_NOT_FOUND':
          res.status(responses.codes.BAD_REQUEST).send({ message: error.message });
          break;
        default:
      }
      next(error);
    }
  },

  /**
   * @swagger
   *
   * /sick/{sickId}/survey/{sickSurveyId}/questions:
   *   get:
   *     description: Get an Survey
   *     tags:
   *       - Survey
   *     security:
   *       - JWT: []
   *       - Requesting: []
   *     produces:
   *       - application/json
   *     parameters:
   *       - in: path
   *         name: sickSurveyId
   *         required: true
   *         schema:
   *           type: string
   *
   *     responses:
   *       200:
   *         description: Success
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/schemas/Response'
   *             example:
   *               message: SUCCESS
   *               data:
   *                 _id: xxxxxxxxxxxxxxxxxxxxxxxx
   *                 att1: true
   *                 att2: text
   *
   *       400:
   *         $ref: '#/components/responses/BadRequest'
   *       401:
   *         $ref: '#/components/responses/Unauthorized'
   *       404:
   *         $ref: '#/components/responses/NotFound'
   *       Default:
   *         $ref: '#/components/responses/DefaultError'
   */
  async getDoctorSurveyQuestions(req, res, next) {
    try {
      const { session } = req;
      const { doctorSurveyId, doctorId } = req.params;

      const doctorSurveyFound = await models.DoctorSurvey.findById(doctorSurveyId, null, {
        session,
      });
      const sickSurvey = await models.SickSurvey.findOne(
        { survey: doctorSurveyFound.survey },
        null,
        {
          session,
        }
      );

      const doctorSurveyQuestions = await models.Question.aggregate([
        ...makeLookup('sickquestions', '_id', 'question', 'sickquestionsObj', true),
        ...makeLookup('surveyquestions', '_id', 'question', 'surveyquestionsObj', true),
        {
          $match: {
            'surveyquestionsObj.survey': doctorSurveyFound.survey,
            'sickquestionsObj.sick': sickSurvey.sick,
            'sickquestionsObj.survey': doctorSurveyFound.survey,
          },
        },
      ]).option({ session });

      if (!doctorSurveyQuestions) throw new Error('SICK_SURVEYQUESTIONS_NOT_FOUND');

      res.status(responses.codes.SUCCESS).json({
        message: responses.messages.SUCCESS,
        data: doctorSurveyQuestions,
      });
      next();
    } catch (error) {
      switch (error.message) {
        case 'SICK_SURVEY_QUESTIONS_NOT_FOUND':
          res.status(responses.codes.BAD_REQUEST).send({ message: error.message });
          break;
        default:
      }
      next(error);
    }
  },

  /**
   * TODO Implement
   * TODO Complete docs
   * TODO Add permission
   */

  /**
   * @swagger
   *
   * /doctor:
   *   get:
   *     description: Get all Doctors
   *     tags:
   *       - Doctor
   *     security:
   *       - JWT: []
   *       - Requesting: []
   *     produces:
   *       - application/json
   *     parameters:
   *       - $ref: '#/components/parameters/Pagination'
   *       - in: query
   *         name: checked
   *
   *         schema:
   *           type: string
   *       - in: query
   *         name: type
   *
   *         schema:
   *           type: string
   *       - in: query
   *         name: user
   *
   *         schema:
   *           type: string
   *       - in: query
   *         name: createdAt
   *
   *         schema:
   *           type: string
   *       - in: query
   *         name: updatedAt
   *
   *         schema:
   *           type: string
   *
   *     responses:
   *       200:
   *         description: Success
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/schemas/Response'
   *             example:
   *               message: SUCCESS
   *               metadata:
   *                 total: 1826
   *                 page: 1
   *                 limit: 5
   *               data:
   *                 - _id: xxxxxxxxxxxxxxxxxxxxxxxx
   *                   checked: true
   *                   address: 1234567890
   *                   createdAt: 2019-02-18T22:31:48.039Z
   *                   updatedAt: 2019-02-18T22:31:48.039Z
   *                   user: "@user"
   *       400:
   *         $ref: '#/components/responses/BadRequest'
   *       401:
   *         $ref: '#/components/responses/Unauthorized'
   *       404:
   *         $ref: '#/components/responses/NotFound'
   *       Default:
   *         $ref: '#/components/responses/DefaultError'
   */
  async getTreatments(req, res, next) {
    try {
      const { session } = req;
      const { pagination } = req.query;
      const { doctorId } = req.params;

      const result = await paginate(models.DoctorTreatment, pagination, { session }, [
        addFields([{ prop: 'doctor', type: 'String' }]),
        { $match: { doctor: doctorId } },
        addFields([{ prop: 'doctor', type: 'ObjectId' }]),
        ...makeLookup('treatments', 'treatment', '_id', 'treatmentObj', true),
        ...makeLookup('sicktreatments', 'treatment', 'treatment', 'sicktreatmentsObj', true),
        ...makeLookup('profiles', 'treatmentObj.sick', '_id', 'sickObj', true, true),
        ...makeLookup('users', 'sickObj.user', '_id', 'sickObj.userObj', true),
        ...makeLookup('peoples', 'sickObj.userObj.people', '_id', 'sickObj.peopleObj', true),
        ...makeLookup('profiles', 'doctor', '_id', 'doctorObj', true),
        ...makeLookup('users', 'doctorObj.user', '_id', 'userObj', true),
        ...makeLookup('peoples', 'userObj.people', '_id', 'peopleObj', true),
        {
          $addFields: {
            doctorName: {
              $concat: ['$peopleObj.name', ' (', '$userObj.shortName', ') '],
            },
            sickName: {
              $concat: ['$sickObj.peopleObj.name', ' (', '$sickObj.userObj.shortName', ') '],
            },
          },
        },
        { $unset: ['sickObj', 'doctorObj', 'userObj', 'peopleObj'] },
      ]);
      res.status(responses.codes.SUCCESS).json({
        message: responses.messages.SUCCESS,
        ...result,
      });
      next();
    } catch (error) {
      switch (error.message) {
        case 'SENTINEL_NOT_FOUND':
          res.status(responses.codes.BAD_REQUEST).send({ message: error.message });
          break;
        default:
      }
      next(error);
    }
  },

  /**
   * TODO Implement
   * TODO Complete docs
   * TODO Add permission
   */

  /**
   * @swagger
   *
   * /doctor:
   *   get:
   *     description: Get all Doctors
   *     tags:
   *       - Doctor
   *     security:
   *       - JWT: []
   *       - Requesting: []
   *     produces:
   *       - application/json
   *     parameters:
   *       - $ref: '#/components/parameters/Pagination'
   *       - in: query
   *         name: checked
   *
   *         schema:
   *           type: string
   *       - in: query
   *         name: type
   *
   *         schema:
   *           type: string
   *       - in: query
   *         name: user
   *
   *         schema:
   *           type: string
   *       - in: query
   *         name: createdAt
   *
   *         schema:
   *           type: string
   *       - in: query
   *         name: updatedAt
   *
   *         schema:
   *           type: string
   *
   *     responses:
   *       200:
   *         description: Success
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/schemas/Response'
   *             example:
   *               message: SUCCESS
   *               metadata:
   *                 total: 1826
   *                 page: 1
   *                 limit: 5
   *               data:
   *                 - _id: xxxxxxxxxxxxxxxxxxxxxxxx
   *                   checked: true
   *                   address: 1234567890
   *                   createdAt: 2019-02-18T22:31:48.039Z
   *                   updatedAt: 2019-02-18T22:31:48.039Z
   *                   user: "@user"
   *       400:
   *         $ref: '#/components/responses/BadRequest'
   *       401:
   *         $ref: '#/components/responses/Unauthorized'
   *       404:
   *         $ref: '#/components/responses/NotFound'
   *       Default:
   *         $ref: '#/components/responses/DefaultError'
   */
  async getSurveys(req, res, next) {
    try {
      const { session } = req;
      const { doctorId } = req.params;
      const { pagination, doctorTreatment } = req.query;

      const doctorTreatmentFound = await models.DoctorTreatment.findById(doctorTreatment, null, {
        session,
      });
      const treatment = await models.Treatment.findById(doctorTreatmentFound.treatment, null, {
        session,
      });
      const result = await paginate(models.DoctorSurvey, pagination, { session }, [
        ...makeLookup('treatmentsurveys', 'survey', 'survey', 'treatmentsurveyObj', true, true),
        addFields([{ prop: 'doctor', type: 'String' }]),
        {
          $match: {
            doctor: doctorId,
            'treatmentsurveyObj.treatment': treatment._id,
          },
        },
        addFields([{ prop: 'doctor', type: 'ObjectId' }]),
        ...makeLookup('sicksurveys', 'survey', 'survey', 'sicksurveyObj', true, true),
        ...makeLookup('profiles', 'sicksurveyObj.sick', '_id', 'sickObj', true, true),
        ...makeLookup('users', 'sickObj.user', '_id', 'sickObj.userObj', true),
        ...makeLookup('peoples', 'sickObj.userObj.people', '_id', 'sickObj.peopleObj', true),
        ...makeLookup('surveys', 'survey', '_id', 'surveyObj', true, true),
        ...makeLookup('profiles', 'surveyObj.owner', '_id', 'ownerObj', true),
        ...makeLookup('users', 'ownerObj.user', '_id', 'userObj', true),
        ...makeLookup('peoples', 'userObj.people', '_id', 'peopleObj', true),
        ...makeLookup('surveyquestions', 'survey', 'survey', 'surveyquestionsObj', false),
        {
          $addFields: {
            surveyQuestionSize: { $size: '$surveyquestionsObj' },
            surveyOwnerName: {
              $concat: ['$peopleObj.name', ' (', '$userObj.shortName', ') '],
            },
            sickName: {
              $concat: ['$sickObj.peopleObj.name', ' (', '$sickObj.userObj.shortName', ') '],
            },
          },
        },
        {
          $unset: ['sickObj', 'userObj', 'peopleObj', 'ownerObj', 'surveyquestionsObj'],
        },
        { $sort: { 'sicksurveyObj.date': -1, 'sicksurveyObj.updatedAt': -1 } },
      ]);
      res.status(responses.codes.SUCCESS).json({
        message: responses.messages.SUCCESS,
        ...result,
      });
      next();
    } catch (error) {
      switch (error.message) {
        case 'SENTINEL_NOT_FOUND':
          res.status(responses.codes.BAD_REQUEST).send({ message: error.message });
          break;
        default:
      }
      next(error);
    }
  },

  /**
   * TODO Implement
   * TODO Complete docs
   * TODO Add permission
   */

  /**
   * @swagger
   *
   * /doctor/{doctorId}/devices:
   *   get:
   *     description: Get all Doctors
   *     tags:
   *       - Doctor
   *     security:
   *       - JWT: []
   *       - Requesting: []
   *     produces:
   *       - application/json
   *     parameters:
   *       - $ref: '#/components/parameters/Pagination'
   *       - in: query
   *         name: checked
   *
   *         schema:
   *           type: string
   *       - in: query
   *         name: type
   *
   *         schema:
   *           type: string
   *       - in: query
   *         name: user
   *
   *         schema:
   *           type: string
   *       - in: query
   *         name: createdAt
   *
   *         schema:
   *           type: string
   *       - in: query
   *         name: updatedAt
   *
   *         schema:
   *           type: string
   *
   *     responses:
   *       200:
   *         description: Success
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/schemas/Response'
   *             example:
   *               message: SUCCESS
   *               metadata:
   *                 total: 1826
   *                 page: 1
   *                 limit: 5
   *               data:
   *                 - _id: xxxxxxxxxxxxxxxxxxxxxxxx
   *                   checked: true
   *                   address: 1234567890
   *                   createdAt: 2019-02-18T22:31:48.039Z
   *                   updatedAt: 2019-02-18T22:31:48.039Z
   *                   user: "@user"
   *       400:
   *         $ref: '#/components/responses/BadRequest'
   *       401:
   *         $ref: '#/components/responses/Unauthorized'
   *       404:
   *         $ref: '#/components/responses/NotFound'
   *       Default:
   *         $ref: '#/components/responses/DefaultError'
   */
  async getDevices(req, res, next) {
    try {
      const { session } = req;
      const { doctorId } = req.params;
      const { pagination } = req.query;

      const result = await paginate(models.DoctorDevice, pagination, { session }, [
        addFields([{ prop: 'doctor', type: 'String' }]),
        { $match: { doctor: doctorId } },
        addFields([{ prop: 'doctor', type: 'ObjectId' }]),
        ...makeLookup('devices', 'device', '_id', 'deviceObj', true, true),
        ...makeLookup('profiles', 'deviceObj.owner', '_id', 'ownerObj', true),
        ...makeLookup('users', 'ownerObj.user', '_id', 'userObj', true),
        ...makeLookup('peoples', 'userObj.people', '_id', 'peopleObj', true),
        {
          $addFields: {
            'deviceObj.ownerName': {
              $concat: ['$peopleObj.name', ' (', '$userObj.shortName', ') '],
            },
          },
        },
        { $unset: ['userObj', 'peopleObj', 'ownerObj'] },
      ]);
      res.status(responses.codes.SUCCESS).json({
        message: responses.messages.SUCCESS,
        ...result,
      });
      next();
    } catch (error) {
      switch (error.message) {
        case 'SENTINEL_NOT_FOUND':
          res.status(responses.codes.BAD_REQUEST).send({ message: error.message });
          break;
        default:
      }
      next(error);
    }
  },

  /**
   * @swagger
   *
   * /doctor/{doctorId}/device/{doctorDeviceId}:
   *   get:
   *     description: Get an Device
   *     tags:
   *       - Device
   *     security:
   *       - JWT: []
   *       - Requesting: []
   *     produces:
   *       - application/json
   *     parameters:
   *       - in: path
   *         name: doctorDeviceId
   *         required: true
   *         schema:
   *           type: string
   *
   *     responses:
   *       200:
   *         description: Success
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/schemas/Response'
   *             example:
   *               message: SUCCESS
   *               data:
   *                 _id: xxxxxxxxxxxxxxxxxxxxxxxx
   *                 att1: true
   *                 att2: text
   *
   *       400:
   *         $ref: '#/components/responses/BadRequest'
   *       401:
   *         $ref: '#/components/responses/Unauthorized'
   *       404:
   *         $ref: '#/components/responses/NotFound'
   *       Default:
   *         $ref: '#/components/responses/DefaultError'
   */
  async getDevice(req, res, next) {
    try {
      const { session } = req;
      const { doctorDeviceId } = req.params;

      const [device] = await models.DoctorDevice.aggregate([
        { $match: { _id: Types.ObjectId(doctorDeviceId) } },
        ...makeLookup('devices', 'device', '_id', 'device', true),
        ...makeLookup('profiles', 'device.owner', '_id', 'ownerObj', true),
        ...makeLookup('users', 'ownerObj.user', '_id', 'userObj', true),
        ...makeLookup('peoples', 'userObj.people', '_id', 'peopleObj', true),
        {
          $addFields: {
            ownerName: {
              $concat: ['$peopleObj.name', ' (', '$userObj.shortName', ') '],
            },
          },
        },
        { $unset: ['userObj', 'peopleObj', 'ownerObj'] },
      ]).option({ session });

      if (!device) throw new Error('DEVICE_NOT_FOUND');

      let deviceOwner = {};
      if (device.owner) {
        deviceOwner = await models.DoctorDevice.findOne(
          { doctor: device.owner, device: device._id },
          null,
          {
            session,
          }
        );
        if (deviceOwner) {
          device.ownerStatus = deviceOwner.status;
        }
      }

      res.status(responses.codes.SUCCESS).json({
        message: responses.messages.SUCCESS,
        data: device,
      });
      next();
    } catch (error) {
      switch (error.message) {
        case 'DEVICE_NOT_FOUND':
          res.status(responses.codes.BAD_REQUEST).send({ message: error.message });
          break;
        default:
      }
      next(error);
    }
  },

  /**
   * @swagger
   *
   * /doctor/{doctorId}/device/{doctorDeviceId}:
   *   delete:
   *     description: Delete a Device
   *     tags:
   *       - Device
   *     security:
   *       - JWT: []
   *       - Requesting: []
   *     produces:
   *       - application/json
   *     parameters:
   *       - in: path
   *         name: doctorDeviceId
   *         required: true
   *         schema:
   *           type: string
   *
   *     responses:
   *       200:
   *         $ref: '#/components/responses/Success'
   *
   *       400:
   *         $ref: '#/components/responses/BadRequest'
   *       401:
   *         $ref: '#/components/responses/Unauthorized'
   *       404:
   *         $ref: '#/components/responses/NotFound'
   *       Default:
   *         $ref: '#/components/responses/DefaultError'
   */
  async deleteDeviceFromDoctor(req, res, next) {
    try {
      const { session } = req;
      const { doctorDeviceId: _id, doctorId: doctor } = req.params;
      const doctorDevice = await models.DoctorDevice.findOne(
        {
          _id,
          doctor,
        },
        null,
        { session }
      );

      if (!doctorDevice) throw new Error('DOCTOR_DEVICE_NOT_FOUND');

      if (doctorDevice.status === 'accepted') throw new Error('DOCTOR_DEVICE_ACCEPTED');

      await models.DoctorDevice.findByIdAndDelete(_id, { session });

      const device = await models.Device.findById(doctorDevice.device, null, { session });
      if (device.owner && device.owner.toString() === doctor.toString()) {
        await models.Device.findByIdAndUpdate(
          device,
          { $unset: { owner: 1 }, $set: { active: false } },
          { session }
        );
        await models.DoctorDevice.deleteMany({ device }).session(session);
      }

      res.status(responses.codes.SUCCESS).json({
        message: responses.messages.SUCCESS,
        data: doctorDevice,
      });
      next();
    } catch (error) {
      switch (error.message) {
        case 'DOCTOR_DEVICE_NOT_FOUND':
          res.status(responses.codes.BAD_REQUEST).send({ message: error.message });
          break;
        case 'DOCTOR_DEVICE_ACCEPTED':
          res.status(responses.codes.BAD_REQUEST).send({ message: error.message });
          break;
        default:
      }
      next(error);
    }
  },

  /**
   * @swagger
   *
   * /doctor/{doctorId}/sick/{doctorSickId}:
   *   delete:
   *     description: Delete a Device
   *     tags:
   *       - Device
   *     security:
   *       - JWT: []
   *       - Requesting: []
   *     produces:
   *       - application/json
   *     parameters:
   *       - in: path
   *         name: doctorSickId
   *         required: true
   *         schema:
   *           type: string
   *
   *     responses:
   *       200:
   *         $ref: '#/components/responses/Success'
   *
   *       400:
   *         $ref: '#/components/responses/BadRequest'
   *       401:
   *         $ref: '#/components/responses/Unauthorized'
   *       404:
   *         $ref: '#/components/responses/NotFound'
   *       Default:
   *         $ref: '#/components/responses/DefaultError'
   */
  async deleteSickFromDoctor(req, res, next) {
    try {
      const { session } = req;
      const { doctorSickId: _id, doctorId: doctor } = req.params;
      const doctorSick = await models.SickDoctor.findOne(
        {
          _id,
          doctor,
        },
        null,
        { session }
      );

      if (!doctorSick) throw new Error('DOCTOR_SICK_NOT_FOUND');

      if (doctorSick.status === 'accepted') throw new Error('DOCTOR_SICK_ACCEPTED');

      await models.SickDoctor.findByIdAndDelete(_id, { session });

      res.status(responses.codes.SUCCESS).json({
        message: responses.messages.SUCCESS,
        data: doctorSick,
      });
      next();
    } catch (error) {
      switch (error.message) {
        case 'DOCTOR_SICK_NOT_FOUND':
          res.status(responses.codes.BAD_REQUEST).send({ message: error.message });
          break;
        case 'DOCTOR_SICK_ACCEPTED':
          res.status(responses.codes.BAD_REQUEST).send({ message: error.message });
          break;
        default:
      }
      next(error);
    }
  },

  /**
   * @swagger
   *
   * /doctor/{doctorId}/treatment/{doctorTreatmentId}:
   *   delete:
   *     description: Delete a DoctorTreatment
   *     tags:
   *       - DoctorTreatment
   *     security:
   *       - JWT: []
   *       - Requesting: []
   *     produces:
   *       - application/json
   *     parameters:
   *       - in: path
   *         name: doctorTreatmentId
   *         required: true
   *         schema:
   *           type: string
   *
   *     responses:
   *       200:
   *         $ref: '#/components/responses/Success'
   *
   *       400:
   *         $ref: '#/components/responses/BadRequest'
   *       401:
   *         $ref: '#/components/responses/Unauthorized'
   *       404:
   *         $ref: '#/components/responses/NotFound'
   *       Default:
   *         $ref: '#/components/responses/DefaultError'
   */
  async deleteTreatmentFromDoctor(req, res, next) {
    try {
      const { session } = req;
      const { doctorTreatmentId: _id, doctorId: doctor } = req.params;

      const doctorTreatment = await models.DoctorTreatment.findOneAndDelete({
        _id,
        doctor,
      }).session(session);

      if (!doctorTreatment) throw new Error('DOCTOR_TREATMENT_NOT_FOUND');

      await models.SickTreatment.findOneAndDelete({ treatment: doctorTreatment.treatment }).session(
        session
      );

      const treatment = await models.Treatment.findByIdAndDelete(doctorTreatment.treatment, {
        session,
      });

      const treatmentSurveys = await models.TreatmentSurvey.find(
        { treatment: treatment._id },
        null,
        { session }
      );

      await Promise.all(
        treatmentSurveys.map(async ({ survey }) => {
          return models.SurveyQuestion.deleteMany({ survey }).session(session);
        })
      );

      await Promise.all(
        treatmentSurveys.map(async ({ survey }) => {
          return models.SickQuestion.deleteMany({ survey, sick: treatment.sick }).session(session);
        })
      );
      await Promise.all(
        treatmentSurveys.map(async ({ survey }) => {
          return models.SickSurvey.deleteMany({ survey, sick: treatment.sick }).session(session);
        })
      );
      await Promise.all(
        treatmentSurveys.map(async ({ survey }) => {
          return models.DoctorSurvey.deleteMany({ survey, doctor }).session(session);
        })
      );

      await Promise.all(
        treatmentSurveys.map(async ({ survey }) => {
          return models.Survey.findByIdAndDelete(survey, { session });
        })
      );

      await models.TreatmentSurvey.deleteMany({ treatment: treatment._id }).session(session);

      res.status(responses.codes.SUCCESS).json({
        message: responses.messages.SUCCESS,
        data: doctorTreatment,
      });
      next();
    } catch (error) {
      console.log(error);
      switch (error.message) {
        case 'DOCTOR_TREATMENT_NOT_FOUND':
          res.status(responses.codes.BAD_REQUEST).send({ message: error.message });
          break;

        default:
      }
      next(error);
    }
  },

  /**
   * @swagger
   *
   * /doctor/{doctorId}/survey/{doctorSurveyId}:
   *   delete:
   *     description: Delete a Survey
   *     tags:
   *       - Survey
   *     security:
   *       - JWT: []
   *       - Requesting: []
   *     produces:
   *       - application/json
   *     parameters:
   *       - in: path
   *         name: doctorSurveyId
   *         required: true
   *         schema:
   *           type: string
   *
   *     responses:
   *       200:
   *         $ref: '#/components/responses/Success'
   *
   *       400:
   *         $ref: '#/components/responses/BadRequest'
   *       401:
   *         $ref: '#/components/responses/Unauthorized'
   *       404:
   *         $ref: '#/components/responses/NotFound'
   *       Default:
   *         $ref: '#/components/responses/DefaultError'
   */
  async deleteSurveyFromDoctor(req, res, next) {
    try {
      const { session } = req;
      const { doctorSurveyId: _id, doctorId: doctor } = req.params;
      const { requesting } = req.headers;

      const doctorSurvey = await models.DoctorSurvey.findOne(
        {
          _id,
          doctor,
        },
        null,
        { session }
      );

      if (!doctorSurvey) throw new Error('DOCTOR_SURVEY_NOT_FOUND');

      await models.Survey.deleteOne({ _id: doctorSurvey.survey, owner: requesting }, { session });
      await models.DoctorSurvey.findByIdAndDelete(_id, { session });
      const treatmentSurvey = await models.TreatmentSurvey.findOneAndRemove(
        {
          survey: doctorSurvey.survey,
        },
        { session }
      );
      const treatment = await models.Treatment.findById(treatmentSurvey.treatment, null, {
        session,
      });
      await models.SurveyQuestion.deleteMany({ survey: doctorSurvey.survey }).session(session);
      await models.SickQuestion.deleteMany({
        sick: treatment.sick,
        survey: doctorSurvey.survey,
      }).session(session);
      await models.SickSurvey.deleteMany({
        sick: treatment.sick,
        survey: doctorSurvey.survey,
      }).session(session);

      res.status(responses.codes.SUCCESS).json({
        message: responses.messages.SUCCESS,
        data: doctorSurvey,
      });
      next();
    } catch (error) {
      switch (error.message) {
        case 'DOCTOR_SURVEY_NOT_FOUND':
          res.status(responses.codes.BAD_REQUEST).send({ message: error.message });
          break;

        default:
      }
      next(error);
    }
  },

  /**
   * TODO Implement
   * TODO Complete docs
   * TODO Add permission
   */

  /**
   * @swagger
   *
   * /doctor/{userId}/diseases:
   *   get:
   *     description: Get all Doctors
   *     tags:
   *       - Doctor
   *     security:
   *       - JWT: []
   *       - Requesting: []
   *     produces:
   *       - application/json
   *     parameters:
   *       - $ref: '#/components/parameters/Pagination'
   *       - in: query
   *         name: checked
   *
   *         schema:
   *           type: string
   *       - in: query
   *         name: type
   *
   *         schema:
   *           type: string
   *       - in: query
   *         name: user
   *
   *         schema:
   *           type: string
   *       - in: query
   *         name: createdAt
   *
   *         schema:
   *           type: string
   *       - in: query
   *         name: updatedAt
   *
   *         schema:
   *           type: string
   *
   *     responses:
   *       200:
   *         description: Success
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/schemas/Response'
   *             example:
   *               message: SUCCESS
   *               metadata:
   *                 total: 1826
   *                 page: 1
   *                 limit: 5
   *               data:
   *                 - _id: xxxxxxxxxxxxxxxxxxxxxxxx
   *                   checked: true
   *                   address: 1234567890
   *                   createdAt: 2019-02-18T22:31:48.039Z
   *                   updatedAt: 2019-02-18T22:31:48.039Z
   *                   user: "@user"
   *       400:
   *         $ref: '#/components/responses/BadRequest'
   *       401:
   *         $ref: '#/components/responses/Unauthorized'
   *       404:
   *         $ref: '#/components/responses/NotFound'
   *       Default:
   *         $ref: '#/components/responses/DefaultError'
   */
  async getDiseases(req, res, next) {
    try {
      const { session } = req;
      const { pagination } = req.query;
      const { doctorId } = req.query;

      const result = await paginate(models.DoctorDisease, pagination, { session }, [
        addFields([{ prop: 'doctor', type: 'String' }]),
        { $match: { doctor: doctorId } },
        addFields([{ prop: 'doctor', type: 'ObjectId' }]),
        ...makeLookup('diseases', 'disease', '_id', 'diseasesObj', true, true),
      ]);
      res.status(responses.codes.SUCCESS).json({
        message: responses.messages.SUCCESS,
        ...result,
      });
      next();
    } catch (error) {
      switch (error.message) {
        case 'SENTINEL_NOT_FOUND':
          res.status(responses.codes.BAD_REQUEST).send({ message: error.message });
          break;
        default:
      }
      next(error);
    }
  },

  /**
   * TODO Implement
   * TODO Complete docs
   * TODO Add permission
   */

  /**
   * @swagger
   *
   * /doctor/{doctorId}:
   *   get:
   *     description: Get an Doctor
   *     tags:
   *       - Doctor
   *     security:
   *       - JWT: []
   *       - Requesting: []
   *     produces:
   *       - application/json
   *     parameters:
   *       - in: path
   *         name: doctorId
   *         required: true
   *         schema:
   *           type: string
   *
   *     responses:
   *       200:
   *         description: Success
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/schemas/Response'
   *             example:
   *               message: SUCCESS
   *               data:
   *                 _id: xxxxxxxxxxxxxxxxxxxxxxxx
   *                 att1: true
   *                 att2: text
   *
   *       400:
   *         $ref: '#/components/responses/BadRequest'
   *       401:
   *         $ref: '#/components/responses/Unauthorized'
   *       404:
   *         $ref: '#/components/responses/NotFound'
   *       Default:
   *         $ref: '#/components/responses/DefaultError'
   */
  async getById(req, res, next) {
    try {
      const { session } = req;
      const { doctorId } = req.params;

      const [doctor] = await models.Doctor.aggregate([
        {
          $match: { _id: Types.ObjectId(doctorId) },
        },
        ...makeLookup('users', 'user', '_id', 'userObj', true, true),
        ...makeLookup('peoples', 'userObj._id', 'user', 'peopleObj', true, true),
        { $unset: ['userObj.password'] },
      ]).option(session);
      res.status(responses.codes.SUCCESS).json({
        message: responses.messages.SUCCESS,
        data: doctor,
      });
      next();
    } catch (error) {
      switch (error.message) {
        case 'SENTINEL_NOT_FOUND':
          res.status(responses.codes.BAD_REQUEST).send({ message: error.message });
          break;
        default:
      }
      next(error);
    }
  },
  /**
   * TODO Implement
   * TODO Complete docs
   * TODO Add permission
   */

  /**
   * @swagger
   *
   * /doctor/{doctorId}:
   *   delete:
   *     description: Delete a Doctor
   *     tags:
   *       - Doctor
   *     security:
   *       - JWT: []
   *       - Requesting: []
   *     produces:
   *       - application/json
   *     parameters:
   *       - in: path
   *         name: doctorId
   *         required: true
   *         schema:
   *           type: string
   *
   *     responses:
   *       200:
   *         $ref: '#/components/responses/Success'
   *
   *       400:
   *         $ref: '#/components/responses/BadRequest'
   *       401:
   *         $ref: '#/components/responses/Unauthorized'
   *       404:
   *         $ref: '#/components/responses/NotFound'
   *       Default:
   *         $ref: '#/components/responses/DefaultError'
   */
  async delete(req, res, next) {
    try {
      const { session } = req;

      res.status(responses.codes.SUCCESS).json({
        message: responses.messages.SUCCESS,
        data: null,
      });
      next();
    } catch (error) {
      switch (error.message) {
        case 'SENTINEL_NOT_FOUND':
          res.status(responses.codes.BAD_REQUEST).send({ message: error.message });
          break;
        default:
      }
      next(error);
    }
  },

  /**
   * @swagger
   *
   * /doctor/{doctorId}/treatment/{doctorTreatmentId}/session/{sessionId}:
   *   delete:
   *     description: Delete a Doctor
   *     tags:
   *       - Doctor
   *     security:
   *       - JWT: []
   *       - Requesting: []
   *     produces:
   *       - application/json
   *     parameters:
   *       - in: path
   *         name: doctorId
   *         required: true
   *         schema:
   *           type: string
   *
   *     responses:
   *       200:
   *         $ref: '#/components/responses/Success'
   *
   *       400:
   *         $ref: '#/components/responses/BadRequest'
   *       401:
   *         $ref: '#/components/responses/Unauthorized'
   *       404:
   *         $ref: '#/components/responses/NotFound'
   *       Default:
   *         $ref: '#/components/responses/DefaultError'
   */
  async deleteSessionFromTreatment(req, res, next) {
    try {
      const { session } = req;
      const { doctorId, sessionId, doctorTreatmentId } = req.params;

      const doctorTreatment = await models.DoctorTreatment.findById(doctorTreatmentId, null, {
        session,
      });

      if (!doctorTreatment) throw new Error('DOCTOR_TREATMENT_NOT_FOUND');

      const sessionFound = await models.Session.findOneAndDelete({
        _id: Types.ObjectId(sessionId),
        doctor: Types.ObjectId(doctorId),
        treatment: doctorTreatment.treatment,
      }).session(session);

      if (!sessionFound) throw new Error('SESSION_NOT_FOUND');

      const SessionHistory = await models.Session.getHistoryModel();
      await SessionHistory.deleteMany({
        parent: sessionFound._id,
      }).session(session);

      res.status(responses.codes.SUCCESS).json({
        message: responses.messages.SUCCESS,
        data: null,
      });
      next();
    } catch (error) {
      switch (error.message) {
        case 'SESSION_NOT_FOUND':
          res.status(responses.codes.BAD_REQUEST).send({ message: error.message });
          break;
        case 'DOCTOR_TREATMENT_NOT_FOUND':
          res.status(responses.codes.BAD_REQUEST).send({ message: error.message });
          break;
        default:
      }
      next(error);
    }
  },

  /**
   * TODO Implement
   * TODO Complete docs
   * TODO Add permission
   */

  /**
   * @swagger
   *
   * /doctor/{doctorId}:
   *   put:
   *     description: Update a Doctor
   *     tags:
   *       - Doctor
   *     security:
   *       - JWT: []
   *       - Requesting: []
   *     produces:
   *       - application/json
   *     parameters:
   *       - in: path
   *         name: doctorId
   *         required: true
   *         schema:
   *           type: string
   *     requestBody:
   *       required: true
   *       content:
   *         application/json:
   *           schema:
   *             type: object
   *             properties:
   *               email:
   *                 type: string
   *               phone:
   *                 type: string
   *           examples:
   *             Phone:
   *               value:
   *                 phone: "1234567890"
   *             Cellphone:
   *               value:
   *                 cellphone: "1234567890"
   *             Email:
   *               value:
   *                 email: email@domain.com
   *
   *     responses:
   *       200:
   *         $ref: '#/components/responses/Success'
   *
   *       400:
   *         $ref: '#/components/responses/BadRequest'
   *       401:
   *         $ref: '#/components/responses/Unauthorized'
   *       404:
   *         $ref: '#/components/responses/NotFound'
   *       Default:
   *         $ref: '#/components/responses/DefaultError'
   */
  async update(req, res, next) {
    try {
      const { session } = req;

      res.status(responses.codes.SUCCESS).json({
        message: responses.messages.SUCCESS,
        data: null,
      });
      next();
    } catch (error) {
      switch (error.message) {
        case 'SENTINEL_NOT_FOUND':
          res.status(responses.codes.BAD_REQUEST).send({ message: error.message });
          break;
        default:
      }
      next(error);
    }
  },

  /**
   * @swagger
   *
   * /doctor/{doctorId}/survey/{doctorSurveyId}:
   *   put:
   *     description: Update a Doctor
   *     tags:
   *       - Doctor
   *     security:
   *       - JWT: []
   *       - Requesting: []
   *     produces:
   *       - application/json
   *     parameters:
   *       - in: path
   *         name: doctorId
   *         required: true
   *         schema:
   *           type: string
   *     requestBody:
   *       required: true
   *       content:
   *         application/json:
   *           schema:
   *             type: object
   *             properties:
   *               email:
   *                 type: string
   *               phone:
   *                 type: string
   *           examples:
   *             Phone:
   *               value:
   *                 phone: "1234567890"
   *             Cellphone:
   *               value:
   *                 cellphone: "1234567890"
   *             Email:
   *               value:
   *                 email: email@domain.com
   *
   *     responses:
   *       200:
   *         $ref: '#/components/responses/Success'
   *
   *       400:
   *         $ref: '#/components/responses/BadRequest'
   *       401:
   *         $ref: '#/components/responses/Unauthorized'
   *       404:
   *         $ref: '#/components/responses/NotFound'
   *       Default:
   *         $ref: '#/components/responses/DefaultError'
   */
  async updateDoctorSurvey(req, res, next) {
    try {
      const { session } = req;
      const { doctorId, doctorSurveyId } = req.params;
      const {
        doctorDate,
        sickDate,
        sickSurvey,
        sick,
        survey,
        sickSurveyStatus,
        doctorSurveyStatus,
      } = req.body;

      const doctorSurveyUpdated = await models.DoctorSurvey.findOneAndUpdate(
        {
          doctor: Types.ObjectId(doctorId),
          _id: Types.ObjectId(doctorSurveyId),
          survey: Types.ObjectId(survey),
        },
        { $set: { status: doctorSurveyStatus, date: doctorDate } },
        { session }
      );
      const sickSurveyUpdated = await models.SickSurvey.findOneAndUpdate(
        {
          sick: Types.ObjectId(sick),
          _id: Types.ObjectId(sickSurvey),
          survey: Types.ObjectId(survey),
        },
        { $set: { status: sickSurveyStatus, date: sickDate } },
        { session }
      );

      res.status(responses.codes.SUCCESS).json({
        message: responses.messages.SUCCESS,
        data: { doctorSurveyUpdated, sickSurveyUpdated },
      });
      next();
    } catch (error) {
      switch (error.message) {
        case 'SENTINEL_NOT_FOUND':
          res.status(responses.codes.BAD_REQUEST).send({ message: error.message });
          break;
        default:
      }
      next(error);
    }
  },

  /**
   * @swagger
   *
   * /doctor/{doctorId}:
   *   put:
   *     description: Update a Doctor
   *     tags:
   *       - Doctor
   *     security:
   *       - JWT: []
   *       - Requesting: []
   *     produces:
   *       - application/json
   *     parameters:
   *       - in: path
   *         name: doctorId
   *         required: true
   *         schema:
   *           type: string
   *     requestBody:
   *       required: true
   *       content:
   *         application/json:
   *           schema:
   *             type: object
   *             properties:
   *               email:
   *                 type: string
   *               phone:
   *                 type: string
   *           examples:
   *             Phone:
   *               value:
   *                 phone: "1234567890"
   *             Cellphone:
   *               value:
   *                 cellphone: "1234567890"
   *             Email:
   *               value:
   *                 email: email@domain.com
   *
   *     responses:
   *       200:
   *         $ref: '#/components/responses/Success'
   *
   *       400:
   *         $ref: '#/components/responses/BadRequest'
   *       401:
   *         $ref: '#/components/responses/Unauthorized'
   *       404:
   *         $ref: '#/components/responses/NotFound'
   *       Default:
   *         $ref: '#/components/responses/DefaultError'
   */
  async updateDoctorTreatment(req, res, next) {
    try {
      const { session } = req;
      const { doctorId, doctorTreatmentId } = req.params;
      const {
        sickTreatment,
        sick,
        treatment,
        treatmentDays,
        sickTreatmentStatus,
        doctorTreatmentStatus,
      } = req.body;
      const treatmentUpdated = await models.Treatment.findOneAndUpdate(
        {
          owner: Types.ObjectId(doctorId),
          _id: Types.ObjectId(treatment),
        },
        { $set: { treatmentDays } },
        { session }
      );
      const doctorTreatmentUpdated = await models.DoctorTreatment.findOneAndUpdate(
        {
          doctor: Types.ObjectId(doctorId),
          _id: Types.ObjectId(doctorTreatmentId),
          treatment: Types.ObjectId(treatment),
        },
        { $set: { status: doctorTreatmentStatus } },
        { session }
      );
      const sickTreatmentUpdated = await models.SickTreatment.findOneAndUpdate(
        {
          sick: Types.ObjectId(sick),
          _id: Types.ObjectId(sickTreatment),
          treatment: Types.ObjectId(treatment),
        },
        { $set: { status: sickTreatmentStatus } },
        { session }
      );

      res.status(responses.codes.SUCCESS).json({
        message: responses.messages.SUCCESS,
        data: { doctorTreatmentUpdated, sickTreatmentUpdated, treatmentUpdated },
      });
      next();
    } catch (error) {
      switch (error.message) {
        case 'SENTINEL_NOT_FOUND':
          res.status(responses.codes.BAD_REQUEST).send({ message: error.message });
          break;
        default:
      }
      next(error);
    }
  },

  /**
   * @swagger
   *
   * /doctor/{doctorId}:
   *   put:
   *     description: Update a Doctor
   *     tags:
   *       - Doctor
   *     security:
   *       - JWT: []
   *       - Requesting: []
   *     produces:
   *       - application/json
   *     parameters:
   *       - in: path
   *         name: doctorId
   *         required: true
   *         schema:
   *           type: string
   *     requestBody:
   *       required: true
   *       content:
   *         application/json:
   *           schema:
   *             type: object
   *             properties:
   *               email:
   *                 type: string
   *               phone:
   *                 type: string
   *           examples:
   *             Phone:
   *               value:
   *                 phone: "1234567890"
   *             Cellphone:
   *               value:
   *                 cellphone: "1234567890"
   *             Email:
   *               value:
   *                 email: email@domain.com
   *
   *     responses:
   *       200:
   *         $ref: '#/components/responses/Success'
   *
   *       400:
   *         $ref: '#/components/responses/BadRequest'
   *       401:
   *         $ref: '#/components/responses/Unauthorized'
   *       404:
   *         $ref: '#/components/responses/NotFound'
   *       Default:
   *         $ref: '#/components/responses/DefaultError'
   */
  async updateSessionFromTreatment(req, res, next) {
    try {
      const { session } = req;
      const { doctorId, doctorTreatmentId, sessionId } = req.params;
      const { ...body } = req.body;
      console.log(body);

      const doctorTreatment = await models.DoctorTreatment.findOne(
        {
          doctor: Types.ObjectId(doctorId),
          _id: Types.ObjectId(doctorTreatmentId),
        },
        null,
        { session }
      );

      if (!doctorTreatment) throw new Error('DOCTOR_TREATMENT_NOT_FOUND');

      const sessionUpdated = await models.Session.findOneAndUpdate(
        {
          doctor: Types.ObjectId(doctorId),
          _id: Types.ObjectId(sessionId),
          treatment: doctorTreatment.treatment,
        },
        { $set: { ...body } },
        { session }
      );

      if (!sessionUpdated) throw new Error('SESSION_NOT_FOUND');

      res.status(responses.codes.SUCCESS).json({
        message: responses.messages.SUCCESS,
        data: { sessionUpdated },
      });
      next();
    } catch (error) {
      switch (error.message) {
        case 'DOCTOR_TREATMENT_NOT_FOUND':
          res.status(responses.codes.BAD_REQUEST).send({ message: error.message });
          break;
        case 'SESSION_NOT_FOUND':
          res.status(responses.codes.BAD_REQUEST).send({ message: error.message });
          break;
        default:
      }
      next(error);
    }
  },

  /**
   * @swagger
   *
   * /sick/{sentinelId}:
   *   put:
   *     description: Update a Sick
   *     tags:
   *       - Sick
   *     security:
   *       - JWT: []
   *       - Requesting: []
   *     produces:
   *       - application/json
   *     parameters:
   *       - in: path
   *         name: sentinelId
   *         required: true
   *         schema:
   *           type: string
   *     requestBody:
   *       required: true
   *       content:
   *         application/json:
   *           schema:
   *             type: object
   *             properties:
   *               email:
   *                 type: string
   *               phone:
   *                 type: string
   *           examples:
   *             Phone:
   *               value:
   *                 phone: "1234567890"
   *             Cellphone:
   *               value:
   *                 cellphone: "1234567890"
   *             Email:
   *               value:
   *                 email: email@domain.com
   *
   *     responses:
   *       200:
   *         $ref: '#/components/responses/Success'
   *
   *       400:
   *         $ref: '#/components/responses/BadRequest'
   *       401:
   *         $ref: '#/components/responses/Unauthorized'
   *       404:
   *         $ref: '#/components/responses/NotFound'
   *       Default:
   *         $ref: '#/components/responses/DefaultError'
   */
  async updateSickFromDoctor(req, res, next) {
    try {
      const { session } = req;
      const { doctorId, doctorSickId } = req.params;
      const { ...body } = req.body;

      const rules = {
        accepted: ['closed', 'accepted'],
        waiting: ['accepted', 'denied', 'waiting'],
        denied: ['waiting', 'denied'],
        closed: ['accepted', 'closed'],
      };

      const doctorSickUpdated = await models.SickDoctor.findOneAndUpdate(
        { doctor: Types.ObjectId(doctorId), _id: Types.ObjectId(doctorSickId) },
        { $set: { ...body } },
        { session }
      );

      if (!doctorSickUpdated) throw new Error('SICK_DOCTOR_NOT_FOUND');

      if (!rules[doctorSickUpdated.status].includes(body.status)) {
        throw new Error('STATUS_NOT_ALLOWED');
      }

      res.status(responses.codes.SUCCESS).json({
        message: responses.messages.SUCCESS,
        data: doctorSickUpdated,
      });
      next();
    } catch (error) {
      switch (error.message) {
        case 'STATUS_NOT_ALLOWED':
          res.status(responses.codes.BAD_REQUEST).send({ message: error.message });
          break;
        case 'SICK_DOCTOR_NOT_FOUND':
          res.status(responses.codes.BAD_REQUEST).send({ message: error.message });
          break;
        default:
      }
      next(error);
    }
  },
};
