const { celebrate, Joi } = require('celebrate');
const { JoiPagination, JoiStatus } = require('../../utils/JoiModels');

module.exports = {
  add() {
    return celebrate({
      body: Joi.object()
        .keys({
          user: Joi.string().required(),
          diseases: Joi.array()
            .items(Joi.string())
            .required()
            .min(1),
        })
        .required(),
    });
  },

  addSick() {
    return celebrate({
      body: Joi.object()
        .keys({
          shortName: Joi.string().required(),
          email: Joi.string().required(),
        })
        .required(),
    });
  },

  addTreatment() {
    return celebrate({
      body: Joi.object()
        .keys({
          description: Joi.string().required(),
          sick: Joi.string().required(),
          disease: Joi.string(),
          treatmentDays: Joi.number(),
          steps: Joi.array(),
        })
        .required(),
    });
  },
  addSessionToTreatment() {
    return celebrate({
      body: Joi.object()
        .keys({
          doctor: Joi.string().required(),
          doctorTreatment: Joi.string().required(),
          date: Joi.date().required(),
          frequency: Joi.number(),
          intensity: Joi.number(),
          seriesNumber: Joi.number(),
          intervalTime: Joi.number(),
          seriesTime: Joi.number(),
          pulsesNumber: Joi.number(),
          applicationLocal: Joi.number(),
          status: Joi.string().regex(JoiStatus),
        })
        .required(),
    });
  },

  addSurvey() {
    return celebrate({
      body: Joi.object()
        .keys({
          doctorTreatment: Joi.string().required(),
          questions: Joi.array(),
          doctorDate: Joi.date(),
          sickDate: Joi.date(),
        })
        .required(),
    });
  },

  addDevice() {
    return celebrate({
      body: Joi.object()
        .keys({
          serialNumber: Joi.string().required(),
        })
        .required(),
    });
  },

  addDisease() {
    return celebrate({
      body: Joi.object()
        .keys({
          disease: Joi.string().required(),
        })
        .required(),
    });
  },

  get() {
    return celebrate({
      query: Joi.object().keys({
        pagination: JoiPagination(1, 100),
      }),
    });
  },

  getDiseases() {
    return celebrate({
      query: Joi.object().keys({
        pagination: JoiPagination(1, 100),
      }),
    });
  },

  getDevices() {
    return celebrate({
      query: Joi.object().keys({
        pagination: JoiPagination(1, 100),
      }),
    });
  },

  getTreatments() {
    return celebrate({
      query: Joi.object().keys({
        pagination: JoiPagination(1, 100),
      }),
    });
  },

  getTreatmentSessions() {
    return celebrate({
      query: Joi.object().keys({
        pagination: JoiPagination(1, 100),
      }),
    });
  },

  getSurveys() {
    return celebrate({
      query: Joi.object().keys({
        pagination: JoiPagination(1, 100),
        doctorTreatment: Joi.string(),
      }),
    });
  },

  getSicks() {
    return celebrate({
      query: Joi.object().keys({
        pagination: JoiPagination(1, 100),
      }),
    });
  },

  update() {
    return celebrate({
      body: Joi.object()
        .keys({})
        .required(),
    });
  },
  updateDoctorSurvey() {
    return celebrate({
      body: Joi.object()
        .keys({
          doctorDate: Joi.date().required(),
          sickDate: Joi.date().required(),
          sickSurvey: Joi.string().required(),
          sick: Joi.string().required(),
          survey: Joi.string().required(),
          sickSurveyStatus: Joi.string().regex(JoiStatus),
          doctorSurveyStatus: Joi.string().regex(JoiStatus),
        })
        .required(),
    });
  },
  updateDoctorTreatment() {
    return celebrate({
      body: Joi.object()
        .keys({
          sickTreatment: Joi.string().required(),
          sick: Joi.string().required(),
          treatment: Joi.string().required(),
          treatmentDays: Joi.number().required(),
          sickTreatmentStatus: Joi.string().regex(JoiStatus),
          doctorTreatmentStatus: Joi.string().regex(JoiStatus),
        })
        .required(),
    });
  },
  updateSessionFromTreatment() {
    return celebrate({
      body: Joi.object()
        .keys({
          status: Joi.string().regex(JoiStatus),
          date: Joi.date().required(),
          frequency: Joi.number(),
          intensity: Joi.number(),
          seriesNumber: Joi.number(),
          intervalTime: Joi.number(),
          pulsesNumber: Joi.number(),
          applicationLocal: Joi.number(),
        })
        .required(),
    });
  },
  updateSick() {
    return celebrate({
      body: Joi.object()
        .keys({
          status: Joi.string().regex(JoiStatus),
          diagnostics: Joi.string().allow('', null),
          motorThreshold: Joi.number(),
          intensity: Joi.number(),
          symptoms: Joi.array(),
          treatments: Joi.array(),
          medicines: Joi.array(),
          exams: Joi.array(),
        })
        .required(),
    });
  },
};
