const mongoose = require('mongoose');

const { Schema } = mongoose;

const TicketSchema = new Schema(
  {
    att: {
      type: Schema.Types.ObjectId,
      ref: 'Model',
    },
  },
  {
    timestamps: {
      createdAt: 'createdAt',
    },
  }
);

module.exports = mongoose.model('Ticket', TicketSchema);
