const { celebrate, Joi } = require('celebrate');

module.exports = {
  get() {
    return celebrate({
      query: Joi.object()
        .keys({
          att: Joi.string(),
        })
        .required(),
    });
  },
};
