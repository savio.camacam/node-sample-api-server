const models = require('../../models');
const responses = require('../../utils/responses');
const { permissionGuard } = require('../../utils/permissions');

module.exports = {
  /**
   * TODO Implement
   * TODO Complete docs
   * TODO Add permission
   */

  /**
   * @swagger
   *
   * /sentinel:
   *   post:
   *     description: Update push notification reference
   *     tags:
   *       - Sentinel
   *     security:
   *       - JWT: []
   *     produces:
   *       - application/json
   *     requestBody:
   *       required: true
   *       content:
   *         application/json:
   *           schema:
   *             type: object
   *             properties:
   *               att1:
   *                 type: string
   *                 example: old
   *               att2:
   *                 type: string
   *                 example: new
   *
   *     responses:
   *       200:
   *         description: Success
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/schemas/Response'
   *             example:
   *               message: 'SUCCESS'
   *               data: xxxxxxxxxxxxxxxxxxxxxxxx
   *
   *       400:
   *         $ref: '#/components/responses/BadRequest'
   *       401:
   *         $ref: '#/components/responses/Unauthorized'
   *       404:
   *         $ref: '#/components/responses/NotFound'
   *       Default:
   *         $ref: '#/components/responses/DefaultError'
   */
  async add(req, res, next) {
    try {
      const { session } = req;

      res.status(responses.codes.SUCCESS).json({
        message: responses.messages.SUCCESS,
        data: null,
      });
      next();
    } catch (error) {
      switch (error.message) {
        case 'SENTINEL_NOT_FOUND':
          res.status(responses.codes.BAD_REQUEST).send({ message: error.message });
          break;
        default:
      }
      next(error);
    }
  },

  /**
   * TODO Implement
   * TODO Complete docs
   * TODO Add permission
   */

  /**
   * @swagger
   *
   * /sentinel:
   *   get:
   *     description: Get all Sentinels
   *     tags:
   *       - Sentinel
   *     security:
   *       - JWT: []
   *       - Requesting: []
   *     produces:
   *       - application/json
   *     parameters:
   *       - $ref: '#/components/parameters/Pagination'
   *       - in: query
   *         name: checked
   *
   *         schema:
   *           type: string
   *       - in: query
   *         name: type
   *
   *         schema:
   *           type: string
   *       - in: query
   *         name: user
   *
   *         schema:
   *           type: string
   *       - in: query
   *         name: createdAt
   *
   *         schema:
   *           type: string
   *       - in: query
   *         name: updatedAt
   *
   *         schema:
   *           type: string
   *
   *     responses:
   *       200:
   *         description: Success
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/schemas/Response'
   *             example:
   *               message: SUCCESS
   *               metadata:
   *                 total: 1826
   *                 page: 1
   *                 limit: 5
   *               data:
   *                 - _id: xxxxxxxxxxxxxxxxxxxxxxxx
   *                   checked: true
   *                   address: 1234567890
   *                   createdAt: 2019-02-18T22:31:48.039Z
   *                   updatedAt: 2019-02-18T22:31:48.039Z
   *                   user: "@user"
   *       400:
   *         $ref: '#/components/responses/BadRequest'
   *       401:
   *         $ref: '#/components/responses/Unauthorized'
   *       404:
   *         $ref: '#/components/responses/NotFound'
   *       Default:
   *         $ref: '#/components/responses/DefaultError'
   */
  async get(req, res, next) {
    try {
      const { session } = req;

      res.status(responses.codes.SUCCESS).json({
        message: responses.messages.SUCCESS,
        data: null,
      });
      next();
    } catch (error) {
      switch (error.message) {
        case 'SENTINEL_NOT_FOUND':
          res.status(responses.codes.BAD_REQUEST).send({ message: error.message });
          break;
        default:
      }
      next(error);
    }
  },

  /**
   * TODO Implement
   * TODO Complete docs
   * TODO Add permission
   */

  /**
   * @swagger
   *
   * /sentinel/{sentinelId}:
   *   get:
   *     description: Get an Sentinel
   *     tags:
   *       - Sentinel
   *     security:
   *       - JWT: []
   *       - Requesting: []
   *     produces:
   *       - application/json
   *     parameters:
   *       - in: path
   *         name: sentinelId
   *         required: true
   *         schema:
   *           type: string
   *
   *     responses:
   *       200:
   *         description: Success
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/schemas/Response'
   *             example:
   *               message: SUCCESS
   *               data:
   *                 _id: xxxxxxxxxxxxxxxxxxxxxxxx
   *                 att1: true
   *                 att2: text
   *
   *       400:
   *         $ref: '#/components/responses/BadRequest'
   *       401:
   *         $ref: '#/components/responses/Unauthorized'
   *       404:
   *         $ref: '#/components/responses/NotFound'
   *       Default:
   *         $ref: '#/components/responses/DefaultError'
   */
  async getById(req, res, next) {
    try {
      const { session } = req;

      res.status(responses.codes.SUCCESS).json({
        message: responses.messages.SUCCESS,
        data: null,
      });
      next();
    } catch (error) {
      switch (error.message) {
        case 'SENTINEL_NOT_FOUND':
          res.status(responses.codes.BAD_REQUEST).send({ message: error.message });
          break;
        default:
      }
      next(error);
    }
  },
  /**
   * TODO Implement
   * TODO Complete docs
   * TODO Add permission
   */

  /**
   * @swagger
   *
   * /sentinel/{sentinelId}:
   *   delete:
   *     description: Delete a Sentinel
   *     tags:
   *       - Sentinel
   *     security:
   *       - JWT: []
   *       - Requesting: []
   *     produces:
   *       - application/json
   *     parameters:
   *       - in: path
   *         name: sentinelId
   *         required: true
   *         schema:
   *           type: string
   *
   *     responses:
   *       200:
   *         $ref: '#/components/responses/Success'
   *
   *       400:
   *         $ref: '#/components/responses/BadRequest'
   *       401:
   *         $ref: '#/components/responses/Unauthorized'
   *       404:
   *         $ref: '#/components/responses/NotFound'
   *       Default:
   *         $ref: '#/components/responses/DefaultError'
   */
  async delete(req, res, next) {
    try {
      const { session } = req;

      res.status(responses.codes.SUCCESS).json({
        message: responses.messages.SUCCESS,
        data: null,
      });
      next();
    } catch (error) {
      switch (error.message) {
        case 'SENTINEL_NOT_FOUND':
          res.status(responses.codes.BAD_REQUEST).send({ message: error.message });
          break;
        default:
      }
      next(error);
    }
  },

  /**
   * TODO Implement
   * TODO Complete docs
   * TODO Add permission
   */

  /**
   * @swagger
   *
   * /sentinel/{sentinelId}:
   *   put:
   *     description: Update a Sentinel
   *     tags:
   *       - Sentinel
   *     security:
   *       - JWT: []
   *       - Requesting: []
   *     produces:
   *       - application/json
   *     parameters:
   *       - in: path
   *         name: sentinelId
   *         required: true
   *         schema:
   *           type: string
   *     requestBody:
   *       required: true
   *       content:
   *         application/json:
   *           schema:
   *             type: object
   *             properties:
   *               email:
   *                 type: string
   *               phone:
   *                 type: string
   *           examples:
   *             Phone:
   *               value:
   *                 phone: "1234567890"
   *             Cellphone:
   *               value:
   *                 cellphone: "1234567890"
   *             Email:
   *               value:
   *                 email: email@domain.com
   *
   *     responses:
   *       200:
   *         $ref: '#/components/responses/Success'
   *
   *       400:
   *         $ref: '#/components/responses/BadRequest'
   *       401:
   *         $ref: '#/components/responses/Unauthorized'
   *       404:
   *         $ref: '#/components/responses/NotFound'
   *       Default:
   *         $ref: '#/components/responses/DefaultError'
   */
  async update(req, res, next) {
    try {
      const { session } = req;

      res.status(responses.codes.SUCCESS).json({
        message: responses.messages.SUCCESS,
        data: null,
      });
      next();
    } catch (error) {
      switch (error.message) {
        case 'SENTINEL_NOT_FOUND':
          res.status(responses.codes.BAD_REQUEST).send({ message: error.message });
          break;
        default:
      }
      next(error);
    }
  },
};
