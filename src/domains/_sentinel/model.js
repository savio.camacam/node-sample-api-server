const mongoose = require('mongoose');

const { Schema } = mongoose;

const ModelSchema = new Schema(
  {
    att: {
      type: Schema.Types.ObjectId,
      ref: 'Model',
    },
  },
  {
    timestamps: {
      createdAt: 'createdAt',
    },
  }
);
// ModelSchema.index({ createdAt: 1 }, { expireAfterSeconds: 600 });

module.exports = mongoose.model('Model', ModelSchema);
