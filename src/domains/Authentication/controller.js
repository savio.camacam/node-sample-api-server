const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const models = require('../../models');
const responses = require('../../utils/responses');
const config = require('../../../config/secrets.json');

module.exports = {
  /**
   * @swagger
   *
   * /authentication/login:
   *   post:
   *     description: This route make and return an access token by user credentials.
   *     tags:
   *       - Authentication
   *     produces:
   *       - application/json
   *     requestBody:
   *       required: true
   *       content:
   *         application/json:
   *           schema:
   *             description: user login information
   *             $ref: '#/components/schemas/Login'
   *
   *     responses:
   *       200:
   *         description: Success
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/schemas/Response'
   *             example:
   *               message: 'SUCCESS'
   *               data:
   *                 token: 'token_string'
   *
   *       400:
   *         $ref: '#/components/responses/BadRequest'
   *       401:
   *         $ref: '#/components/responses/InvalidPassword'
   *       404:
   *         content:
   *           application/json:
   *             examples:
   *               User not found:
   *                 value:
   *                   message: 'USER_NOT_FOUND'
   *                   data: null
   *       Default:
   *         $ref: '#/components/responses/DefaultError'
   */
  async login(req, res, next) {
    try {
      const { session } = req;
      const { identifier, password } = req.body;
      let user = await models.User.findOne({ shortName: identifier }, null, {
        session,
      }).select('shortName password');

      if (!user) {
        const mainEmail = await models.Contact.findOne({ address: identifier }, null, {
          session,
        });

        if (mainEmail) {
          user = await models.User.findById(mainEmail.user, null, { session }).select(
            'shortName password'
          );
        }
      }

      if (!user) {
        const mainPhone = await models.Contact.findOne({ address: identifier }, null, {
          session,
        });

        if (mainPhone) {
          user = await models.User.findById(mainPhone.user, null, { session }).select(
            'shortName password'
          );
        }
      }

      if (!user) throw new Error('USER_NOT_FOUND');

      const passwordMatch = await bcrypt.compare(password, user.password);

      if (!passwordMatch) throw new Error('INVALID_PASSWORD');

      const token = jwt.sign({ shortName: user.shortName, user: user._id }, config.JWT_SECRET, {
        expiresIn: 60 * 60 * 24 * 7,
      });

      res
        .status(responses.codes.SUCCESS)
        .json({ message: responses.messages.SUCCESS, data: { token, user: user._id } });

      next();
    } catch (error) {
      [
        { message: 'USER_NOT_FOUND', code: responses.codes.NOT_FOUND },
        { message: 'CONTACT_NOT_FOUND', code: responses.codes.NOT_FOUND },
        { message: 'MAIN_PHONE_NOT_VERIFIED', code: responses.codes.NOT_FOUND },
        { message: 'MAIN_EMAIL_NOT_VERIFIED', code: responses.codes.NOT_FOUND },
        { message: 'INVALID_PASSWORD', code: responses.codes.UNAUTHORIZED },
      ].forEach(({ message, code }) => {
        if (error.message === message) res.status(code).json({ message, data: null });
      });
      next(error);
    }
  },
};
