const router = require('express').Router();
const controller = require('./controller');
const validations = require('./validations');

router.post('/login', validations.login(), controller.login);
module.exports = router;
