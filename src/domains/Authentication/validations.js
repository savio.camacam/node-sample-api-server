const { celebrate, Joi } = require('celebrate');
const { JoiPagination } = require('../../utils/JoiModels');

module.exports = {
  login() {
    return celebrate({
      body: Joi.object()
        .keys({
          // shortName | email | cellphone
          identifier: Joi.string().required(),
          password: Joi.string()
            .min(8)
            .required(),
        })
        .required(),
    });
  },

  sendPasswordRecoverEmail() {
    return celebrate({
      body: Joi.object().keys({
        email: Joi.string()
          .email()
          .required(),
      }),
    });
  },

  passwordRecoverConfirmation() {
    return celebrate({
      body: Joi.object()
        .keys({
          password: Joi.string()
            .min(8)
            .required(),
          token: Joi.string().required(),
        })
        .required(),
    });
  },

  getAllSchoolInstitutional() {
    return celebrate({
      query: Joi.object()
        .keys({
          countyInstitutional: Joi.string().required(),
          pagination: JoiPagination(1, 100),
          all: Joi.boolean().default(false),
        })
        .required(),
    });
  },

  getAllCountyInstitutional() {
    return celebrate({
      query: Joi.object()
        .keys({
          _t: Joi.string(),
          state: Joi.string(),
          city: Joi.string(),
          pagination: JoiPagination(1, 100),
          cityCode: Joi.number(),
        })
        .required(),
    });
  },

  whoAmI() {
    return celebrate({
      body: Joi.object()
        .keys({
          email: Joi.string().allow(''),
          phone: Joi.string().allow(''),
        })
        .required(),
    });
  },
};
