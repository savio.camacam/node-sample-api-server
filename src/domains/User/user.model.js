const mongoose = require('mongoose');

const { Schema } = mongoose;

const UserSchema = new Schema(
  {
    mainProfile: {
      type: Schema.Types.ObjectId,
      ref: 'Profile',
    },
    people: {
      type: Schema.Types.ObjectId,
      ref: 'People',
      required: true,
      unique: true,
    },
    mainPhone: {
      type: Schema.Types.ObjectId,
      ref: 'Contact',
      unique: true,
      sparse: true,
      required: true,
    },
    mainEmail: {
      type: Schema.Types.ObjectId,
      ref: 'Contact',
      unique: true,
      sparse: true,
      required: true,
    },
    push: {
      type: Schema.Types.ObjectId,
      ref: 'Contact',
      unique: true,
      sparse: true,
    },
    shortName: {
      type: Schema.Types.String,
      required: true,
      unique: true,
    },
    password: {
      type: Schema.Types.String,
      required: true,
      select: false,
    },
  },
  {
    timestamps: {
      createdAt: 'createdAt',
    },
  }
);

module.exports = mongoose.model('User', UserSchema);
