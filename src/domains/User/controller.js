const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const { Types } = require('mongoose');
const models = require('../../models');
const responses = require('../../utils/responses');
// const { permissionGuard } = require('../../utils/permissions');
const { paginate, addFields, makeLookup } = require('../../utils/aggregations');
const config = require('../../../config/secrets.json');
const { model } = require('../Sick/sick.model');

module.exports = {
  /**
   * TODO Implement
   * TODO Complete docs
   * TODO Add permission
   */

  /**
   * @swagger
   *
   * /user:
   *   post:
   *     description: Update push notification reference
   *     tags:
   *       - User
   *     security:
   *       - JWT: []
   *     produces:
   *       - application/json
   *     requestBody:
   *       required: true
   *       content:
   *         application/json:
   *           schema:
   *             type: object
   *             properties:
   *               att1:
   *                 type: string
   *                 example: old
   *               att2:
   *                 type: string
   *                 example: new
   *
   *     responses:
   *       200:
   *         description: Success
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/schemas/Response'
   *             example:
   *               message: 'SUCCESS'
   *               data: xxxxxxxxxxxxxxxxxxxxxxxx
   *
   *       400:
   *         $ref: '#/components/responses/BadRequest'
   *       401:
   *         $ref: '#/components/responses/Unauthorized'
   *       404:
   *         $ref: '#/components/responses/NotFound'
   *       Default:
   *         $ref: '#/components/responses/DefaultError'
   */
  async add(req, res, next) {
    try {
      const { session } = req;
      const { password, passwordConfirm, shortName, phoneAddress, emailAddress, name } = req.body;

      const contactEmail = await new models.Contact({
        type: 'email',
        address: emailAddress,
      }).save({
        session,
      });

      const contactPhone = await new models.Contact({
        type: 'phone',
        address: phoneAddress,
      }).save({ session });

      if (password !== passwordConfirm) throw new Error('DIFFERENT_PASSWORD_PASSWORDCONFIRM');

      const people = await new models.People({
        name,
      }).save({ session });

      const user = await new models.User({
        shortName,
        password: await bcrypt.hash(req.body.password, 12),
        mainEmail: contactEmail,
        mainPhone: contactPhone,
        people,
      }).save({ session });

      await models.Contact.updateMany(
        { $or: [contactEmail, contactPhone] },
        { $set: { user } },
        { session }
      );
      await models.People.updateMany({ _id: people }, { $set: { user } }, { session });

      const token = jwt.sign({ shortName: user.shortName }, config.JWT_SECRET, {
        expiresIn: 60 * 60 * 24 * 7,
      });

      delete user.password;

      res.status(responses.codes.SUCCESS).json({
        message: responses.messages.SUCCESS,
        data: { user, token },
      });
      next();
    } catch (error) {
      switch (error.message) {
        case 'SENTINEL_NOT_FOUND':
          res.status(responses.codes.BAD_REQUEST).send({ message: error.message });
          break;
        default:
      }
      next(error);
    }
  },

  /**
   * @swagger
   *
   * /user/{userId}/contact:
   *   post:
   *     description: Update push notification reference
   *     tags:
   *       - Sentinel
   *     security:
   *       - JWT: []
   *     produces:
   *       - application/json
   *     requestBody:
   *       required: true
   *       content:
   *         application/json:
   *           schema:
   *             type: object
   *             properties:
   *               att1:
   *                 type: string
   *                 example: old
   *               att2:
   *                 type: string
   *                 example: new
   *
   *     responses:
   *       200:
   *         description: Success
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/schemas/Response'
   *             example:
   *               message: 'SUCCESS'
   *               data: xxxxxxxxxxxxxxxxxxxxxxxx
   *
   *       400:
   *         $ref: '#/components/responses/BadRequest'
   *       401:
   *         $ref: '#/components/responses/Unauthorized'
   *       404:
   *         $ref: '#/components/responses/NotFound'
   *       Default:
   *         $ref: '#/components/responses/DefaultError'
   */
  async addContact(req, res, next) {
    try {
      const { session } = req;

      res.status(responses.codes.SUCCESS).json({
        message: responses.messages.SUCCESS,
        data: null,
      });
      next();
    } catch (error) {
      switch (error.message) {
        case 'SENTINEL_NOT_FOUND':
          res.status(responses.codes.BAD_REQUEST).send({ message: error.message });
          break;
        default:
      }
      next(error);
    }
  },

  /**
   * @swagger
   *
   * /user/{userId}/doctor:
   *   post:
   *     description: Update push notification reference
   *     tags:
   *       - Sentinel
   *     security:
   *       - JWT: []
   *     produces:
   *       - application/json
   *     requestBody:
   *       required: true
   *       content:
   *         application/json:
   *           schema:
   *             type: object
   *             properties:
   *               att1:
   *                 type: string
   *                 example: old
   *               att2:
   *                 type: string
   *                 example: new
   *
   *     responses:
   *       200:
   *         description: Success
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/schemas/Response'
   *             example:
   *               message: 'SUCCESS'
   *               data: xxxxxxxxxxxxxxxxxxxxxxxx
   *
   *       400:
   *         $ref: '#/components/responses/BadRequest'
   *       401:
   *         $ref: '#/components/responses/Unauthorized'
   *       404:
   *         $ref: '#/components/responses/NotFound'
   *       Default:
   *         $ref: '#/components/responses/DefaultError'
   */
  async addDoctor(req, res, next) {
    try {
      const { session } = req;
      const { userId: user } = req.params;
      const { diseases, crm } = req.body;

      const userFound = await models.User.findById(user, null, { session });

      if (!userFound) throw new Error('USER_NOT_FOUND');

      const doctorCreated = await models.Doctor.findOneAndUpdate(
        { user, crm },
        { $set: { user, crm } },
        { session, upsert: true, new: true }
      );

      const doctorDiseases = await Promise.all(
        diseases.map(async disease => {
          return models.DoctorDisease.create([{ doctor: doctorCreated, disease }], { session });
        })
      );

      res.status(responses.codes.SUCCESS).json({
        message: responses.messages.SUCCESS,
        data: { doctor: doctorCreated, diseases: doctorDiseases },
      });
      next();
    } catch (error) {
      switch (error.message) {
        case 'SENTINEL_NOT_FOUND':
          res.status(responses.codes.BAD_REQUEST).send({ message: error.message });
          break;
        default:
      }
      next(error);
    }
  },

  /**
   * @swagger
   *
   * /user/{userId}/sick:
   *   post:
   *     description: Update push notification reference
   *     tags:
   *       - Sentinel
   *     security:
   *       - JWT: []
   *     produces:
   *       - application/json
   *     requestBody:
   *       required: true
   *       content:
   *         application/json:
   *           schema:
   *             type: object
   *             properties:
   *               att1:
   *                 type: string
   *                 example: old
   *               att2:
   *                 type: string
   *                 example: new
   *
   *     responses:
   *       200:
   *         description: Success
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/schemas/Response'
   *             example:
   *               message: 'SUCCESS'
   *               data: xxxxxxxxxxxxxxxxxxxxxxxx
   *
   *       400:
   *         $ref: '#/components/responses/BadRequest'
   *       401:
   *         $ref: '#/components/responses/Unauthorized'
   *       404:
   *         $ref: '#/components/responses/NotFound'
   *       Default:
   *         $ref: '#/components/responses/DefaultError'
   */
  async addSick(req, res, next) {
    try {
      const { session } = req;
      const { userId: user } = req.params;
      const { diseases } = req.body;

      const userFound = await models.User.findById(user, null, { session });

      if (!userFound) throw new Error('USER_NOT_FOUND');

      const sickCreated = await models.Sick.findOneAndUpdate(
        { user },
        { $set: { user } },
        { session, upsert: true, new: true }
      );

      const sickDiseases = await Promise.all(
        diseases.map(async disease => {
          return models.SickDisease.create([{ sick: sickCreated, disease }], { session });
        })
      );

      res.status(responses.codes.SUCCESS).json({
        message: responses.messages.SUCCESS,
        data: { sick: sickCreated, diseases: sickDiseases },
      });
      next();
    } catch (error) {
      switch (error.message) {
        case 'SENTINEL_NOT_FOUND':
          res.status(responses.codes.BAD_REQUEST).send({ message: error.message });
          break;
        default:
      }
      next(error);
    }
  },

  /**
   * TODO Implement
   * TODO Complete docs
   * TODO Add permission
   */

  /**
   * @swagger
   *
   * /user:
   *   get:
   *     description: Get all Users
   *     tags:
   *       - User
   *     security:
   *       - JWT: []
   *       - Requesting: []
   *     produces:
   *       - application/json
   *     parameters:
   *       - $ref: '#/components/parameters/Pagination'
   *       - in: query
   *         name: checked
   *
   *         schema:
   *           type: string
   *       - in: query
   *         name: type
   *
   *         schema:
   *           type: string
   *       - in: query
   *         name: user
   *
   *         schema:
   *           type: string
   *       - in: query
   *         name: createdAt
   *
   *         schema:
   *           type: string
   *       - in: query
   *         name: updatedAt
   *
   *         schema:
   *           type: string
   *
   *     responses:
   *       200:
   *         description: Success
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/schemas/Response'
   *             example:
   *               message: SUCCESS
   *               metadata:
   *                 total: 1826
   *                 page: 1
   *                 limit: 5
   *               data:
   *                 - _id: xxxxxxxxxxxxxxxxxxxxxxxx
   *                   checked: true
   *                   address: 1234567890
   *                   createdAt: 2019-02-18T22:31:48.039Z
   *                   updatedAt: 2019-02-18T22:31:48.039Z
   *                   user: "@user"
   *       400:
   *         $ref: '#/components/responses/BadRequest'
   *       401:
   *         $ref: '#/components/responses/Unauthorized'
   *       404:
   *         $ref: '#/components/responses/NotFound'
   *       Default:
   *         $ref: '#/components/responses/DefaultError'
   */
  async get(req, res, next) {
    try {
      const { session } = req;

      res.status(responses.codes.SUCCESS).json({
        message: responses.messages.SUCCESS,
        data: null,
      });
      next();
    } catch (error) {
      switch (error.message) {
        case 'SENTINEL_NOT_FOUND':
          res.status(responses.codes.BAD_REQUEST).send({ message: error.message });
          break;
        default:
      }
      next(error);
    }
  },

  /**
   * TODO Implement
   * TODO Complete docs
   * TODO Add permission
   */

  /**
   * @swagger
   *
   * /user/{userId}:
   *   get:
   *     description: Get an User
   *     tags:
   *       - User
   *     security:
   *       - JWT: []
   *       - Requesting: []
   *     produces:
   *       - application/json
   *     parameters:
   *       - in: path
   *         name: userId
   *         required: true
   *         schema:
   *           type: string
   *
   *     responses:
   *       200:
   *         description: Success
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/schemas/Response'
   *             example:
   *               message: SUCCESS
   *               data:
   *                 _id: xxxxxxxxxxxxxxxxxxxxxxxx
   *                 att1: true
   *                 att2: text
   *
   *       400:
   *         $ref: '#/components/responses/BadRequest'
   *       401:
   *         $ref: '#/components/responses/Unauthorized'
   *       404:
   *         $ref: '#/components/responses/NotFound'
   *       Default:
   *         $ref: '#/components/responses/DefaultError'
   */
  async getById(req, res, next) {
    try {
      const { session } = req;
      const { userId } = req.params;
      const { user: userAuthenticated } = res.locals;
      let user;
      if (userId !== 'null') {
        user = userId;
      } else {
        user = userAuthenticated._id.toString();
      }
      const [userFound] = await models.User.aggregate([
        addFields([{ prop: '_id', type: 'String' }]),
        { $match: { _id: user } },
        { $unset: ['password'] },
        addFields([{ prop: '_id', type: 'ObjectId' }]),
        ...makeLookup('profiles', '_id', 'user', 'profiles', false, true),
        ...makeLookup('profiles', 'mainProfile', '_id', 'mainProfileObj', true, true),
        ...makeLookup('contacts', '_id', 'user', 'contacts', false),
        ...makeLookup('contacts', 'mainPhone', '_id', 'mainPhoneObj', true),
        ...makeLookup('contacts', 'mainEmail', '_id', 'mainEmailObj', true),
        ...makeLookup('peoples', 'people', '_id', 'peopleObj', true),
      ]).option({ session });
      res.status(responses.codes.SUCCESS).json({
        message: responses.messages.SUCCESS,
        data: userFound,
      });

      next();
    } catch (error) {
      switch (error.message) {
        case 'SENTINEL_NOT_FOUND':
          res.status(responses.codes.BAD_REQUEST).send({ message: error.message });
          break;
        default:
      }
      next(error);
    }
  },

  /**
   * TODO Implement
   * TODO Complete docs
   * TODO Add permission
   */

  /**
   * @swagger
   *
   * /user/{userId}/profiles:
   *   get:
   *     description: Get an User
   *     tags:
   *       - User
   *     security:
   *       - JWT: []
   *       - Requesting: []
   *     produces:
   *       - application/json
   *     parameters:
   *       - in: path
   *         name: userId
   *         required: true
   *         schema:
   *           type: string
   *
   *     responses:
   *       200:
   *         description: Success
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/schemas/Response'
   *             example:
   *               message: SUCCESS
   *               data:
   *                 _id: xxxxxxxxxxxxxxxxxxxxxxxx
   *                 att1: true
   *                 att2: text
   *
   *       400:
   *         $ref: '#/components/responses/BadRequest'
   *       401:
   *         $ref: '#/components/responses/Unauthorized'
   *       404:
   *         $ref: '#/components/responses/NotFound'
   *       Default:
   *         $ref: '#/components/responses/DefaultError'
   */
  async getProfiles(req, res, next) {
    try {
      const { session } = req;
      const { pagination } = req.query;
      const { userId: user } = req.params;

      const result = await paginate(models.Profile, pagination, { session }, [
        addFields([{ prop: 'user', type: 'String' }]),
        { $match: { user } },
      ]);

      res.status(responses.codes.SUCCESS).json({
        message: responses.messages.SUCCESS,
        ...result,
      });
      next();
    } catch (error) {
      switch (error.message) {
        case 'SENTINEL_NOT_FOUND':
          res.status(responses.codes.BAD_REQUEST).send({ message: error.message });
          break;
        default:
      }
      next(error);
    }
  },

  /**
   * TODO Implement
   * TODO Complete docs
   * TODO Add permission
   */

  /**
   * @swagger
   *
   * /user/{userId}/contacts:
   *   get:
   *     description: Get an User
   *     tags:
   *       - User
   *     security:
   *       - JWT: []
   *       - Requesting: []
   *     produces:
   *       - application/json
   *     parameters:
   *       - in: path
   *         name: userId
   *         required: true
   *         schema:
   *           type: string
   *
   *     responses:
   *       200:
   *         description: Success
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/schemas/Response'
   *             example:
   *               message: SUCCESS
   *               data:
   *                 _id: xxxxxxxxxxxxxxxxxxxxxxxx
   *                 att1: true
   *                 att2: text
   *
   *       400:
   *         $ref: '#/components/responses/BadRequest'
   *       401:
   *         $ref: '#/components/responses/Unauthorized'
   *       404:
   *         $ref: '#/components/responses/NotFound'
   *       Default:
   *         $ref: '#/components/responses/DefaultError'
   */
  async getContacts(req, res, next) {
    try {
      const { session } = req;
      const { pagination } = req.query;
      const { userId: user } = req.params;

      const result = await paginate(models.Contact, pagination, { session }, [
        addFields([{ prop: 'user', type: 'String' }]),
        { $match: { user } },
      ]);

      res.status(responses.codes.SUCCESS).json({
        message: responses.messages.SUCCESS,
        ...result,
      });
      next();
    } catch (error) {
      switch (error.message) {
        case 'SENTINEL_NOT_FOUND':
          res.status(responses.codes.BAD_REQUEST).send({ message: error.message });
          break;
        default:
      }
      next(error);
    }
  },

  /**
   * @swagger
   *
   * /user/{userId}/contacts:
   *   get:
   *     description: Get an User
   *     tags:
   *       - User
   *     security:
   *       - JWT: []
   *       - Requesting: []
   *     produces:
   *       - application/json
   *     parameters:
   *       - in: path
   *         name: userId
   *         required: true
   *         schema:
   *           type: string
   *
   *     responses:
   *       200:
   *         description: Success
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/schemas/Response'
   *             example:
   *               message: SUCCESS
   *               data:
   *                 _id: xxxxxxxxxxxxxxxxxxxxxxxx
   *                 att1: true
   *                 att2: text
   *
   *       400:
   *         $ref: '#/components/responses/BadRequest'
   *       401:
   *         $ref: '#/components/responses/Unauthorized'
   *       404:
   *         $ref: '#/components/responses/NotFound'
   *       Default:
   *         $ref: '#/components/responses/DefaultError'
   */
  async getPersons(req, res, next) {
    try {
      const { session } = req;
      const { pagination } = req.query;
      const { userId: user } = req.params;

      const result = await paginate(models.People, pagination, { session }, [
        addFields([{ prop: 'user', type: 'String' }]),
        { $match: { user } },
        addFields([{ prop: 'user', type: 'ObjectId' }]),
        ...makeLookup('users', 'user', '_id', 'userObj', true),
        { $unset: ['userObj.password'] },
      ]);

      res.status(responses.codes.SUCCESS).json({
        message: responses.messages.SUCCESS,
        ...result,
      });
      next();
    } catch (error) {
      switch (error.message) {
        case 'SENTINEL_NOT_FOUND':
          res.status(responses.codes.BAD_REQUEST).send({ message: error.message });
          break;
        default:
      }
      next(error);
    }
  },

  /**
   * @swagger
   *
   * /user/{userId}/contacts:
   *   get:
   *     description: Get an User
   *     tags:
   *       - User
   *     security:
   *       - JWT: []
   *       - Requesting: []
   *     produces:
   *       - application/json
   *     parameters:
   *       - in: path
   *         name: userId
   *         required: true
   *         schema:
   *           type: string
   *
   *     responses:
   *       200:
   *         description: Success
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/schemas/Response'
   *             example:
   *               message: SUCCESS
   *               data:
   *                 _id: xxxxxxxxxxxxxxxxxxxxxxxx
   *                 att1: true
   *                 att2: text
   *
   *       400:
   *         $ref: '#/components/responses/BadRequest'
   *       401:
   *         $ref: '#/components/responses/Unauthorized'
   *       404:
   *         $ref: '#/components/responses/NotFound'
   *       Default:
   *         $ref: '#/components/responses/DefaultError'
   */
  async getPerson(req, res, next) {
    try {
      const { session } = req;
      const { userId: user, personId } = req.params;

      const [person] = await models.People.aggregate([
        { $match: { _id: Types.ObjectId(personId), user: Types.ObjectId(user) } },
        ...makeLookup('users', 'user', '_id', 'userObj', true),
        { $unset: ['userObj.password'] },
      ]).option({ session });

      if (!person) throw new Error('SICK_DOCTOR_NOT_FOUND');

      res.status(responses.codes.SUCCESS).json({
        message: responses.messages.SUCCESS,
        data: person,
      });
      next();
    } catch (error) {
      switch (error.message) {
        case 'SENTINEL_NOT_FOUND':
          res.status(responses.codes.BAD_REQUEST).send({ message: error.message });
          break;
        default:
      }
      next(error);
    }
  },

  /**
   * TODO Implement
   * TODO Complete docs
   * TODO Add permission
   */

  /**
   * @swagger
   *
   * /user/{userId}:
   *   delete:
   *     description: Delete a User
   *     tags:
   *       - User
   *     security:
   *       - JWT: []
   *       - Requesting: []
   *     produces:
   *       - application/json
   *     parameters:
   *       - in: path
   *         name: userId
   *         required: true
   *         schema:
   *           type: string
   *
   *     responses:
   *       200:
   *         $ref: '#/components/responses/Success'
   *
   *       400:
   *         $ref: '#/components/responses/BadRequest'
   *       401:
   *         $ref: '#/components/responses/Unauthorized'
   *       404:
   *         $ref: '#/components/responses/NotFound'
   *       Default:
   *         $ref: '#/components/responses/DefaultError'
   */
  async delete(req, res, next) {
    try {
      const { session } = req;

      res.status(responses.codes.SUCCESS).json({
        message: responses.messages.SUCCESS,
        data: null,
      });
      next();
    } catch (error) {
      switch (error.message) {
        case 'SENTINEL_NOT_FOUND':
          res.status(responses.codes.BAD_REQUEST).send({ message: error.message });
          break;
        default:
      }
      next(error);
    }
  },

  /**
   * TODO Implement
   * TODO Complete docs
   * TODO Add permission
   */

  /**
   * @swagger
   *
   * /user/{userId}:
   *   put:
   *     description: Update user
   *     tags:
   *       - User
   *     security:
   *       - JWT: []
   *       - Requesting: []
   *     produces:
   *       - application/json
   *     parameters:
   *       - in: path
   *         name: userId
   *         required: true
   *         schema:
   *           type: string
   *     requestBody:
   *       required: true
   *       content:
   *         application/json:
   *           schema:
   *             type: object
   *             properties:
   *               shortName:
   *                 type: string
   *               mainEmail:
   *                 type: string
   *               mainPhone:
   *                 type: string
   *               mainProfile:
   *                 type: string
   *               password:
   *                 type: string
   *               newPassword:
   *                 type: string
   *               newPasswordConfirm:
   *                 type: string
   *
   *     responses:
   *       200:
   *         $ref: '#/components/responses/Success'
   *
   *       400:
   *         $ref: '#/components/responses/BadRequest'
   *       401:
   *         $ref: '#/components/responses/Unauthorized'
   *       404:
   *         $ref: '#/components/responses/NotFound'
   *       Default:
   *         $ref: '#/components/responses/DefaultError'
   */
  async update(req, res, next) {
    try {
      const { session } = req;
      const { userId } = req.params;
      const {
        shortName,
        mainEmail,
        mainPhone,
        mainProfile,
        anchor,
        password,
        newPassword,
        newPasswordConfirm,
      } = req.body;
      const { requesting } = req.headers;
      const { user } = res.locals;

      const userFound = await models.User.findById(userId, null, { session });
      const contacts = await models.Contact.find({ user: user._id }, null, { session });
      const profiles = await models.Profile.find({ user: user._id }, null, { session });
      if (!userFound) throw Error('USER_NOT_FOUND');

      if (mainProfile) {
        const profile = profiles.find(({ _id }) => _id.toString() === mainProfile);
        if (profile) {
          userFound.mainProfile = profile;
        } else {
          throw Error('USER_NOT_OWNER_PROFILE');
        }
      }
      let token;
      if (shortName && shortName !== userFound.shortName) {
        userFound.shortName = shortName;
        token = jwt.sign(
          { shortName: userFound.shortName, user: userFound._id },
          config.JWT_SECRET,
          {
            expiresIn: 60 * 60 * 24 * 7,
          }
        );
      }
      if (mainEmail) {
        const email = contacts.find(
          ({ _id }) => _id.toString() === mainEmail && userFound.mainEmail.toString() !== mainEmail
        );
        if (email) {
          userFound.mainEmail = email;
        } else {
          throw Error('USER_NOT_OWNER_CONTACT');
        }
      }
      if (mainPhone) {
        const phone = contacts.find(
          ({ _id }) => _id.toString() === mainPhone && userFound.mainPhone.toString() !== mainPhone
        );
        if (phone) {
          userFound.mainPhone = phone;
        } else {
          throw Error('USER_NOT_OWNER_CONTACT');
        }
      }
      if (newPassword && newPasswordConfirm) {
        if (newPassword.length < 8) throw new Error('INVALID_PASSWORD_LENGTH');
        // const passwordMatch = await bcrypt.compare(password, user.password);
        // if (!passwordMatch) throw new Error('INVALID_PASSWORD');

        if (newPassword !== newPasswordConfirm)
          throw new Error('DIFFERENT_PASSWORD_PASSWORDCONFIRM');

        userFound.password = await bcrypt.hash(newPassword, 12);
        token = jwt.sign(
          { shortName: userFound.shortName, user: userFound._id },
          config.JWT_SECRET,
          {
            expiresIn: 60 * 60 * 24 * 7,
          }
        );
      }
      await userFound.save({ session });

      res.status(responses.codes.SUCCESS).json({
        message: responses.messages.SUCCESS,
        data: token,
      });

      next();
    } catch (error) {
      switch (error.message) {
        case 'USER_NOT_FOUND':
          res.status(responses.codes.NOT_FOUND).json({
            message: responses.messages.USER_NOT_FOUND,
            data: null,
          });
          break;
        case 'SHORT_NAME_ALREADY_EXISTS':
          res.status(responses.codes.BAD_REQUEST).json({
            message: responses.messages.SHORT_NAME_ALREADY_EXISTS,
            data: null,
          });
          break;
        case 'EMAIL_ALREADY_EXISTS':
          res.status(responses.codes.BAD_REQUEST).json({
            message: responses.messages.EMAIL_ALREADY_EXISTS,
            data: null,
          });
          break;
        case 'PHONE_ALREADY_EXISTS':
          res.status(responses.codes.BAD_REQUEST).json({
            message: responses.messages.PHONE_ALREADY_EXISTS,
            data: null,
          });
          break;

        case 'INVALID_PASSWORD':
          res.status(responses.codes.BAD_REQUEST).json({
            message: error.message,
            data: null,
          });
          break;
        case 'INVALID_PASSWORD_LENGTH':
          res.status(responses.codes.BAD_REQUEST).json({
            message: error.message,
            data: null,
          });
          break;

        case 'DIFFERENT_PASSWORD_PASSWORDCONFIRM':
          res.status(responses.codes.BAD_REQUEST).json({
            message: error.message,
            data: null,
          });
          break;

        case 'USER_NOT_OWNER_CONTACT':
          res.status(responses.codes.UNAUTHORIZED).json({
            message: error.message,
            data: null,
          });
          break;
        case 'USER_NOT_OWNER_PROFILE':
          res.status(responses.codes.UNAUTHORIZED).json({
            message: error.message,
            data: null,
          });
          break;
        default:
      }
      next(error);
    }
  },

  /**
   * TODO Implement
   * TODO Complete docs
   * TODO Add permission
   */

  /**
   * @swagger
   *
   * /user/{userId}/people:
   *   put:
   *     description: Update a User
   *     tags:
   *       - User
   *     security:
   *       - JWT: []
   *       - Requesting: []
   *     produces:
   *       - application/json
   *     parameters:
   *       - in: path
   *         name: userId
   *         required: true
   *         schema:
   *           type: string
   *     requestBody:
   *       required: true
   *       content:
   *         application/json:
   *           schema:
   *             type: object
   *             properties:
   *               email:
   *                 type: string
   *               phone:
   *                 type: string
   *           examples:
   *             Phone:
   *               value:
   *                 phone: "1234567890"
   *             Cellphone:
   *               value:
   *                 cellphone: "1234567890"
   *             Email:
   *               value:
   *                 email: email@domain.com
   *
   *     responses:
   *       200:
   *         $ref: '#/components/responses/Success'
   *
   *       400:
   *         $ref: '#/components/responses/BadRequest'
   *       401:
   *         $ref: '#/components/responses/Unauthorized'
   *       404:
   *         $ref: '#/components/responses/NotFound'
   *       Default:
   *         $ref: '#/components/responses/DefaultError'
   */
  async updatePeople(req, res, next) {
    try {
      const { session } = req;
      const { personId, userId } = req.params;
      const { ...body } = req.body;

      console.log(body);
      console.log("that fuck")
      const person = await models.People.findOneAndUpdate(
        { _id: Types.ObjectId(personId), user: Types.ObjectId(userId) },
        body,
        { session }
      );

      if (!person) throw new Error('PERSON_NOT_FOUND');
      res.status(responses.codes.SUCCESS).json({
        message: responses.messages.SUCCESS,
        data: person,
      });
      next();
    } catch (error) {
      switch (error.message) {
        case 'PERSON_NOT_FOUND':
          res.status(responses.codes.BAD_REQUEST).send({ message: error.message });
          break;
        default:
      }
      next(error);
    }
  },
};
