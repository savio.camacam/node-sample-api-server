const { celebrate, Joi } = require('celebrate');
const { JoiPagination } = require('../../utils/JoiModels');

module.exports = {
  add() {
    return celebrate({
      body: Joi.object()
        .keys({
          phoneAddress: Joi.string().required(),
          emailAddress: Joi.string()
            .email()
            .required(),
          name: Joi.string().required(),
          shortName: Joi.string().required(),
          password: Joi.string()
            .min(8)
            .required(),
          passwordConfirm: Joi.string()
            .min(8)
            .required()
            .equal(Joi.ref('password')),
        })
        .required(),
    });
  },
  addSick() {
    return celebrate({
      body: Joi.object().keys({
        diseases: Joi.array().min(1),
      }),
    });
  },
  addDoctor() {
    return celebrate({
      body: Joi.object().keys({
        crm: Joi.string().required(),
        diseases: Joi.array().min(1),
      }),
    });
  },
  get() {
    return celebrate({
      query: Joi.object().keys({
        pagination: JoiPagination(1, 100),
      }),
    });
  },
  getProfiles() {
    return celebrate({
      query: Joi.object().keys({
        pagination: JoiPagination(1, 100),
      }),
    });
  },
  getContacts() {
    return celebrate({
      query: Joi.object().keys({
        pagination: JoiPagination(1, 100),
      }),
    });
  },
  getPersons() {
    return celebrate({
      query: Joi.object().keys({
        pagination: JoiPagination(1, 100),
      }),
    });
  },
  update() {
    return celebrate({
      body: Joi.object()
        .keys({
          shortName: Joi.string().allow('', null),
          mainEmail: Joi.string().allow('', null),
          mainPhone: Joi.string().allow('', null),
          mainProfile: Joi.string().allow('', null),
          anchor: Joi.string().allow('', null),
          password: Joi.string().allow('', null),
          newPassword: Joi.string().allow('', null),
          newPasswordConfirm: Joi.string().allow('', null),
        })
        .required(),
    });
  },
  updatePeople() {
    return celebrate({
      body: Joi.object()
        .keys({
          name: Joi.string().required(),
          birth: Joi.string().required(),
          gender: Joi.string().required(),
          schoolarDegree: Joi.string().required(),
        })
        .required(),
    });
  },
};
