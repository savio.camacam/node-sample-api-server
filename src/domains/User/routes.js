const router = require('express').Router();
const controller = require('./controller');
const validations = require('./validations');
const reportRoutes = require('./report/routes');
const auditRoutes = require('./audit/routes');
const auth = require('../../middlewares/authentication');

router.use('/audit', auditRoutes);
router.use('/report', reportRoutes);
router.post('/', validations.add(), controller.add);
router.post('/:userId/contact', validations.add(), auth.checkToken, controller.addContact);
router.post('/:userId/sick', validations.addSick(), auth.checkToken, controller.addSick);
router.post('/:userId/doctor', validations.addDoctor(), auth.checkToken, controller.addDoctor);

router.get('/', validations.get(), auth.checkToken, controller.get);
router.get('/:userId', auth.checkToken, controller.getById);
router.get('/:userId/profiles', validations.getProfiles(), auth.checkToken, controller.getProfiles);
router.get('/:userId/contacts', validations.getContacts(), auth.checkToken, controller.getContacts);
router.get('/:userId/persons', validations.getPersons(), auth.checkToken, controller.getPersons);
router.get('/:userId/person/:personId', auth.checkToken, controller.getPerson);

router.put('/:userId', validations.update(), auth.checkToken, controller.update);
router.put(
  '/:userId/person/:personId',
  validations.updatePeople(),
  auth.checkToken,
  controller.updatePeople
);

module.exports = router;
