/* eslint-disable no-unused-vars */
const { Types } = require('mongoose');
const models = require('../../../models');
const responses = require('../../../utils/responses');

module.exports = {

    /**
   * @swagger
   *
   * /sentinel/audit:
   *   get:
   *     description: Get all Sentinels
   *     tags:
   *       - Sentinel
   *     security:
   *       - JWT: []
   *       - Requesting: []
   *     produces:
   *       - application/json
   *     parameters:
   *       - $ref: '#/components/parameters/Pagination'
   *       - in: query
   *         name: checked
   *
   *         schema:
   *           type: string
   *       - in: query
   *         name: type
   *
   *         schema:
   *           type: string
   *       - in: query
   *         name: user
   *
   *         schema:
   *           type: string
   *       - in: query
   *         name: createdAt
   *
   *         schema:
   *           type: string
   *       - in: query
   *         name: updatedAt
   *
   *         schema:
   *           type: string
   *
   *     responses:
   *       200:
   *         description: Success
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/schemas/Response'
   *             example:
   *               message: SUCCESS
   *               metadata:
   *                 total: 1826
   *                 page: 1
   *                 limit: 5
   *               data:
   *                 - _id: xxxxxxxxxxxxxxxxxxxxxxxx
   *                   checked: true
   *                   address: 1234567890
   *                   createdAt: 2019-02-18T22:31:48.039Z
   *                   updatedAt: 2019-02-18T22:31:48.039Z
   *                   user: "@user"
   *       400:
   *         $ref: '#/components/responses/BadRequest'
   *       401:
   *         $ref: '#/components/responses/Unauthorized'
   *       404:
   *         $ref: '#/components/responses/NotFound'
   *       Default:
   *         $ref: '#/components/responses/DefaultError'
   */
  async get(req, res, next) {
    try {
      const { session } = req;

      res.status(responses.codes.SUCCESS).json({
        message: responses.messages.SUCCESS,
        data: null,
      });
      next();
    } catch (error) {
      res.status(responses.codes.BAD_REQUEST).json({
        message: error.message,
        data: null,
      });
      next(error);
    }
  },
};
