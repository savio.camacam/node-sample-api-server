const mongoose = require('mongoose');

const { Schema } = mongoose;

const TreatmentSchema = new Schema(
  {
    description: {
      type: Schema.Types.String,
      required: true,
    },
    sick: {
      type: Schema.Types.ObjectId,
      ref: 'Sick',
      required: true,
    },
    disease: {
      type: Schema.Types.ObjectId,
      ref: 'Disease',
    },
    treatmentDays: {
      type: Schema.Types.Number,
    },
    owner: {
      type: Schema.Types.ObjectId,
      ref: 'Doctor',
      required: true,
    },
  },
  {
    timestamps: {
      createdAt: 'createdAt',
    },
  }
);

module.exports = mongoose.model('Treatment', TreatmentSchema);
