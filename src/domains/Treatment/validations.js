const { celebrate, Joi } = require('celebrate');
const { JoiPagination } = require('../../utils/JoiModels');

module.exports = {
  add() {
    return celebrate({
      body: Joi.object()
        .keys({})
        .required(),
    });
  },
  get() {
    return celebrate({
      query: Joi.object().keys({
        pagination: JoiPagination(1, 100),
      }),
    });
  },
  update() {
    return celebrate({
      body: Joi.object()
        .keys({})
        .required(),
    });
  },
};
