const mongoose = require('mongoose');

const { Schema } = mongoose;

const ContactSchema = new Schema(
  {
    user: {
      type: Schema.Types.ObjectId,
      ref: 'User',
    },
    type: {
      type: Schema.Types.String,
      enum: ['phone', 'cellphone', 'email', 'app'],
      required: true,
    },
    address: { type: Schema.Types.String, unique: true, required: true },
    notificationTokens: [{ type: Schema.Types.String }],
    checked: {
      type: Schema.Types.Boolean,
      default: false,
    },
  },
  {
    timestamps: {
      createdAt: 'createdAt',
    },
  }
);

module.exports = mongoose.model('Contact', ContactSchema);
