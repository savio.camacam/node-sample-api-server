const mongoose = require('mongoose');
const Profile = require('../../models/profile.model');

const { Schema } = mongoose;

const AdminSchema = new Schema(
  {
    att: {
      type: Schema.Types.ObjectId,
      ref: 'Model',
    },
  },
  {
    timestamps: {
      createdAt: 'createdAt',
    },
  }
);

module.exports = Profile.discriminator('Admin', AdminSchema);
