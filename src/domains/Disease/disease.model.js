const mongoose = require('mongoose');

const { Schema } = mongoose;

const DiseaseSchema = new Schema(
  {
    owner: {
      type: Schema.Types.ObjectId,
      ref: 'Profile',
    },
    code: {
      type: Schema.Types.String,
      required: true,
    },
    name: {
      type: Schema.Types.String,
      required: true,
    },
    description: {
      type: Schema.Types.String,
      required: true,
    },
    frequency: { type: Schema.Types.Number },
    intensity: { type: Schema.Types.Number },
    seriesNumber: { type: Schema.Types.Number },
    intervalTime: { type: Schema.Types.Number },
    pulsesNumber: { type: Schema.Types.Number },
    applicationLocal: { type: Schema.Types.String },
  },
  {
    timestamps: {
      createdAt: 'createdAt',
    },
  }
);

module.exports = mongoose.model('Disease', DiseaseSchema);
