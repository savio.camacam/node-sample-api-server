const { celebrate, Joi } = require('celebrate');
const { JoiPagination } = require('../../utils/JoiModels');

module.exports = {
  add() {
    return celebrate({
      body: Joi.object()
        .keys({
          code: Joi.string().required(),
          name: Joi.string().required(),
          description: Joi.string().required(),
          frequency: Joi.number(),
          intensity: Joi.number(),
          seriesNumber: Joi.number(),
          intervalTime: Joi.number(),
          pulsesNumber: Joi.number(),
        })
        .required(),
    });
  },
  get() {
    return celebrate({
      query: Joi.object().keys({
        pagination: JoiPagination(1, 100),
      }),
    });
  },
  update() {
    return celebrate({
      body: Joi.object()
        .keys({
          code: Joi.string().required(),
          name: Joi.string().required(),
          description: Joi.string().required(),
          frequency: Joi.number(),
          intensity: Joi.number(),
          seriesNumber: Joi.number(),
          intervalTime: Joi.number(),
          pulsesNumber: Joi.number(),
        })
        .required(),
    });
  },
};
