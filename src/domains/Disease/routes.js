const router = require('express').Router();
const controller = require('./controller');
const validations = require('./validations');
const reportRoutes = require('./report/routes');
const auditRoutes = require('./audit/routes');
const auth = require('../../middlewares/authentication');

router.use('/audit', auditRoutes);
router.use('/report', reportRoutes);

router.post('/', validations.add(), auth.checkToken, controller.add);
router.post('/:diseaseId/sick', validations.add(), auth.checkToken, controller.addSick);
router.post('/:diseaseId/doctor', validations.add(), auth.checkToken, controller.addDoctor);

router.get('/', validations.get(), auth.checkToken, controller.get);
router.get('/:diseaseId', auth.checkToken, controller.getById);
router.get('/:diseaseId/sicks', auth.checkToken, controller.getSicks);
router.get('/:diseaseId/doctors', auth.checkToken, controller.getDoctors);

router.put('/:diseaseId', validations.update(), auth.checkToken, controller.update);
router.delete('/:diseaseId', auth.checkToken, controller.delete);

module.exports = router;
