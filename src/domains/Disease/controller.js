const models = require('../../models');
const responses = require('../../utils/responses');
// const { permissionGuard } = require('../../utils/permissions');
const { paginate, addFields } = require('../../utils/aggregations');

module.exports = {
  /**
   * TODO Implement
   * TODO Complete docs
   * TODO Add permission
   */

  /**
   * @swagger
   *
   * /disease:
   *   post:
   *     description: Update push notification reference
   *     tags:
   *       - Disease
   *     security:
   *       - JWT: []
   *     produces:
   *       - application/json
   *     requestBody:
   *       required: true
   *       content:
   *         application/json:
   *           schema:
   *             type: object
   *             properties:
   *               att1:
   *                 type: string
   *                 example: old
   *               att2:
   *                 type: string
   *                 example: new
   *
   *     responses:
   *       200:
   *         description: Success
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/schemas/Response'
   *             example:
   *               message: 'SUCCESS'
   *               data: xxxxxxxxxxxxxxxxxxxxxxxx
   *
   *       400:
   *         $ref: '#/components/responses/BadRequest'
   *       401:
   *         $ref: '#/components/responses/Unauthorized'
   *       404:
   *         $ref: '#/components/responses/NotFound'
   *       Default:
   *         $ref: '#/components/responses/DefaultError'
   */
  async add(req, res, next) {
    try {
      const { session } = req;

      const disease = await models.Disease.create([{ ...req.body }], { session });

      res.status(responses.codes.SUCCESS).json({
        message: responses.messages.SUCCESS,
        data: disease,
      });
      next();
    } catch (error) {
      switch (error.message) {
        case 'SENTINEL_NOT_FOUND':
          res.status(responses.codes.BAD_REQUEST).send({ message: error.message });
          break;
        default:
      }
      next(error);
    }
  },

  /**
   * @swagger
   *
   * /disease/{diseaseId}/sick:
   *   post:
   *     description: Update push notification reference
   *     tags:
   *       - Disease
   *     security:
   *       - JWT: []
   *     produces:
   *       - application/json
   *     requestBody:
   *       required: true
   *       content:
   *         application/json:
   *           schema:
   *             type: object
   *             properties:
   *               att1:
   *                 type: string
   *                 example: old
   *               att2:
   *                 type: string
   *                 example: new
   *
   *     responses:
   *       200:
   *         description: Success
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/schemas/Response'
   *             example:
   *               message: 'SUCCESS'
   *               data: xxxxxxxxxxxxxxxxxxxxxxxx
   *
   *       400:
   *         $ref: '#/components/responses/BadRequest'
   *       401:
   *         $ref: '#/components/responses/Unauthorized'
   *       404:
   *         $ref: '#/components/responses/NotFound'
   *       Default:
   *         $ref: '#/components/responses/DefaultError'
   */
  async addSick(req, res, next) {
    try {
      const { session } = req;
      const { diseaseId: disease } = req.params;
      const { sickId: sick } = req.body;

      const sickDisease = await models.SickDisease.create([{ disease, sick }], { session });

      res.status(responses.codes.SUCCESS).json({
        message: responses.messages.SUCCESS,
        data: sickDisease,
      });
      next();
    } catch (error) {
      switch (error.message) {
        case 'SENTINEL_NOT_FOUND':
          res.status(responses.codes.BAD_REQUEST).send({ message: error.message });
          break;
        default:
      }
      next(error);
    }
  },

  /**
   * @swagger
   *
   * /disease/{diseaseId}/doctor:
   *   post:
   *     description: Update push notification reference
   *     tags:
   *       - Disease
   *     security:
   *       - JWT: []
   *     produces:
   *       - application/json
   *     requestBody:
   *       required: true
   *       content:
   *         application/json:
   *           schema:
   *             type: object
   *             properties:
   *               att1:
   *                 type: string
   *                 example: old
   *               att2:
   *                 type: string
   *                 example: new
   *
   *     responses:
   *       200:
   *         description: Success
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/schemas/Response'
   *             example:
   *               message: 'SUCCESS'
   *               data: xxxxxxxxxxxxxxxxxxxxxxxx
   *
   *       400:
   *         $ref: '#/components/responses/BadRequest'
   *       401:
   *         $ref: '#/components/responses/Unauthorized'
   *       404:
   *         $ref: '#/components/responses/NotFound'
   *       Default:
   *         $ref: '#/components/responses/DefaultError'
   */
  async addDoctor(req, res, next) {
    try {
      const { session } = req;
      const { diseaseId: disease } = req.params;
      const { doctorId: doctor } = req.body;

      const doctorDisease = await models.DoctorDisease.create([{ disease, doctor }], { session });

      res.status(responses.codes.SUCCESS).json({
        message: responses.messages.SUCCESS,
        data: doctorDisease,
      });
      next();
    } catch (error) {
      switch (error.message) {
        case 'SENTINEL_NOT_FOUND':
          res.status(responses.codes.BAD_REQUEST).send({ message: error.message });
          break;
        default:
      }
      next(error);
    }
  },

  /**
   * TODO Implement
   * TODO Complete docs
   * TODO Add permission
   */

  /**
   * @swagger
   *
   * /disease:
   *   get:
   *     description: Get all Diseases
   *     tags:
   *       - Disease
   *     security:
   *       - JWT: []
   *       - Requesting: []
   *     produces:
   *       - application/json
   *     parameters:
   *       - $ref: '#/components/parameters/Pagination'
   *       - in: query
   *         name: checked
   *
   *         schema:
   *           type: string
   *       - in: query
   *         name: type
   *
   *         schema:
   *           type: string
   *       - in: query
   *         name: user
   *
   *         schema:
   *           type: string
   *       - in: query
   *         name: createdAt
   *
   *         schema:
   *           type: string
   *       - in: query
   *         name: updatedAt
   *
   *         schema:
   *           type: string
   *
   *     responses:
   *       200:
   *         description: Success
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/schemas/Response'
   *             example:
   *               message: SUCCESS
   *               metadata:
   *                 total: 1826
   *                 page: 1
   *                 limit: 5
   *               data:
   *                 - _id: xxxxxxxxxxxxxxxxxxxxxxxx
   *                   checked: true
   *                   address: 1234567890
   *                   createdAt: 2019-02-18T22:31:48.039Z
   *                   updatedAt: 2019-02-18T22:31:48.039Z
   *                   user: "@user"
   *       400:
   *         $ref: '#/components/responses/BadRequest'
   *       401:
   *         $ref: '#/components/responses/Unauthorized'
   *       404:
   *         $ref: '#/components/responses/NotFound'
   *       Default:
   *         $ref: '#/components/responses/DefaultError'
   */
  async get(req, res, next) {
    try {
      const { session } = req;
      const { pagination } = req.query;

      const result = await paginate(models.Disease, pagination, { session }, [{ $match: {} }]);

      res.status(responses.codes.SUCCESS).json({
        message: responses.messages.SUCCESS,
        ...result,
      });
      next();
    } catch (error) {
      switch (error.message) {
        case 'SENTINEL_NOT_FOUND':
          res.status(responses.codes.BAD_REQUEST).send({ message: error.message });
          break;
        default:
      }
      next(error);
    }
  },

  /**
   * TODO Implement
   * TODO Complete docs
   * TODO Add permission
   */

  /**
   * @swagger
   *
   * /disease/{diseaseId}:
   *   get:
   *     description: Get an Disease
   *     tags:
   *       - Disease
   *     security:
   *       - JWT: []
   *       - Requesting: []
   *     produces:
   *       - application/json
   *     parameters:
   *       - in: path
   *         name: diseaseId
   *         required: true
   *         schema:
   *           type: string
   *
   *     responses:
   *       200:
   *         description: Success
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/schemas/Response'
   *             example:
   *               message: SUCCESS
   *               data:
   *                 _id: xxxxxxxxxxxxxxxxxxxxxxxx
   *                 att1: true
   *                 att2: text
   *
   *       400:
   *         $ref: '#/components/responses/BadRequest'
   *       401:
   *         $ref: '#/components/responses/Unauthorized'
   *       404:
   *         $ref: '#/components/responses/NotFound'
   *       Default:
   *         $ref: '#/components/responses/DefaultError'
   */
  async getById(req, res, next) {
    try {
      const { session } = req;
      const { diseaseId } = req.params;

      const disease = await models.Disease.findById(diseaseId, null, { session });

      res.status(responses.codes.SUCCESS).json({
        message: responses.messages.SUCCESS,
        data: disease,
      });
      next();
    } catch (error) {
      switch (error.message) {
        case 'SENTINEL_NOT_FOUND':
          res.status(responses.codes.BAD_REQUEST).send({ message: error.message });
          break;
        default:
      }
      next(error);
    }
  },

  /**
   * TODO Implement
   * TODO Complete docs
   * TODO Add permission
   */

  /**
   * @swagger
   *
   * /disease/{diseaseId}/sicks:
   *   get:
   *     description: Get all Diseases
   *     tags:
   *       - Disease
   *     security:
   *       - JWT: []
   *       - Requesting: []
   *     produces:
   *       - application/json
   *     parameters:
   *       - $ref: '#/components/parameters/Pagination'
   *       - in: query
   *         name: checked
   *
   *         schema:
   *           type: string
   *       - in: query
   *         name: type
   *
   *         schema:
   *           type: string
   *       - in: query
   *         name: user
   *
   *         schema:
   *           type: string
   *       - in: query
   *         name: createdAt
   *
   *         schema:
   *           type: string
   *       - in: query
   *         name: updatedAt
   *
   *         schema:
   *           type: string
   *
   *     responses:
   *       200:
   *         description: Success
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/schemas/Response'
   *             example:
   *               message: SUCCESS
   *               metadata:
   *                 total: 1826
   *                 page: 1
   *                 limit: 5
   *               data:
   *                 - _id: xxxxxxxxxxxxxxxxxxxxxxxx
   *                   checked: true
   *                   address: 1234567890
   *                   createdAt: 2019-02-18T22:31:48.039Z
   *                   updatedAt: 2019-02-18T22:31:48.039Z
   *                   user: "@user"
   *       400:
   *         $ref: '#/components/responses/BadRequest'
   *       401:
   *         $ref: '#/components/responses/Unauthorized'
   *       404:
   *         $ref: '#/components/responses/NotFound'
   *       Default:
   *         $ref: '#/components/responses/DefaultError'
   */
  async getSicks(req, res, next) {
    try {
      const { session } = req;
      const { diseaseId } = req.params;
      const { pagination } = req.query;

      const result = await paginate(models.SickDisease, pagination, { session }, [
        addFields([{ prop: 'disease', type: 'String' }]),
        { $match: { disease: diseaseId } },
        addFields([{ prop: 'disease', type: 'ObjectId' }]),
      ]);

      res.status(responses.codes.SUCCESS).json({
        message: responses.messages.SUCCESS,
        ...result,
      });
    } catch (error) {
      switch (error.message) {
        case 'SENTINEL_NOT_FOUND':
          res.status(responses.codes.BAD_REQUEST).send({ message: error.message });
          break;
        default:
      }
      next(error);
    }
  },

  /**
   * TODO Implement
   * TODO Complete docs
   * TODO Add permission
   */

  /**
   * @swagger
   *
   * /disease/{diseaseId}/doctors:
   *   get:
   *     description: Get all Diseases
   *     tags:
   *       - Disease
   *     security:
   *       - JWT: []
   *       - Requesting: []
   *     produces:
   *       - application/json
   *     parameters:
   *       - $ref: '#/components/parameters/Pagination'
   *       - in: query
   *         name: checked
   *
   *         schema:
   *           type: string
   *       - in: query
   *         name: type
   *
   *         schema:
   *           type: string
   *       - in: query
   *         name: user
   *
   *         schema:
   *           type: string
   *       - in: query
   *         name: createdAt
   *
   *         schema:
   *           type: string
   *       - in: query
   *         name: updatedAt
   *
   *         schema:
   *           type: string
   *
   *     responses:
   *       200:
   *         description: Success
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/schemas/Response'
   *             example:
   *               message: SUCCESS
   *               metadata:
   *                 total: 1826
   *                 page: 1
   *                 limit: 5
   *               data:
   *                 - _id: xxxxxxxxxxxxxxxxxxxxxxxx
   *                   checked: true
   *                   address: 1234567890
   *                   createdAt: 2019-02-18T22:31:48.039Z
   *                   updatedAt: 2019-02-18T22:31:48.039Z
   *                   user: "@user"
   *       400:
   *         $ref: '#/components/responses/BadRequest'
   *       401:
   *         $ref: '#/components/responses/Unauthorized'
   *       404:
   *         $ref: '#/components/responses/NotFound'
   *       Default:
   *         $ref: '#/components/responses/DefaultError'
   */
  async getDoctors(req, res, next) {
    try {
      const { session } = req;
      const { diseaseId } = req.params;
      const { pagination } = req.query;

      const result = await paginate(models.DoctorDisease, pagination, { session }, [
        addFields([{ prop: 'disease', type: 'String' }]),
        { $match: { disease: diseaseId } },
        addFields([{ prop: 'disease', type: 'ObjectId' }]),
      ]);

      res.status(responses.codes.SUCCESS).json({
        message: responses.messages.SUCCESS,
        ...result,
      });
    } catch (error) {
      switch (error.message) {
        case 'SENTINEL_NOT_FOUND':
          res.status(responses.codes.BAD_REQUEST).send({ message: error.message });
          break;
        default:
      }
      next(error);
    }
  },

  /**
   * TODO Implement
   * TODO Complete docs
   * TODO Add permission
   */

  /**
   * @swagger
   *
   * /disease/{diseaseId}:
   *   delete:
   *     description: Delete a Disease
   *     tags:
   *       - Disease
   *     security:
   *       - JWT: []
   *       - Requesting: []
   *     produces:
   *       - application/json
   *     parameters:
   *       - in: path
   *         name: diseaseId
   *         required: true
   *         schema:
   *           type: string
   *
   *     responses:
   *       200:
   *         $ref: '#/components/responses/Success'
   *
   *       400:
   *         $ref: '#/components/responses/BadRequest'
   *       401:
   *         $ref: '#/components/responses/Unauthorized'
   *       404:
   *         $ref: '#/components/responses/NotFound'
   *       Default:
   *         $ref: '#/components/responses/DefaultError'
   */
  async delete(req, res, next) {
    try {
      const { session } = req;
      const { diseaseId } = req.params;

      const disease = await models.Disease.findByIdAndDelete(diseaseId, { session });
      if (!disease) throw new Error('DISEASE_NOT_FOUND');
      await models.SickDisease.deleteMany({ disease: diseaseId }).session(session);
      await models.DoctorDisease.deleteMany({ disease: diseaseId }).session(session);
      await models.TreatmentDisease.deleteMany({ disease: diseaseId }).session(session);

      res.status(responses.codes.SUCCESS).json({
        message: responses.messages.SUCCESS,
        data: disease,
      });
      next();
    } catch (error) {
      switch (error.message) {
        case 'DISEASE_NOT_FOUND':
          res.status(responses.codes.BAD_REQUEST).send({ message: error.message });
          break;
        default:
      }
      next(error);
    }
  },

  /**
   * TODO Implement
   * TODO Complete docs
   * TODO Add permission
   */

  /**
   * @swagger
   *
   * /disease/{diseaseId}:
   *   put:
   *     description: Update a Disease
   *     tags:
   *       - Disease
   *     security:
   *       - JWT: []
   *       - Requesting: []
   *     produces:
   *       - application/json
   *     parameters:
   *       - in: path
   *         name: diseaseId
   *         required: true
   *         schema:
   *           type: string
   *     requestBody:
   *       required: true
   *       content:
   *         application/json:
   *           schema:
   *             type: object
   *             properties:
   *               email:
   *                 type: string
   *               phone:
   *                 type: string
   *           examples:
   *             Phone:
   *               value:
   *                 phone: "1234567890"
   *             Cellphone:
   *               value:
   *                 cellphone: "1234567890"
   *             Email:
   *               value:
   *                 email: email@domain.com
   *
   *     responses:
   *       200:
   *         $ref: '#/components/responses/Success'
   *
   *       400:
   *         $ref: '#/components/responses/BadRequest'
   *       401:
   *         $ref: '#/components/responses/Unauthorized'
   *       404:
   *         $ref: '#/components/responses/NotFound'
   *       Default:
   *         $ref: '#/components/responses/DefaultError'
   */
  async update(req, res, next) {
    try {
      const { session } = req;
      const { diseaseId } = req.params;
      const { ...body } = req.body;

      const disease = await models.Disease.findByIdAndUpdate(diseaseId, body, { session });

      if (!disease) throw new Error('DISEASE_NOT_FOUND');
      res.status(responses.codes.SUCCESS).json({
        message: responses.messages.SUCCESS,
        data: disease,
      });
      next();
    } catch (error) {
      switch (error.message) {
        case 'DISEASE_NOT_FOUND':
          res.status(responses.codes.BAD_REQUEST).send({ message: error.message });
          break;
        default:
      }
      next(error);
    }
  },
};
