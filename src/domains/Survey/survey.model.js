const mongoose = require('mongoose');

const { Schema } = mongoose;

const SurveySchema = new Schema(
  {
    owner: {
      type: Schema.Types.ObjectId,
      ref: 'Profile',
      required: true,
    },
  },
  {
    timestamps: {
      createdAt: 'createdAt',
    },
  }
);

module.exports = mongoose.model('Survey', SurveySchema);
