const mongoose = require('mongoose');

const { Schema } = mongoose;

const QuestionSchema = new Schema(
  {
    owner: {
      type: Schema.Types.ObjectId,
      ref: 'Profile',
    },
    question: {
      type: Schema.Types.String,
      required: true,
    },
    questionType: {
      type: Schema.Types.String,
      enum: ['multipleOption', 'linearScale', 'selectionBox', 'paragraph', 'shortAnswer'],
      required: true,
    },
    initial: {
      type: Schema.Types.Number,
      min: 0,
      max: 1,
    },
    initialMarker: {
      type: Schema.Types.String,
    },
    final: {
      type: Schema.Types.Number,
      min: 2,
      max: 10,
    },
    finalMarker: {
      type: Schema.Types.String,
    },
    lines: [
      {
        value: { type: Schema.Types.String },
      },
    ],
    columns: [
      {
        value: { type: Schema.Types.String },
      },
    ],
  },
  {
    timestamps: {
      createdAt: 'createdAt',
    },
  }
);
// ModelSchema.index({ createdAt: 1 }, { expireAfterSeconds: 600 });

module.exports = mongoose.model('Question', QuestionSchema);
