const router = require('express').Router();
const controller = require('./controller');
const auth = require('../../../middlewares/authentication');
const validations = require('./validations');

router.get('/', validations.get(), auth.checkToken, controller.get);

module.exports = router;
