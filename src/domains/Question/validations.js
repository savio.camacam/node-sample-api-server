const { celebrate, Joi } = require('celebrate');
const { JoiPagination, JoiQuestionTypes } = require('../../utils/JoiModels');

module.exports = {
  add() {
    return celebrate({
      body: Joi.object()
        .keys({
          question: Joi.string().required(),
          questionType: Joi.string().regex(JoiQuestionTypes),
          lines: Joi.array(),
          columns: Joi.array(),
          initial: Joi.number(),
          initialMarker: Joi.string(),
          final: Joi.number(),
          finalMarker: Joi.string(),
        })
        .required(),
    });
  },
  get() {
    return celebrate({
      query: Joi.object().keys({
        pagination: JoiPagination(1, 100),
      }),
    });
  },
  update() {
    return celebrate({
      body: Joi.object()
        .keys({
          question: Joi.string().required(),
          questionType: Joi.string().regex(JoiQuestionTypes),
          lines: Joi.array(),
          columns: Joi.array(),
          initial: Joi.number(),
          initialMarker: Joi.string(),
          final: Joi.number(),
          finalMarker: Joi.string(),
        })
        .required(),
    });
  },
};
