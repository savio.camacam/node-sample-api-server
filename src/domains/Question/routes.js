const router = require('express').Router();
const controller = require('./controller');
const validations = require('./validations');
const reportRoutes = require('./report/routes');
const auditRoutes = require('./audit/routes');
const auth = require('../../middlewares/authentication');

router.use('/audit', auditRoutes);
router.use('/report', reportRoutes);

router.post('/', validations.add(), auth.checkToken, controller.add);
router.get('/', validations.get(), auth.checkToken, controller.get);
router.get('/:questionId', auth.checkToken, controller.getById);
router.put('/', validations.update(), auth.checkToken, controller.update);
router.delete('/:questionId', auth.checkToken, controller.delete);

module.exports = router;
