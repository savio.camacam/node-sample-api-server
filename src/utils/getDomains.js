const { readdirSync } = require('fs');
const path = require('path');

const getDirectories = source =>
  readdirSync(`./${source}`, { withFileTypes: true })
    .filter(dirent => dirent.isDirectory())
    .map(dirent => [
      path.join(process.cwd(), source, dirent.name),
      ...getDirectories(`${source}/${dirent.name}`),
    ])
    .reduce((a, b) => [...a, ...b], []);

module.exports = {
  getDomains() {
    return getDirectories('src/domains');
  },
};
