/**
 * This class aims to simplify the use of permissions.
 *
 * @class
 */
class BasePermission {
  get read() {
    return this.createMiddle('read');
  }

  get write() {
    return this.createMiddle('write');
  }

  get delete() {
    return this.createMiddle('delete');
  }

  createMiddle(operation = 'read') {
    this._middle = async function middle(_req, _res, next) {
      try {
        // eslint-disable-next-line no-console
        console.log("Permission don't impremented!");

        next();
      } catch (error) {
        // eslint-disable-next-line no-console
        console.log('Error in permission guard! for operation', operation);
        next(error);
      }
    };

    return this._middle;
  }
}
module.exports = { BasePermission };
