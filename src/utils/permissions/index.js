/* eslint-disable import/no-dynamic-require */
/* eslint-disable global-require */
/* eslint-disable no-await-in-loop */
/* eslint-disable no-restricted-syntax */

const path = require('path');
const fs = require('fs');
const { Types } = require('mongoose');
const models = require('../../models');
const responses = require('../responses');
const { getDomains } = require('../../utils/getDomains');
const { logAction } = require('../audit');
const { BasePermission } = require('./permission-middle');

const policies = [];

for (const domain of getDomains()) {
  const p = path.join(domain, 'policies.js');
  if (fs.existsSync(p)) {
    policies.push(...require(p));
  }
}

/**
 * Check permissions.
 * @function permissionGuard
 *
 * @param {Request} req
 * @param {Response} res
 * @param {string} resourceType
 * @param {string} permissionName
 * @param {string} action
 * @param {object} resource
 * @param {string} resource.profile
 * @param {boolean} resource.audition
 * @param {string} resource.
 */
async function permissionGuard(req, res, resourceType, permissionName, action, resource) {
  try {
    const { user } = res.locals;

    const userPermissions = await models.User.aggregate([
      {
        $match: {
          shortName: user.shortName,
        },
      },
      {
        $lookup: {
          from: 'profiles',
          localField: 'profiles',
          foreignField: '_id',
          as: 'profiles',
        },
      },
      {
        $unwind: {
          path: '$profiles',
        },
      },
      {
        $match: {
          'profiles._id': Types.ObjectId(resource.profile),
        },
      },
      {
        $unwind: {
          path: '$profiles.permissions',
        },
      },
      {
        $project: {
          profile: '$profiles',
          profilePermissionLevel: '$profiles.permissions.action',
          permissionDefinition: '$profiles.permissions.permissionDefinition',
        },
      },
      {
        $lookup: {
          from: 'permissiondefinitions',
          localField: 'permissionDefinition',
          foreignField: '_id',
          as: 'permissionDefinition',
        },
      },
      {
        $match: {
          $and: [
            {
              'permissionDefinition.resourceType': resourceType,
            },
            {
              'permissionDefinition.permissionName': permissionName,
            },
          ],
        },
      },
    ]);

    let authorized = false;

    for (const permission of userPermissions) {
      const possibleActions = permission.permissionDefinition[0].actions;

      const profileActionIndex = possibleActions.indexOf(permission.profilePermissionLevel);
      const requiredActionIndex = possibleActions.indexOf(action);

      if (
        profileActionIndex !== -1 &&
        requiredActionIndex !== -1 &&
        profileActionIndex >= requiredActionIndex
      ) {
        const policy = policies.find(
          p =>
            p.profileType === permission.profile.profileType &&
            p.resourceType === resourceType &&
            p.permissionName === permissionName &&
            p.action === action
        );

        if (policy) {
          const result = await policy.checker(permission.profile, resource);
          if (result) {
            authorized = true;
            logAction({
              user,
              profile: permission.profile,
              resourceType,
              permissionName,
              action,
              resource,
              authorized,
            });
            break;
          }
        }
      }
    }

    if (authorized) return true;

    logAction({
      user,
      profile: resource.profile,
      resourceType,
      permissionName,
      action,
      resource,
      authorized,
    });
    throw new Error(responses.messages.UNAUTHORIZED);
  } catch (error) {
    throw new Error(responses.messages.UNAUTHORIZED);
  }
}

module.exports = { BasePermission, permissionGuard };
