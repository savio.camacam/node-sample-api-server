const JoiPagination = require('./pagination');

module.exports = {
  JoiPagination,
  JoiStatus: /denied|waiting|closed|accepted|done|progress/,
  JoiQuestionTypes: /multipleOption|linearScale|selectionBox|paragraph|shortAnswer/,
};
