const { Joi } = require('celebrate');

module.exports = (min, max) =>
  Joi.object()
    .keys({
      page: Joi.number()
        .min(1)
        .default(1),
      limit: Joi.number()
        .min(min || 1)
        .max(max || 100)
        .default(5),
    })
    .default({ page: 1, limit: 5 });
