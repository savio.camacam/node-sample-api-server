const models = require('../../models');

async function logAction(info) {
  new models.AuditionLog(info).save();
}

module.exports = { logAction };
