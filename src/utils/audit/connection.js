const mongoose = require('mongoose');
const config = require('../../../config/database.json');

const uristring = process.env.MONGODB_AUDITION_URI || config.MONGODB_AUDITION_URI;

const connection = mongoose.createConnection(uristring, {
  useNewUrlParser: true,
});

module.exports = connection;
