const mongoose = require('mongoose');
const config = require('../../config/database.json');

module.exports = {
  async connectToDatabase() {
    const uristring = process.env.MONGODB_URI || config.MONGODB_URI;
    return mongoose.connect(uristring, {
      useNewUrlParser: true,
      useCreateIndex: true,
      useFindAndModify: false,
      useUnifiedTopology: true,
      poolSize: 300,
    });
  },
};
