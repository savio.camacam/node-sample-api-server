const models = require('../models');

/**
 * This function returns an array responsible for paging aggregate's response;
 * @function paginationData
 *
 * @param {object} pagination
 * @param {number} pagination.page
 * @param {number} pagination.limit
 *
 * @returns {array} Array of pipelines
 */
function paginationData(pagination) {
  return [
    {
      $facet: {
        metadata: [
          {
            $count: 'total',
          },
          {
            $addFields: {
              page: pagination.page,
              limit: pagination.limit,
            },
          },
        ],
        data: [
          {
            $skip: (pagination.page - 1) * pagination.limit,
          },
          {
            $limit: pagination.limit,
          },
        ],
      },
    },
    {
      $unwind: {
        path: '$metadata',
        preserveNullAndEmptyArrays: true,
      },
    },
  ];
}

/**
 * @function emptyPagination
 *
 * @param {object} pagination
 * @param {number} pagination.page
 * @param {number} pagination.limit
 */
function emptyPagination(pagination) {
  return { metadata: { total: 0, ...pagination }, data: [] };
}

/**
 * @function aggregation
 *
 * @param {*} model
 * @param {*} opts
 * @param {*} pipelines
 */
async function aggregation(model, opts = {}, pipelines = []) {
  return model.aggregate(pipelines).option(opts);
}

/**
 * @function paginate
 *
 * @param {*} model
 * @param {*} pagination
 * @param {*} opts
 * @param {*} aggregate
 */
async function paginate(model, pagination, opts = {}, aggregate = [], all = false) {
  if (all) {
    return { data: await model.aggregate(aggregate).option(opts) };
  }

  const [result] = await model
    .aggregate([...aggregate, ...paginationData(pagination)])
    .option(opts);

  return { ...emptyPagination(pagination), ...result };
}

// async function newPaginate(model, pagination, opts = {}, aggregate = [], all = false) {
//   if (all) {
//     return { data: await aggregation(model, opts, aggregate) };
//   }

//   const [result] = await aggregation(model, opts, [...aggregate, ...paginationData(pagination)]);

//   return { ...emptyPagination(pagination), ...result };
// }

/**
 * @function makeLookup
 *
 * @param {string} from
 * @param {string} localField
 * @param {string} foreignField
 * @param {string=} _as
 * @param {boolean=} unwind
 *
 * @returns {[{$lookup}] | [{$lookup}, { $unwind}]}
 */
function makeLookup(from, localField, foreignField, _as, unwind = null, preserve = true) {
  const lookUp = { $lookup: { from, localField, foreignField, as: _as || localField } };
  const unWind = {
    $unwind: { path: `$${_as || localField}`, preserveNullAndEmptyArrays: preserve },
  };

  return !unwind ? [lookUp] : [lookUp, unWind];
}

/**
 * @typedef Key
 *
 * @property {string|Key} prop
 * @property {string} type
 * @property {string=} name
 */

/**
 * @function _nestedFields
 *
 * @param {object[]} keys - Array of type `Key`.
 * @param {string|Key[]} keys[].prop - An prop name or `array` of `Keys`.
 * @param {string=} keys[].type - An type to cast prop.
 * @param {string=} keys[].name - An custom name to receive prop.
 */
function _nestedFields(keys) {
  try {
    return keys
      .map(key => {
        if (key.prop instanceof Array) {
          return { [key.name]: _nestedFields(key.prop) };
        }
        if (key.prop instanceof Object) {
          return { [key.name]: key.prop };
        }

        return {
          [key.name || key.prop]: key.type
            ? { [`$to${key.type}`]: `$${key.prop}` }
            : `$${key.prop}`,
        };
      })
      .reduce((prev, next) => ({ ...prev, ...next }), {});
  } catch (error) {
    return {};
  }
}

/**
 * @function addFields
 *
 * @param {object[]} keys - Array of type `Key`.
 * @param {string|Key[]} keys[].prop - An prop name or `array` of `Keys`.
 * @param {string} keys[].type - An type to cast prop.
 * @param {string=} keys[].name - An custom name to receive prop.
 */
function addFields(keys) {
  return { $addFields: _nestedFields(keys) };
}

async function populateUser(match, session) {
  const data = await models.User.aggregate([
    match,
    ...makeLookup('profiles', 'profiles', '_id', 'profilesObj'),
    ...makeLookup('contacts', 'mainEmail', '_id', 'mainEmailObj', true),
    ...makeLookup('contacts', 'mainPhone', '_id', 'mainPhoneObj', true),
    ...makeLookup('contacts', 'push', '_id', 'pushObj', true),
    ...makeLookup('contacts', 'contacts', '_id', null),
    ...makeLookup('peoples', 'people', '_id', 'peopleObj', true),
    {
      $project: {
        _id: 1,
        shortName: 1,
        mainEmail: 1,
        mainPhone: 1,
        mainProfile: 1,
        profiles: 1,
        contacts: 1,
        people: 1,
        profilesObj: 1,
        peopleObj: {
          _id: 1,
          name: 1,
        },
        mainEmailObj: {
          _id: 1,
          checked: 1,
          address: 1,
        },
        mainPhoneObj: {
          _id: 1,
          checked: 1,
          address: 1,
        },
        pushObj: {
          _id: 1,
          checked: 1,
          address: 1,
          notificationTokens: 1,
        },
      },
    },
  ]).option({ session });
  const user = data[0];

  if (!user) return null;

  let profilesComunity = user.profilesObj.filter(p => p.profileType === 'ProfileComunity');
  let profilesCounty = user.profilesObj.filter(p => p.profileType === 'ProfileCounty');
  let profilesParent = user.profilesObj.filter(p => p.profileType === 'ProfileParent');
  let profilesSchool = user.profilesObj.filter(p => p.profileType === 'ProfileSchool');
  let profilesProfessor = user.profilesObj.filter(p => p.profileType === 'ProfileProfessor');
  const profilesAdmin = user.profilesObj.filter(p => p.profileType === 'ProfileAdmin');

  const profilesObj = [];

  const baseAggregate = async (_id, field) =>
    (
      await models.Profile.aggregate([
        { $match: { _id } },
        ...makeLookup('links', field, '_id', field, true),
        ...makeLookup('profiles', `${field}.requested`, '_id', null, true),
        ...makeLookup(
          'institutions',
          `${field}.requested.institution`,
          '_id',
          `${field}.institution`,
          true
        ),
      ])
    )[0];

  if (profilesAdmin.length) {
    profilesObj.push(
      ...profilesAdmin.map(p => ({
        _id: p._id,
        profileType: p.profileType,
        showType: p.showType,
        avatar: p.avatar,
        institutionName: '',
        institutionProfileId: '',
        permissions: p.permissions,
      }))
    );
  }

  if (profilesCounty.length) {
    profilesCounty = await Promise.all(profilesCounty.map(p => baseAggregate(p._id, 'county')));

    profilesObj.push(
      ...profilesCounty.map(p => ({
        _id: p._id,
        profileType: p.profileType,
        active: p.active,
        showType: p.showType,
        avatar: p.avatar,
        status: p.county.status,
        institutionAvatar: p.county.requested.avatar,
        institutionName: p.county.institution.name,
        institutionProfileId: p.county.requested._id,
        permissions: p.permissions,
      }))
    );
  }

  if (profilesSchool.length) {
    profilesSchool = await Promise.all(profilesSchool.map(p => baseAggregate(p._id, 'school')));

    profilesObj.push(
      ...profilesSchool
        .filter(p => p && p.school.institution) // FIXME This filter not is allowed
        .map(p => ({
          _id: p._id,
          profileType: p.profileType,
          active: p.active,
          showType: p.showType,
          avatar: p.avatar,
          status: p.school.status,
          institutionAvatar: p.school.requested.avatar,
          institutionName: p.school.institution.name,
          institutionProfileId: p.school.requested._id,
          permissions: p.permissions,
        }))
    );
  }
  // console.log(profilesSchool);

  if (profilesProfessor.length) {
    profilesProfessor = await Promise.all(
      profilesProfessor.map(p => baseAggregate(p._id, 'school'))
    );

    profilesObj.push(
      ...profilesProfessor
        .filter(p => p && p.school.institution)
        .map(p => ({
          _id: p._id,
          profileType: p.profileType,
          active: p.active,
          showType: p.showType,
          avatar: p.avatar,
          status: p.school.status,
          institutionAvatar: p.school.requested.avatar,
          institutionName: p.school.institution.name,
          institutionProfileId: p.school.requested._id,
          permissions: p.permissions,
        }))
    );
  }

  if (profilesComunity.length) {
    profilesComunity = await Promise.all(profilesComunity.map(p => baseAggregate(p._id, 'county')));

    profilesObj.push(
      ...profilesComunity.map(p => ({
        _id: p._id,
        profileType: p.profileType,
        active: p.active,
        showType: p.showType,
        avatar: p.avatar,
        status: p.county.status,
        institutionName: p.county.institution.name,
        institutionProfileId: p.county.requested._id,
        permissions: p.permissions,
      }))
    );
  }
  if (profilesParent.length) {
    profilesParent = await Promise.all(profilesParent.map(p => baseAggregate(p._id, 'county')));

    profilesObj.push(
      ...profilesParent.map(p => ({
        _id: p._id,
        profileType: p.profileType,
        active: p.active,
        showType: p.showType,
        avatar: p.avatar,
        status: p.county.status,
        institutionName: p.county.institution.name,
        institutionProfileId: p.county.requested._id,
        permissions: p.permissions,
      }))
    );
  }

  user.profilesObj = profilesObj;

  return user;
}

async function populateUserOneProfile(match, profileId) {
  const data = await models.User.aggregate([
    match,
    ...makeLookup('contacts', 'mainEmail', '_id', 'mainEmailObj', true),
    ...makeLookup('contacts', 'mainPhone', '_id', 'mainPhoneObj', true),
    ...makeLookup('contacts', 'push', '_id', 'pushObj', true),
    ...makeLookup('contacts', 'contacts', '_id', null),
    ...makeLookup('peoples', 'people', '_id', 'peopleObj', true),
    {
      $project: {
        _id: 1,
        shortName: 1,
        mainEmail: 1,
        mainPhone: 1,
        mainProfile: 1,
        profiles: 1,
        contacts: 1,
        people: 1,
        peopleObj: {
          _id: 1,
          name: 1,
        },
        mainEmailObj: {
          _id: 1,
          checked: 1,
          address: 1,
        },
        mainPhoneObj: {
          _id: 1,
          checked: 1,
          address: 1,
        },
        pushObj: {
          _id: 1,
          checked: 1,
          address: 1,
          notificationTokens: 1,
        },
      },
    },
  ]);
  const user = data[0];

  if (!user) return null;
  let profileFound = user.profiles.find(p => p.toString() === profileId);
  if (!profileFound) return null;

  profileFound = await models.Profile.findById(profileId);

  const baseAggregate = async (_id, field) =>
    (
      await models.Profile.aggregate([
        { $match: { _id } },
        ...makeLookup('links', field, '_id', field, true),
        ...makeLookup('profiles', `${field}.requested`, '_id', null, true),
        ...makeLookup(
          'institutions',
          `${field}.requested.institution`,
          '_id',
          `${field}.institution`,
          true
        ),
      ])
    )[0];

  if (profileFound && profileFound.profileType === 'ProfileAdmin') {
    const profile = {
      _id: profileFound._id,
      profileType: profileFound.profileType,
      active: true,
      showType: profileFound.showType,
      avatar: profileFound.avatar,
      status: 'accepted',
      institutionName: '',
      institutionProfileId: '',
      permissions: profileFound.permissions,
    };
    profileFound = profile;
  }

  if (profileFound && profileFound.profileType === 'ProfileCounty') {
    let profile = await baseAggregate(profileFound._id, 'county');
    profile = {
      _id: profile._id,
      profileType: profile.profileType,
      active: profile.active,
      showType: profile.showType,
      avatar: profile.avatar,
      status: profile.county.status,
      institutionName: profile.county.institution.name,
      institutionProfileId: profile.county.requested._id,
      permissions: profile.permissions,
    };
    profileFound = profile;
  }

  if (profileFound && profileFound.profileType === 'ProfileSchool') {
    let profile = await baseAggregate(profileFound._id, 'school');
    profile = {
      _id: profile._id,
      profileType: profile.profileType,
      active: profile.active,
      showType: profile.showType,
      avatar: profile.avatar,
      status: profile.school.status,
      institutionName: profile.school.institution.name,
      institutionProfileId: profile.school.requested._id,
      permissions: profile.permissions,
    };
    profileFound = profile;
  }

  if (profileFound && profileFound.profileType === 'ProfileProfessor') {
    let profile = await baseAggregate(profileFound._id, 'school');
    profile = {
      _id: profile._id,
      profileType: profile.profileType,
      active: profile.active,
      showType: profile.showType,
      avatar: profile.avatar,
      status: profile.school.status,
      institutionName: profile.school.institution.name,
      institutionProfileId: profile.school.requested._id,
      permissions: profile.permissions,
    };
    profileFound = profile;
  }

  if (profileFound && profileFound.profileType === 'ProfileComunity') {
    let profile = await baseAggregate(profileFound._id, 'county');

    profile = {
      _id: profile._id,
      profileType: profile.profileType,
      active: profile.active,
      showType: profile.showType,
      avatar: profile.avatar,
      status: profile.county.status,
      institutionName: profile.county.institution.name,
      institutionProfileId: profile.county.requested._id,
      permissions: profile.permissions,
    };
    profileFound = profile;
  }
  user.mainProfileObj = profileFound;
  user.profilesObj = [profileFound];
  return user;
}

function countAttachsTypes() {
  return [
    {
      $unwind: {
        path: '$attachments',
        preserveNullAndEmptyArrays: true,
      },
    },
    {
      $group: {
        _id: '$_id',
        attachments: {
          $push: '$attachments',
        },
        youTubeAttachs: {
          $sum: {
            $cond: {
              if: {
                $eq: ['$attachments.attachType', 'youtube'],
              },
              then: 1,
              else: 0,
            },
          },
        },
        siteAttachs: {
          $sum: {
            $cond: {
              if: {
                $eq: ['$attachments.attachType', 'site'],
              },
              then: 1,
              else: 0,
            },
          },
        },
        imageAttachs: {
          $sum: {
            $cond: {
              if: {
                $eq: ['$attachments.attachType', 'image'],
              },
              then: 1,
              else: 0,
            },
          },
        },
        pdfAttachs: {
          $sum: {
            $cond: {
              if: {
                $eq: ['$attachments.attachType', 'pdf'],
              },
              then: 1,
              else: 0,
            },
          },
        },
        data: {
          $first: '$$ROOT',
        },
      },
    },
    {
      $replaceRoot: {
        newRoot: {
          $mergeObjects: ['$$ROOT.data', '$$ROOT'],
        },
      },
    },
  ];
}

module.exports = {
  populateUser,
  populateUserOneProfile,
  paginationData,
  paginate,
  aggregation,
  makeLookup,
  addFields,
  countAttachsTypes,
};
