const express = require('express');
const morgan = require('morgan');
const cors = require('cors');
const swaggerUi = require('swagger-ui-express');
const swaggerJSDocs = require('swagger-jsdoc');

const app = express();
const database = require('./utils/database');
const routes = require('./routes');
const responses = require('./utils/responses');
const config = require('../config/server.json');

const customCss = require('../docs/css');

database
  .connectToDatabase()
  .then(() => console.log('connection succesful'))
  .catch(err => console.error(err));

const apiRouter = express.Router();

apiRouter.use('/v1', routes);

apiRouter.use((req, res) => {
  if (!res.headersSent) {
    res
      .status(responses.codes.NOT_FOUND)
      .json({ message: responses.messages.NOT_FOUND, data: null });
  }
});

app.use(
  cors({
    credentials: true,
    origin: '*',
  })
);

const PORT = process.env.PORT || config.PORT;
let host = 'falconer.herokuapp.com';
let scheme = ['https'];
let servers = [
  {
    url: 'https://falconer.herokuapp.com/api/v3',
  },
];

if (process.env.NODE_ENV !== 'production') {
  app.use(morgan('tiny'));
  host = `localhost:${PORT}`;
  scheme = ['http'];
  servers = [
    {
      url: 'http://localhost:3000/api/v1',
    },
    ...servers,
  ];
}

app.use('/api', apiRouter);

const swaggerDefinition = {
  openapi: '3.0.0',
  info: {
    description: 'Documentação do projeto Falconer v1',
    title: 'Falconer',
    version: '1.0.0',
  },
  host,
  basePath: '/api/v1',
  produces: ['application/json'],
  servers,
  schemes: scheme,
};

const options = {
  swaggerDefinition,
  apis: [
    'docs/tags.js',
    'docs/security.js',
    'docs/schemas.js',
    'docs/parameters.js',
    'docs/responses.js',
    'src/domains/**/responses.js',
    'src/domains/**/controller.js',
  ],
};

const swaggerUIOptions = {
  customSiteTitle: 'Falconer | Docs',
  customfavIcon: '/favicon.ico',
  customCss,
  customJs: '/styles.js',
  explorer: true,
  swaggerUrl: '/api-docs.json',
  swaggerOptions: {
    docExpansion: 'none',
  },
};

app.use('/styles.js', express.static('docs/styles.js'));
app.use('/favicon.ico', express.static('docs/favicon.ico'));
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(null, swaggerUIOptions));
app.get('/api-docs.json', (req, res) => res.json(swaggerJSDocs(options)));

module.exports = app;
