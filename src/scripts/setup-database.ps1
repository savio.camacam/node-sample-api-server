# Guarda o hash do container se existir ou texto vazia caso contrario
$CONTAINER_EXISTS=$(docker container ls -a -f=name=falconer -q)

echo "Criando container"
docker-compose up -d
sleep 2
docker exec -it falconer bash -c "mongo < /scripts/init_rs.js"
docker exec -it falconer bash -c "mongo < /scripts/setup.js"
sleep 2


# docker exec -it falconer bash -c "mongodump -h localhost:27017 -d falconer -o db-upload"
# docker exec -it falconer bash -c "mongorestore --uri='mongodb+srv://ioara:ml4knsEXFWlh0ur1@falconer.gqz9r.gcp.mongodb.net/takeshi?retryWrites=true&w=majority' db-upload --nsFrom='falconer.*' --nsTo='takeshi.*'"