const express = require('express');
const { errors } = require('celebrate');

const router = express.Router();
const transactionMiddleware = require('./middlewares/transactions');

router.use(express.json());
router.use('/', transactionMiddleware.transactionStart);

router.use('/sentinel', require('./domains/_sentinel').routes);
router.use('/authentication', require('./domains/Authentication').routes);
router.use('/disease', require('./domains/Disease').routes);
router.use('/doctor', require('./domains/Doctor').routes);
router.use('/device', require('./domains/Device').routes);
router.use('/sick', require('./domains/Sick').routes);
router.use('/question', require('./domains/Question').routes);
router.use('/user', require('./domains/User').routes);

router.use('/', transactionMiddleware.transactionCommit);
router.use('/', transactionMiddleware.transactionAbort);

router.use(errors());

module.exports = router;
