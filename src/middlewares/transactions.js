/* eslint-disable no-unused-vars */
/* eslint-disable consistent-return */
const mongoose = require('mongoose');
const { isCelebrate } = require('celebrate');
const responses = require('../utils/responses');

module.exports = {
  async transactionStart(req, res, next) {
    // eslint-disable-next-line no-console
    console.log('Iniciando transação');
    req.session = await mongoose.connection.startSession();
    await req.session.startTransaction();
    next();
  },

  async transactionCommit(req, res, next) {
    // eslint-disable-next-line no-console
    console.log('Concluindo transação');
    await req.session.commitTransaction();
    req.session.endSession();

    next();
  },

  async transactionAbort(err, req, res, next) {
    // eslint-disable-next-line no-console
    console.error('Abortando transação');

    await req.session.abortTransaction();
    req.session.endSession();

    if (!res.headersSent) {
      if (isCelebrate(err)) {
        return res.status(responses.codes.VALIDATION_ERROR).json({
          message: responses.messages.VALIDATION_ERROR,
          data: err.joi.details,
        });
      }

      if (err.message === responses.messages.UNAUTHORIZED) {
        return res.status(responses.codes.UNAUTHORIZED).json({
          message: responses.messages.UNAUTHORIZED,
          data: null,
        });
      }

      if (err instanceof SyntaxError && err.status === 400 && 'body' in err) {
        return res.status(responses.codes.VALIDATION_ERROR).json({
          message: responses.messages.VALIDATION_ERROR,
          data: null,
        });
      }

      const environment = process.env.NODE_ENV || 'development';

      return res.status(responses.codes.SERVER_ERROR).json({
        message: responses.messages.SERVER_ERROR,
        data: environment === 'development' ? err : null,
      });
    }
  },
};
