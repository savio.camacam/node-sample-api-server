const jwt = require('jsonwebtoken');
const models = require('../models');
const config = require('../../config/secrets.json');
const responses = require('../utils/responses');

module.exports = {
  async checkToken(req, res, next) {
    try {
      const { session } = req;

      const token = req.headers['x-access-token'];
      const decoded = jwt.verify(token, config.JWT_SECRET);
      const user = await models.User.findOne({ shortName: decoded.shortName }, null, { session });

      if (!user) throw new Error('USER_NOT_FOUND');

      res.locals.user = user;

      next();
    } catch (error) {
      switch (error.message) {
        case 'USER_NOT_FOUND':
          res
            .status(responses.codes.UNAUTHORIZED)
            .send({ message: responses.messages.UNAUTHORIZED, data: 'INVALID_TOKEN' });
          break;
        case 'jwt expired':
          res
            .status(responses.codes.UNAUTHORIZED)
            .send({ message: responses.messages.UNAUTHORIZED, data: 'EXPIRED_TOKEN' });
          break;
        default:
      }
      next(error);
    }
  },
};
