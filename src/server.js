const app = require("./app");
const config = require("../config/server.json");
require("./push_notifications/watchers");

const PORT = process.env.PORT || config.PORT;

app.listen(PORT, "0.0.0.0", () => {
  // eslint-disable-next-line no-console
  console.log(`Servidor executando na porta ${PORT}`);
});
