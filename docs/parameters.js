/**
 * @swagger
 * components:
 *   parameters:
 *     Pagination:
 *       in: query
 *       name: pagination
 *       required: false
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               page:
 *                 type: integer
 *                 minimum: 1
 *                 example: 1
 *               limit:
 *                 type: integer
 *                 minimum: 1
 *                 maximum: 10
 *                 example: 5
 */
