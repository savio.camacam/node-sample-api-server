module.exports = {
  /**
   * @swagger
   * components:
   *   schemas:
   *     Login:
   *       type: object
   *       required:
   *         - identifier
   *         - password
   *       properties:
   *         identifier:
   *           type: string
   *         password:
   *          type: string
   */
  Login: {},

  /**
   * @swagger
   * components:
   *   schemas:
   *     Response:
   *       type: object
   *       required:
   *         - message
   *         - data
   *       properties:
   *         message:
   *           type: string
   *         data:
   *           type:
   *             oneof:
   *               - type: string
   *               - type: array
   *               - type: object
   *       example:
   *         message: 'SUCCESS'
   *         data: 'success'
   */
  Response: {},

  /**
   * @swagger
   * components:
   *   schemas:
   *     PaginatedResponse:
   *       allOf:
   *         - $ref: '#/components/schemas/Response'
   *         - type: object
   *           required:
   *             - metadata
   *           properties:
   *             metadata:
   *               required:
   *                 - page
   *                 - total
   *                 - limit
   *               properties:
   *                 page:
   *                   type: integer
   *                 total:
   *                   type: integer
   *                 limit:
   *                   type: integer
   *           example:
   *             metadata:
   *               page: 1
   *               total: 5
   *               limit: 5
   */
  PaginatedResponse: {},

  /**
   * @swagger
   * components:
   *   schemas:
   *     h-errorResponse:
   *       type: object
   *       required:
   *         - message
   *         - data
   *       properties:
   *         message:
   *           type: string
   *         data:
   *           type:
   *             oneOf:
   *               - type: string
   *               - type: array
   *               - type: object
   */
  'h-errorResponse': {},
};
