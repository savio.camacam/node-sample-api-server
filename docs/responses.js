/**
 * @swagger
 * components:
 *   responses:
 *     Success:
 *       description: Default success response.
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/Response'
 *
 *           example:
 *             message: 'SUCCESS'
 *             data: 'response'
 */

/**
 * @swagger
 * components:
 *   responses:
 *     PaginatedSuccess:
 *       description: Default success paginated response.
 *       content:
 *         application/json:
 *           schema:
 *             allOf:
 *               - $ref: '#/components/schemas/Response'
 *               - $ref: '#/components/schemas/PaginatedResponse'
 *
 *           example:
 *             message: 'SUCCESS'
 *             metadata:
 *               page: 1
 *               total: 5
 *               limit: 5
 *             data: 'response'
 */

/**
 * @swagger
 * components:
 *   responses:
 *     BadRequest:
 *       description: Request error.
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/h-errorResponse'
 *
 *           example:
 *             message: 'BAD_REQUEST'
 *             data: null
 */

/**
 * @swagger
 * components:
 *   responses:
 *     InvalidPassword:
 *       description: Verify your credentials and try again.
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/h-errorResponse'
 *
 *           example:
 *             message: 'UNAUTHORIZED'
 *             data: null
 */

/**
 * @swagger
 * components:
 *   responses:
 *     Unauthorized:
 *       description: >
 *                Your permission level doesn't grant access to this resource
 *                or the token recieved no more valid, in this case, please login again.
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/h-errorResponse'
 *
 *           examples:
 *             Not Authorized:
 *               value:
 *                 message: 'UNAUTHORIZED'
 *                 data: null
 *             Invalid Token:
 *               value:
 *                 message: 'UNAUTHORIZED'
 *                 data: 'INVALID_TOKEN'
 */

/**
 * @swagger
 * components:
 *   responses:
 *     NotFound:
 *       description: The server did not find this resource.
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/h-errorResponse'
 *
 *           example:
 *             message: 'NOT_FOUND'
 *             data: null
 */

/**
 * @swagger
 * components:
 *   responses:
 *     DefaultError:
 *       description: The server did not find this resource.
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/h-errorResponse'
 *
 *           example:
 *             message: 'UNEXPECTED_ERROR'
 *             data: null
 */

/**
 * @swagger
 * components:
 *   responses:
 *     DefaultError:
 *       description: The server did not find this resource.
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/h-errorResponse'
 *
 *           example:
 *             message: 'UNEXPECTED_ERROR'
 *             data: null
 */

/**
 * @swagger
 *
 * components:
 *   responses:
 *     ServerError:
 *       description: There was an error while processing this request. Consider checking the logs or stdout for further information.
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/h-errorResponse'
 *
 *           example:
 *             message: 'SERVER_ERROR'
 *             data: null
 */
