
function format () {
  
    setInterval(function (){
  
      const spans = Array.from(document.querySelectorAll("span.prop-type"));
  
      const mySpans = spans.filter(function (sp) {
        return sp.className == "prop-type";
      });
      
      mySpans.forEach(function (sp) {
        let props = sp.innerHTML
        .replace(/<\!--\s((\w|\/|-|\:|\s)*)\s-->/g, " ")
        .replace(/\s\s/g, " ")
        .trim()
        .split(" ");
        
        if (props.length > 1) {
          sp.innerHTML = props.join(" | ");
          sp.className = "prop-type prop-types";
        }
      });
    }, 100);
  }
  
  window.addEventListener("DOMContentLoaded", format);
  