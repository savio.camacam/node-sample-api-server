/**
 * @swagger
 * components:
 *   securitySchemes:
 *     JWT:
 *       type: apiKey
 *       in: header
 *       name: x-access-token
 *
 *     Requesting:
 *       type: apiKey
 *       in: header
 *       name: requesting
 */
